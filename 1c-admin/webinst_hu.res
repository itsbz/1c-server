  �'      ResB              P   �   N	  N	         N	  IDS_USAGE_WIN IDS_USAGE_LINUX IDS_UNKNOWN_PARAMETER IDS_IIS_NOTFOUND IDS_PATH_REQUIRED IDS_NAME_REQUIRED IDS_CONNSTR_REQUIRED IDS_CONFPATH_REQUIRED IDS_INCOMP_KEYS IDS_WEBSRV_REQUIRED IDS_WSDIR_OR_DESCRIPTOR_REQUIRED IDS_DIR_KEY_REQUIRED IDS_CONNSTR_OR_DESCRIPTOR_KEY_REQUIRED IDS_PUB_NOTFOUND IDS_WRONG_CONNECTIONSTRING IDS_WRONG_DIRECTORY IDS_WRONG_DIRECTORY1 IDS_DELETE_SUCCESS IDS_PUB_SUCCESS IDS_PUB_UPDATED IDS_OBSOLETTE_KEY IDS_EXCEPTION IDS_WWWROOT_DENIED IDS_WEBINST_NOADAPTER IDS_DESCRIPTOR_REQUIRED IDS_DESCRIPTORNOTFOUND IDS_ADMIN_PRIVILEGES_REQUIRED_WARNING_WINDOWS IDS_ADMIN_PRIVILEGES_REQUIRED_WARNING_LINUX �  K i v � t e l :     L e � r �   n e m   t a l � l h a t �   A   k i a d v � n y   f r i s s � t v e   K � z z � t � t e l   n e m   t a l � l h a t �   A   k � z z � t � t e l   v � g r e h a j t v a   w e b s r v   -   k � t e l e z Q  p a r a m � t e r   I I S   w e b k i s z o l g � l �   n e m   t a l � l h a t � .   A   k � z z � t � t e l   l e � l l � t � s a   m e g t � r t � n t   I s m e r e t l e n   p a r a n c s s o r i   b e � l l � t � s :     A   % s   k u l c s   e l a v u l t ,   h a s z n � l j a   a   % s - t   - d i r   -   k � z z � t � t e l   k � t e l e z Q  p a r a m � t e r e   ,�A   % s   k � n y v t � r   n e m   l e h e t   a   k i a d v � n y   k � n y v t � r a   -�M e g   k e l l   a d n i   - w s d i r   v a g y   - d e s c r i p t o r   k u l c s o t   0�A   % s   � s   % s   k u l c s o k   n e m   k o m p a t i b i l i s e k   e g y m � s s a l .   1�M e g   k e l l   a d n i   a   - c o n n s t r   v a g y   - d e s c r i p t o r   k u l c s o t   1�M e g   k e l l   a d n i   a   k � z z � t � t e l   n e v � t   a   b e � l l � t � s b a n :     6�M e g   k e l l   a d n i   a   k � z z � t � t e l   k � n y v t � r � t   a   b e � l l � t � s b a n :     9�A   l e � r �   k � n y v t � r a   n e m   e g y e z i k   m e g   a   k i a d v � n y   k � n y v t � r � v a l   :�M e g   k e l l   a d n i   a z   � t n o n a l a t   a   . v r d   f � j l h o z   a   b e � l l � t � s b a n :     ;�M e g   k e l l   a d n i   a   c s a t l a k o z � s   k a r a k t e r l � n c � t   a   b e � l l � t � s b a n :     B�A   - d i r   p a r a m � t e r   k � n y v t � r a   n e m   e g y e z i k   m e g   a   k i a d v � n y   k � n y v t � r � v a l   K�M e g   k e l l   a d n i   a z   � t n o n a l a t   a z   A p a c h e   k o n f i g u r � c i � s   f � j l h o z   a   b e � l l � t � s b a n :     P�A   p a r a m � t e r   k a p c s o l a t i   s o r a   - c o n n s t r   n e m   e g y e z i k   a   k � z z � t � t e l   k a p c s o l a t i   s o r � v a l   ~�N i n c s e n e k   t e l e p � t v e   k i t e r j e s z t � s i   m o d u l o k   a   w e b - s z e r v e r h e z . 
 A   k i a d v � n y   f u t t a t � s � h o z   m � d o s � t a n i   k e l l   a   1 C : V � l l a l a t   b e � l l � t � s � t .   ��E n n e k   a   m qv e l e t n e k   a   v � g r e h a j t � s � h o z   s z � k s � g e s e k   a   s z u p e r f e l h a s z n � l �   h a t � s k � r e i   ( r o o t ) . 
 E l l e n k e z Q  e s e t b e n   a   p r o g r a m   h i b � s   m qk � d � s e   f o r d u l h a t   e l Q. 
 A   s z u p e r f e l h a s z n � l � i   m � d b a n   t � r t � n Q  i n d � t � s h o z   h a j t s a   v � g r e   a   p a r a n c s o t   a   s u d o - n   k e r e s z t � l .    �E n n e k   a   m qv e l e t n e k   a   v � g r e h a j t � s � h o z   O P - r e n d s z e r   a d m i n i s z t r � t o r i   j o g o k r a   v a n   s z � k s � g . 
 E l l e n k e z Q  e s e t b e n   a   p r o g r a m   h i b � s   m qk � d � s e   f o r d u l h a t   e l Q. 
 A   r e n d s z e r g a z d a   j o g o k k a l   t � r t � n Q  i n d � t � s h o z   m e g   k e l l   n y i t n i   a   r e n d s z e r g a z d a i   p a r a n c s s o r t   � s   e b b e n   a   p a r a n c s s o r b a n   k e l l   v � g r e h a j t a n i   a   p a r a n c s o t .   ���1 C : @54?@8OB85  8 .   #B8;8B0  ?C1;8:0F88  251- :;85=B0 
  
         C1;8:0F8O:  
  
         w e b i n s t   [ - p u b l i s h ]   w e b s r v   - w s d i r   V i r t u a l D i r   - d i r   D i r   - c o n n s t r   c o n n S t r   [ - c o n f P a t h   c o n f P a t h ]  
  
         C1;8:0F8O  =0  >A=>25  ACI5AB2CNI53>  v r d   D09;0:  
  
         w e b i n s t   [ - p u b l i s h ]   w e b s r v   [ - w s d i r   V i r t u a l D i r ]   - d e s c r i p t o r   v r d P a t h   - d i r   D i r   [ - c o n n s t r   c o n n S t r ]   [ - c o n f P a t h   c o n f P a t h ]  
  
         #40;5=85  ?C1;8:0F88:  
  
         w e b i n s t   - d e l e t e   w e b s r v   - w s d i r   V i r t u a l D i r   [ - d i r   D i r ]   [ - c o n n s t r   c o n n S t r ]   [ - c o n f P a t h   c o n f P a t h ]  
  
         #40;5=85  ?C1;8:0F88  ?>  ACI5AB2CNI5<C  v r d   D09;C:  
  
         w e b i n s t   - d e l e t e   w e b s r v   [ - w s d i r   V i r t u a l D i r ]   - d e s c r i p t o r   v r d P a t h   [ - d i r   D i r ]   [ - c o n n s t r   c o n n S t r ]   [ - c o n f P a t h   c o n f P a t h ]  
  
         ;NG8:  
  
                 - p u b l i s h :   >?C1;8:>20BL,   :;NG  ?>  C<>;G0=8N 
                 - d e l e t e :   C40;8BL  ?C1;8:0F8N 
                 w e b s r v  
                         - a p a c h e 2 :   ?C1;8:0F8O  51- :;85=B0  4;O  A p a c h e   2 . 0  
                         - a p a c h e 2 2 :   ?C1;8:0F8O  51- :;85=B0  4;O  A p a c h e   2 . 2  
                         - a p a c h e 2 4 :   ?C1;8:0F8O  51- :;85=B0  4;O  A p a c h e   2 . 4  
                 - w s d i r   V i r t u a l D i r :   28@BC0;L=K9  :0B0;>3 
                 - d i r   D i r :   D878G5A:89  :0B0;>3,   2  :>B>@K9  1C45B  >B>1@065=  28@BC0;L=K9 
                 - d e s c r i p t o r   v r d P a t h :   ?CBL  :  ACI5AB2CNI5<C  v r d   D09;C 
                 - c o n n s t r   c o n n S t r :   AB@>:0  A>548=5=8O   
                 - c o n f P a t h   c o n f P a t h :   ?>;=K9  ?CBL  :  :>=D83C@0F8>==><C  D09;C  A p a c h e   �ߣ1 C : @54?@8OB85  8 .   #B8;8B0  ?C1;8:0F88  251- :;85=B0 
  
         C1;8:0F8O:  
  
         w e b i n s t   [ - p u b l i s h ]   w e b s r v   - w s d i r   V i r t u a l D i r   - d i r   D i r   - c o n n s t r   c o n n S t r   [ - c o n f P a t h   c o n f P a t h ]   [ - o s a u t h ]  
  
         C1;8:0F8O  =0  >A=>25  ACI5AB2CNI53>  v r d   D09;0:  
  
         w e b i n s t   [ - p u b l i s h ]   w e b s r v   [ - w s d i r   V i r t u a l D i r ]   - d e s c r i p t o r   v r d P a t h   - d i r   D i r   [ - c o n n s t r   c o n n S t r ]   [ - c o n f P a t h   c o n f P a t h ]   [ - o s a u t h ]  
  
         #40;5=85  ?C1;8:0F88:  
  
         w e b i n s t   - d e l e t e   w e b s r v   - w s d i r   V i r t u a l D i r   [ - d i r   D i r ]   [ - c o n n s t r   c o n n S t r ]   [ - c o n f P a t h   c o n f P a t h ]  
  
         #40;5=85  ?C1;8:0F88  ?>  ACI5AB2CNI5<C  v r d   D09;C:  
  
         w e b i n s t   - d e l e t e   w e b s r v   [ - w s d i r   V i r t u a l D i r ]   - d e s c r i p t o r   v r d P a t h   [ - d i r   D i r ]   [ - c o n n s t r   c o n n S t r ]   [ - c o n f P a t h   c o n f P a t h ]  
  
         ;NG8:  
  
                 - p u b l i s h :   >?C1;8:>20BL,   :;NG  ?>  C<>;G0=8N 
                 - d e l e t e :   C40;8BL  ?C1;8:0F8N 
                 w e b s r v  
                         - i i s :   ?C1;8:0F8O  51- :;85=B0  4;O  I I S  
                         - a p a c h e 2 :   ?C1;8:0F8O  51- :;85=B0  4;O  A p a c h e   2 . 0  
                         - a p a c h e 2 2 :   ?C1;8:0F8O  51- :;85=B0  4;O  A p a c h e   2 . 2  
                         - a p a c h e 2 4 :   ?C1;8:0F8O  51- :;85=B0  4;O  A p a c h e   2 . 4  
                 - w s d i r   V i r t u a l D i r :   28@BC0;L=K9  :0B0;>3 
                 - d i r   D i r :   D878G5A:89  :0B0;>3,   2  :>B>@K9  1C45B  >B>1@065=  28@BC0;L=K9 
                 - d e s c r i p t o r   v r d P a t h :   ?CBL  :  ACI5AB2CNI5<C  v r d   D09;C 
                 - c o n n s t r   c o n n S t r :   AB@>:0  A>548=5=8O   
                 - c o n f P a t h   c o n f P a t h :   ?>;=K9  ?CBL  :  :>=D83C@0F8>==><C  D09;C  A p a c h e   ( B>;L:>  4;O  ?C1;8:0F88  =0  A p a c h e )  
                 - o s a u t h :   8A?>;L7>20=85  W i n d o w s   02B>@870F88  ( B>;L:>  4;O  ?C1;8:0F88  =0  I I S )    c5� � �� �T � w �e 5��> .   �� Fau� �xgY���  � � ��� )4 M  � �t�f �ab4��