  �'      ResB             �c     .  Rf  Rf  �      �   IDS_MAP_CONTROL_NAME IDS_MAP_PROPERTIES_TITLE IDS_MAPPROP_FORMBACKCOLOR IDS_MAPPROP_TRANSPARENT IDS_MAPPROP_BORDER IDS_MAPPROP_CATEGORY_APPEARANCE IDS_INVALID_IDENTIFIER IDS_MAPPROP_BORDERCOLOR IDS_MAPPROP_ENABLE IDS_MAPPROP_CATEGORY_GENERAL IDS_MAPPROP_SHORTCUT IDS_MAPPROP_TIPTEXT IDS_MAPPROP_CATEGORY_BEHAVIOR IDS_MAPPROP_CATEGORY_USE IDS_MAP_CWOR_LAYERS_COLS_HAS_DIFFERENT_SIZE IDS_MAP_UNDO_CHANGE_FIELD IDS_GEO_SCHEME_DOC IDS_MAP_PRINTERSNOTINSTALLED IDS_MAP_DSSD_DATA_SERIES_TEXT_FONT IDS_NOTHING_TO_PRINT IDS_MAP_DSSD_DATA_SERIES_TEXT_ROTATION_ANGLE IDS_TYPE_PRESENTATION_MAP_OBJECT_POINT IDS_MAPPROP_CATEGORY_EVENTS IDS_MAP_EVENT_SELECT_PROCESSING IDS_MAP_EVENT_DETAILS_PROCESSING IDS_CANT_LOAD_DBF_DATA IDS_MAP_DSIMPORTDLG_DS_SOURCE_NAME IDS_MAP_DSIMPORTDLG_DS_TARGET_NAME IDS_MAP_DSIMPORTDLG_IMPORT IDS_MAP_DSIMPORTDLG_DRAWING_TYPE IDS_MAP_DSIMPORTDLG_DS_TYPE IDS_MAP_DBF_FILED_TYPE_CHAR IDS_MAP_DBF_FILED_TYPE_NUMERIC IDS_MAP_NONE_SERIES_SELECTED IDS_MAP_DBF_FILED_TYPE_LOGICAL IDS_MAP_DBF_FILED_TYPE_DATE IDS_TYPE_PRESENTATION_MAP_LINE_SEGMENTS_ARRAY IDS_CMD_SETUP_LAYERS IDS_MAP_UNDO_CHANGE_OBJECT_POLYLINE IDS_TYPE_PRESENTATION_MAP_DOCUMENT IDS_MAP_STRING_NOTFOUND IDS_MAPPROP_LOAD_MAP_LINK IDS_MAPPROP_CLEAR_MAP IDS_MAP_CLEAR_MAP IDS_MAP_INCORRECT_MAP_FILE_NAME IDS_TYPE_PRESENTATION_MAP_LAYERS_ARRAY IDS_TYPE_PRESENTATION_MAP IDS_TYPE_PRESENTATION_MAP_OBJECT IDS_TYPE_PRESENTATION_MAP_ARRAY IDS_TYPE_PRESENTATION_MAP_LAYER IDS_MAPPROP_SCALE IDS_MAP_LAYER_ELEMENTS_TYPE_ALREADY_SET IDS_TYPE_PRESENTATION_DATA_SERIES IDS_TYPE_PRESENTATION_MAP_DATA_SERIES_ARRAY IDS_MAP_PARAMETER_WRONG_NUMBER IDS_MAP_PARAMETER_WRONG_MAP_OBJECT IDS_MAP_PROP_SUB_CATEGORY_MAP_VIEW IDS_MAP_MAP_FORMAT_ARCINFO IDS_MAP_SELECT_FILE_DIALOG_CAPTION IDS_TYPE_PRESENTATION_POLYLINE_SEGMENT IDS_TYPE_PRESENTATION_MAP_OBJECT_LINE IDS_MAP_DATA_SOURCE_COLUMNS_MORE_THAN_THREE IDS_MAPLINE_NONE IDS_MAPLINE_SOLID IDS_MAPLINE_DOT IDS_MAPLINE_DASH IDS_MAPLINE_DASHDOT IDS_MAPLINE_DASHDOTDOT IDS_TYPE_PRESENTATION_MAP_OBJECT_POLYGON IDS_TYPE_PRESENTATION_MAP_POLYGON_LINES_ARRAY IDS_MAP_LOADING_MAP_FILE IDS_MAP_INVALID_SHP_FILE IDS_MAP_UNEXPECTED_SHAPE IDS_TYPE_PRESENTATION_MAP_OBJECT_MULTI_POINT IDS_TYPE_PRESENTATION_POLYGON_CONTOUR IDS_MAP_UNDO_CHANGE_PROPS IDS_MAPPROP_SETUP_LAYERS IDS_MAP_UNDO_CHANGE_LAYERS IDS_MAP_PROP_SUB_CATEGORY_NON_VISUAL_OBJECTS_SETUP IDS_MAP_LINK_SETUP IDS_MAP_SCALE_RANGE_INVALID IDS_LOAD_LAYER_FAILURE IDS_MAP_OBJECT_PROP_PRESENTATION_PICTURE IDS_MAP_LSD_LAYER_DATA_SOURCE_CONTENT_TYPE IDS_MAP_OBJECT_DOES_NOT_BELONG_TO_ARRAY IDS_MAP_INVALID_SEEK_FILE IDS_MAP_LOADING_INDEX_FILE IDS_MAP_DATA_SERIES_DRAWING_TYPE_DO_NOT_DISPLAY IDS_MAP_DATA_SERIES_DRAWING_TYPE_TEXT IDS_MAP_DATA_SERIES_DRAWING_TYPE_HISTOGRAM IDS_MAP_LOADED_SHAPE_IS_NOT_NULL_SHAPE IDS_MAP_LOADED_SHAPE_IS_NOT_POINT_SHAPE IDS_MAP_LOADED_SHAPE_IS_NOT_MULTI_POINT_SHAPE IDS_MAP_LOADED_SHAPE_IS_NOT_POLY_LINE_SHAPE IDS_MAP_LOADED_SHAPE_IS_NOT_POLYGON_SHAPE IDS_MAP_LOADED_SHAPE_IS_NOT_MULTIPATCH_SHAPE IDS_MAP_WRONG_OBJECTUUID_TO_UNDO IDS_MAP_UNDO_CHANGE_LAYER_OBJECT IDS_MAP_ACCESS_TO_DELETED_OWNER IDS_MAP_UNDO_CHANGE_OBJECT_POLYGON IDS_MAP_IMPORT_SHP_FILE IDS_MAP_LOAD_NEW_DOCUMENT IDS_MAP_UNDO_CHANGE_MOVE_OBJECT IDS_MAP_UNDO_CHANGE_DELETE_LAYER IDS_MAP_UNDO_CHANGE_DELETE_DATA_SERIES IDS_MAP_UNDO_CHANGE_DELETE_OBJECT IDS_MAP_WRONG_DATASERIESUUID_TO_UNDO IDS_MAP_WRONG_LAYERUUID_TO_UNDO IDS_MAP_UNDO_APPLY_DATASERIES IDS_MAP_UNDO_APPLY_OBJECTS IDS_MAP_UNDO_CLEAR IDS_MAP_UNDO_LOAD_DOCUMENT IDS_MAP_PRINTING_DOCUMENT_NAME ID_PREVIEWPAGEDESC IDS_MAP_UNDO_CHANGE_CHANGE_PRINT_PARAMS IDS_CMD_CATEGORY_MAP IDS_MAP_INSERT_CONTROL IDS_MAP_LSD_LAYER_TEXT IDS_MAP_UNDO_CHANGE_DATA_SERIES IDS_MAP_LSD_LAYER_VISIBLE IDS_MAP_LSD_LAYER_SELECTABLE IDS_MAP_UNDO_CHANGE_OBJECT IDS_MAP_UNDO_CHANGE_OBJECT_POINT IDS_MAP_UNDO_CHANGE_OBJECT_MULTIPOINT IDS_MAP_DSSD_DATA_SERIES_TEXT IDS_MAP_DSSD_DATA_SERIES_FORMAT IDS_MAP_DSSD_DATA_SERIES_DRAWING_TYPE IDS_MAP_DSSD_OBJECT_SHOW_DATA IDS_CMD_APPLY_ATTRIBUTES IDS_MAP_VALUELISTUI_TITLE IDS_MAP_OBJECT_PROP_PRESENTATION_COLOR IDS_MAP_OBJECT_PROP_PRESENTATION_BORDER_COLOR IDS_MAP_OBJECT_PROP_PRESENTATION_TEXT_SYMBOL IDS_MAP_OBJECT_PROP_PRESENTATION_FONT IDS_MAP_OBJECT_PROP_PRESENTATION_LINE_STYLE IDS_MAP_OBJECT_PROP_PRESENTATION_LINE_THICKNESS IDS_MAP_OBJECT_PROP_PRESENTATION_TEXT_ANGLE IDS_MAP_OBJECT_PROP_PRESENTATION_TEXT_TRANSPARENT IDS_MAP_DATA_SOURCE_CONTENT_TYPE_ON_ROW IDS_MAP_DATA_SOURCE_CONTENT_TYPE_ON_INTERSECTION IDS_MAP_DATA_SERIES_DRAWING_TYPE_FIGURE_COLOR IDS_MAP_DATA_SERIES_DRAWING_TYPE_FIGURE_SIZE IDS_MAP_DSSD_DATA_SERIES_FIGURE_BASE_COLOR IDS_MAP_LSD_LAYER_SCALE_RANGE_START IDS_MAP_LSD_LAYER_SCALE_RANGE_END IDS_MAP_DATA_SERIES_DRAWING_TYPE_PICTURE IDS_MAP_FONT_REF_STRING IDS_MAPLINESCLASS_DESC IDS_MAP_MAP_FORMAT_INTERNAL IDS_MAP_CWOR_DATA_SERIES_COLS_HAS_DIFFERENT_SIZE IDS_MAP_DATA_SERIES_DRAWING_TYPE_FIGURE_COLOR_OFFSET IDS_MAP_DSSD_DATA_SERIES_TEXT_COLOR IDS_MAP_OBJECT_PROP_PRESENTATION_REAPER_POINT IDS_MAP_DSSD_OBJECT_VISIBLE IDS_MAP_SHOW_MODE_AS_SCALE_DEFINED IDS_MAP_SHOW_MODE_ALL_DATA IDS_MAPPROP_SHOW_MODE IDS_MAP_BAD_OBJECT_TYPE IDS_MAP_CWOR_DATA_SERIES_TEXT IDS_MAP_CWOR_DATA_SERIES_FORMAT IDS_MAP_CWOR_DATA_SERIES_DRAWING_TYPE IDS_MAP_CWOR_DATA_SERIES_FIGURE_COLOR IDS_MAP_CWOR_DATA_SERIES_TEXT_COLOR IDS_MAP_CWOR_DATA_SERIES_TEXT_ORIENTATION IDS_MAP_CWOR_DATA_SERIES_TEXT_FONT IDS_MAPPROP_SAVE_AS_INTERNAL_MAP_LINK IDS_MAP_SAVE_FILE_DIALOG_CAPTION IDS_MAP_REAPER_POINT_POSITION_LEFT_TOP IDS_MAP_REAPER_POINT_POSITION_LEFT_CENTER IDS_MAP_REAPER_POINT_POSITION_LEFT_BOTTOM IDS_MAP_REAPER_POINT_POSITION_CENTER_TOP IDS_MAP_REAPER_POINT_POSITION_CENTER IDS_MAP_REAPER_POINT_POSITION_CENTER_BOTTOM IDS_MAP_REAPER_POINT_POSITION_RIGHT_TOP IDS_MAP_REAPER_POINT_POSITION_RIGHT_CENTER IDS_MAP_REAPER_POINT_POSITION_RIGHT_BOTTOM IDS_MAP_CWOR_LAYER_TEXT IDS_MAP_CWOR_LAYER_VISIBLE IDS_MAP_CWOR_LAYER_SCALE_RANGE_START IDS_MAP_CWOR_LAYER_SCALE_RANGE_END IDS_MAP_CWOR_OBJECTS_COL_HAS_DIFFERENT_SIZE IDS_MAP_CWOR_OBJECT_TYPE IDS_MAP_CWOR_OBJECT_DETAILS IDS_MAP_CWOR_OBJECT_VALUE IDS_MAP_CWOR_OBJECT_TOOLTIP IDS_MAP_CWOR_OBJECT_VISIBLE IDS_MAP_CWOR_OBJECT_SHOW_LINKED_DATA IDS_MAP_CWOR_OBJECT_POINT_COLOR IDS_MAP_CWOR_OBJECT_POINT_FONT IDS_MAP_CWOR_OBJECT_POINT_SYMBOL IDS_MAP_CWOR_OBJECT_LATITIDE IDS_MAP_CWOR_OBJECT_LONGITUDE IDS_MAP_CWOR_MULTIPOINTS_POINTS_COLS_HAS_DIFFERENT_SIZE IDS_MAP_CWOR_OBJECT_PICTURE_POINT_POSITION IDS_MAP_CWOR_OBJECT_PICTURE_PICTURE IDS_MAP_CWOR_OBJECT_LINE_COLOR IDS_MAP_CWOR_OBJECT_LINE_STYLE IDS_MAP_CWOR_LINE_POINTS_COLS_HAS_DIFFERENT_SIZE IDS_MAP_CWOR_LINE_SEGMENT_COLS_HAS_DIFFERENT_SIZE IDS_MAP_CWOR_POLYGON_POINTS_COLS_HAS_DIFFERENT_SIZE IDS_MAP_OBJECT_PROP_VISIBLE IDS_MAP_OBJECT_PROP_SHOW_LINKED_DATA IDS_CANT_WORK_WHEN_DATA_SOURCE_IS_PRESENT IDS_CANT_ADD_COLUMN_TO_DATA_SOURCE IDS_OBJECT_DELETE_WARNING IDS_MAPPROP_ACTIVE_LAYER IDS_MAPPROP_CATEGORY_DATA_SOURCE IDS_MAP_PROP_SUB_CATEGORY_DATA_SOURCE_SETUP IDS_MAP_CWOR_LAYER_MOXEL_DATA_SOURCE_RANGE IDS_MAPPROP_ACTIVE_LAYER_DS_RANGE IDS_MAPPROP_ACTIVE_LAYER_DS_CONTENT_TYPE IDS_MAP_CWOR_OBJECTS_DATA_HAS_DIFFERENT_SIZE IDS_MAP_CANT_LOAD_PROJECTION_FILE IDS_MAP_TOOLS_ERRPR IDS_MAP_INVALID_FILE_FORMAT IDS_TYPE_PRESENTATION_DATA_SERIES_VALUE IDS_MAP_CANT_LOAD_DUPLICATE_FILE IDS_MAP_CWOR_LAYER_SELECTABLE IDS_MAP_UNDO_SCHEME_BOUND_BOX_CHANGE IDS_MAP_UNDO_ADD_NEW_LAYER IDS_MAP_OBJECT_PROP_PRESENTATION_PUSH_PIN IDS_MAP_OBJECT_PROP_DATASERIES_NAME IDS_MAP_OBJECT_PROP_DATASERIES_VALUE IDS_MAP_OBJECT_PROP_DATASERIES_DRAWING_TYPE IDS_MAP_DS_NAME_NOT_ENTERED IDS_MAP_SET_AS_VALUE IDS_MAP_UNDO_SET_AS_OBJECT_VALUE IDS_MAP_FILL_SERIES_NOT_IMPORTED IDS_MAP_CANT_IDENTIFY_OBJECT IDS_CMD_SETUP_OBJECT IDS_MAP_FILE_FORMAT_NOT_SUPPORTED IDS_CMD_SHOW_ALL_DATA IDS_CMD_SHOW_AS_SCALE_DEFINED IDS_MAP_PUSHPIN_NONE IDS_MAP_PUSHPIN_PIN IDS_MAP_PUSHPIN_DART IDS_MAP_PUSHPIN_LITTLECIRCLE IDS_MAP_PUSHPIN_BIGCIRCLE IDS_MAP_PUSHPIN_LITTLESQUARE IDS_MAP_PUSHPIN_BIGSQUARE IDS_MAP_PUSHPIN_LITTLETRIANGLE IDS_MAP_PUSHPIN_BIGTRIANGLE IDS_MAP_PUSHPIN_QUESTION IDS_MAP_PUSHPIN_EXCLAMATION IDS_MAP_SHOW_MODE_SETTED_AREA IDS_MAPPROP_CATEGORY_VIEWED_BOX IDS_MAP_TREE_DATASOURCE_USING_WARNING IDS_MAP_CWOR_LAYER_OBJECTS_TYPE IDS_MAP_CWOR_OBJECT_MARKER IDS_MAP_DSSD_DATA_SERIES_VALUE IDS_MAP_DSSD_OBJECT_VALUE IDS_DATASERIES_IMPORT_MODE_ALL IDS_DATASERIES_IMPORT_MODE_NONE IDS_POINT_DRAWING_STYLE_AS_SYMBOL IDS_POINT_DRAWING_STYLE_AS_MARKER IDS_POINT_DRAWING_STYLE_AS_PICTURE IDS_TYPE_PRESENTATION_MAP_COORDINATES_ARRAY IDS_MAP_OBJECT_PROP_PRESENTATION_DRAWING_TYPE IDS_TYPE_PRESENTATION_MAP_BOUNDARY_BOX IDS_MAPPROP_VIEWED_BOX_LEFT IDS_MAPPROP_VIEWED_BOX_RIGHT IDS_MAPPROP_VIEWED_BOX_TOP IDS_MAPPROP_VIEWED_BOX_BOTTOM IDS_FIND_BY_LOCATION_INCLUDES IDS_FIND_BY_LOCATION_INCLUDES_FULL IDS_FIND_BY_LOCATION_INCLUDE IDS_FIND_BY_LOCATION_INCLUDE_FULL IDS_MAPPROP_CATEGORY_TITLE_AREA IDS_MAPPROP_TITLE_AREA_SHOW IDS_MAPPROP_TITLE_AREA_TEXT IDS_MAPPROP_TITLE_AREA_TEXT_FONT IDS_MAPPROP_TITLE_AREA_TEXT_COLOR IDS_MAPPROP_TITLE_AREA_TEXT_ALIGN IDS_MAPPROP_TITLE_AREA_BORDER IDS_MAPPROP_TITLE_AREA_BORDER_COLOR IDS_MAPPROP_TITLE_AREA_TRANSPARENT IDS_MAPPROP_TITLE_AREA_BACK_COLOR IDS_MAPPROP_CATEGORY_LEGEND_AREA IDS_MAPPROP_LEGEND_AREA_SHOW IDS_MAPPROP_LEGEND_AREA_BORDER IDS_MAPPROP_LEGEND_AREA_BORDER_COLOR IDS_MAPPROP_LEGEND_AREA_TRANSPARENT IDS_MAPPROP_LEGEND_AREA_BACK_COLOR IDS_MAPPROP_LEGEND_AREA_ITEMS IDS_MAP_HORIZJUST_LEFT IDS_MAP_HORIZJUST_RIGHT IDS_MAP_HORIZJUST_CENTER IDS_MAPPROP_LEGEND_AREA_SHOW_SCALE_LINE IDS_MAP_TITLE_AREA_TITLE IDS_MAP_LEGEND_AREA_TITLE IDS_MAP_LEGEND_ITEMS_OBJECT_VALUE IDS_CMD_SEND_TO_LEGEND IDS_TYPE_PRESENTATION_GEOGRAPHICAL_SCHEME_TITLE_AREA IDS_TYPE_PRESENTATION_GEOGRAPHICAL_SCHEME_LEGEND_AREA IDS_TYPE_PRESENTATION_MAP_LEGEND_ITEM IDS_MAP_LEGEND_ITEMS_PICTURE IDS_MAP_LEGEND_ITEMS_SAMPLE_TEXT IDS_MAP_LEGEND_ITEMS_SAMPLE_FONT IDS_MAP_LEGEND_ITEMS_CAPTION_TEXT IDS_MAP_LEGEND_ITEMS_CAPTION_FONT IDS_MAP_LEGEND_ITEMS_SAMPLE_COLOR IDS_MAP_COORDINATES_EDIT IDS_TYPE_PRESENTATION_MAP_SHOWED_AREA IDS_MAP_DATA_SERIES_DRAWING_TYPE_CIRCLE IDS_MAP_UNDO_ADD_LEGEND_ITEM IDS_MAP_UNDO_DELETE_LEGEND_ITEM IDS_MAP_UNDO_EDIT_LEGEND_ITEM IDS_MAPPROP_LEGEND_AREA_COLOR IDS_MAPPROP_LEGEND_AREA_FONT IDS_MAP_SELECT_LAYER IDS_MAP_SELECT_SERIES IDS_CMD_SETUP_LEGEND IDS_CMD_SET_OBJECT_VALUE IDS_MAP_FAILED_INITIALIZE_PROJECTION IDS_MAP_COORDINATE_TRANSFORM_ERROR IDS_CMD_MAP_LATITUDE IDS_CMD_MAP_LONGITUDE IDS_MAPPROP_SHOW_COORDINATES IDS_MAP_DATA_SERIES_DRAWING_TYPE_SIZED_CIRCLE IDS_MAP_UNDO_EDIT_TITLE IDS_MAP_UNDO_EDIT_LEGEND IDS_SCALE_TYPE_DO_NOT_DISPLAY IDS_SCALE_TYPE_DISPLAY_BY_VALUES IDS_MAP_LEGEND_ITEMS_SCALE_TYPE IDS_TYPE_PRESENTATION_GEOGRAPHICAL_SCHEME_DRAWING_AREA IDS_MAP_DRAWING_AREA_TITLE IDS_MAPPROP_DRAWING_AREA_BORDER IDS_MAPPROP_DRAWING_AREA_BORDER_COLOR IDS_MAPPROP_DRAWING_AREA_TRANSPARENT IDS_MAPPROP_DRAWING_AREA_BACK_COLOR IDS_MAPPROP_CATEGORY_DRAWING_AREA IDS_MAP_UNDO_EDIT_DRAWING_AREA IDS_MAP_LSD_LAYER_NAME IDS_MAP_INVALID_OBJECT_NAME IDS_MAP_DSSD_DATA_SERIES_NAME IDS_MAP_NEW_SERIES_NAME IDS_MAP_NON_UNIQUE_OBJECT_NAME IDS_CMD_MAP_OBJECT_SERIES IDS_MAPPROP_LATITUDE_SHIFT IDS_MAPPROP_LONGITIDE_SHIFT IDS_MAP_PROP_SUB_CATEGORY_MAP_PROJECTION IDS_MAPPROP_SCHEME_PROJECTION IDS_MAP_SELECT_SERIES_TITLE IDS_SERIES_VALUE_SHOW_MODE_AS_VALUE IDS_SERIES_VALUE_SHOW_MODE_AS_PART IDS_MAP_DSSD_DATA_SERIES_GROUP IDS_MAP_DSSD_DATA_SERIES_VALUES_DRAWING_TYPE IDS_MAP_SELECT_GROUP IDS_MAP_CANT_REPROJECT_SCHEME IDS_MAP_PROJECTION_CILINDRICAL_MILLER IDS_MAP_PROJECTION_CILINDRICAL_LAMBERT_EQUAL_AREA IDS_MAP_PROJECTION_CILINDRICAL_GALL_STEREOGRAPHIC IDS_MAP_PROJECTION_CILINDRICAL_EQUIDISTANT IDS_MAP_PROJECTION_PSEUDO_CILINDRICAL_SINUSOIDAL IDS_MAP_PROJECTION_PSEUDO_CILINDRICAL_MOLLWEIDE IDS_MAP_PROJECTION_PSEUDO_CILINDRICAL_ROBINSON IDS_MAP_PROJECTION_PSEUDO_CILINDRICAL_ECKERT_I IDS_MAP_PROJECTION_PSEUDO_CILINDRICAL_ECKERT_II IDS_MAP_PROJECTION_PSEUDO_CILINDRICAL_ECKERT_III IDS_MAP_PROJECTION_PSEUDO_CILINDRICAL_ECKERT_IV IDS_MAP_PROJECTION_PSEUDO_CILINDRICAL_ECKERT_V IDS_MAP_PROJECTION_PSEUDO_CILINDRICAL_ECKERT_VI IDS_MAP_PROJECTION_PSEUDO_CILINDRICAL_HATANO_ASYMETRICAL IDS_MAP_PROJECTION_PSEUDO_CILINDRICAL_LOXIMUTAL IDS_MAP_PROJECTION_PSEUDO_CILINDRICAL_MBTFPP IDS_MAP_PROJECTION_PSEUDO_CILINDRICAL_MBTFPQ IDS_MAP_PROJECTION_PSEUDO_CILINDRICAL_MBTFPS IDS_MAP_PROJECTION_PSEUDO_CILINDRICAL_PUTNIN_P2 IDS_MAP_PROJECTION_PSEUDO_CILINDRICAL_PUTNIN_P5 IDS_MAP_PROJECTION_PSEUDO_CILINDRICAL_WINKEL_I IDS_MAP_PROJECTION_PSEUDO_CILINDRICAL_BOGGS_EUMORPHIC IDS_MAP_PROJECTION_AZIMUTAL_HAMMER IDS_MAP_PROJECTION_AZIMUTAL_WAGNER_VII IDS_MAP_PROJECTION_AZIMUTAL_AITOFF IDS_MAP_PROJECTION_AZIMUTAL_WINKEL_TRIPEL IDS_MAP_PROJECTION_MISCELLANEOUS_AUGUST_EPICYCLIODAL IDS_MAP_PROJECTION_MISCELLANEOUS_BACON_GLOBULAR IDS_MAP_PROJECTION_MISCELLANEOUS_NICOLOSI_GLOBULAR IDS_MAP_PROJECTION_MISCELLANEOUS_APIAN_GLOBULAR IDS_MAP_PROJECTION_MISCELLANEOUS_ORTELIUS_OVAL IDS_MAP_PROJECTION_MISCELLANEOUS_VAN_DER_GRINTEN_I IDS_MAP_PROJECTION_MISCELLANEOUS_VAN_DER_GRINTEN_II IDS_MAP_PROJECTION_MISCELLANEOUS_VAN_DER_GRINTEN_III IDS_MAP_INVALID_CTOR_PARAMS_SIZE IDS_MAP_INVALID_CTOR_PARAM IDS_MAP_INDEX_OUT_OF_BOUNDS IDS_MAP_DUPLICATE_LAYER_NAMES IDS_MAP_PROJECTION_AZIMUTAL_EQUIDISTANT IDS_MAP_PROJECTION_AZIMUTAL_LAMBERT_EQUAL_AREA IDS_MAP_PROJECTION_AZIMUTAL_LAMBERT_EQUAL_AREA_CONIC IDS_MAP_UNDO_CHANGE_OBJECT_DATA IDS_CMD_FIND_OBJECT_ON_MAP IDS_MAP_OUTPUTDISABLED IDS_MAPPROP_OUTPUT IDS_MAP_KM map.xsd model.xdto styles.png DSDrawingStyles.png PushPins.png ReaperPoint.png styles_old.bmp DSDrawingStyles_old.bmp PushPins_old.bmp ReaperPoint_old.bmp   k m   U s e   D a t e   L e f t   N o n e   F o n t   D a r t   N a m e   T e x t   S o l i d   V a l u e   R u l e r   C o l o r   C h a r t   R i g h t   D a s h e d   I m p o r t   M i d d l e   F o r m a t   E v e n t s   S e r i e s   C o l u m n   B u t t o n   C e n t e r   S t y l e :   N u m e r i c   I n c l u d e   V i s i b l e   I n   r o w s   P i c t u r e   L o g i c a l   G e n e r a l   D e t a i l s   P u s h p i n   T o o l t i p   A l l   d a t a   S e r i e s % d   L a t i t u d e   L e f t   t o p   S e t t i n g s   I n c l u d e s   P i e   c h a r t   S h o w   d a t a   a d d   l a y e r   L a t i t u d e :   R i g h t   t o p   L o n g i t u d e   C h a r a c t e r   B e h a v i o r s   P l o t   a r e a   S h o w   a r e a   S h o w   v a l u e   m o v e   l a y e r   L i n e   s t y l e   S h o w   c h a r t   S h o w   r u l e r   T i t l e   a r e a   D a t a   r a n g e   T e x t   c o l o r   C e n t e r   t o p   I m p o r t   a l l   L o n g i t u d e :   L i n e   w i d t h   V a l u e   t y p e   D a s h - d o t t e d   F i g u r e   s i z e   S a m p l e   f o n t   L e g e n d   A r e a   L e f t   c e n t e r   D a t a   s e r i e s   S a m p l e   t e x t   L e f t   b o t t o m   O b j e c t   t y p e   S h o w   v a l u e s   C h a r t   t i t l e   D r a w i n g   t y p e   C h a r t   l e g e n d   C o n t e n t   t y p e   S e l e c t   g r o u p   A c t i v e   l a y e r   S m a l l   s q u a r e   L a r g e   s q u a r e   D i s p l a y   t y p e   A r c I n f o   m a p s   R i g h t   c e n t e r   C a p t i o n   t e x t   N a m e   i n   f i l e   B o r d e r   c o l o r   F i g u r e   c o l o r   V i s i b l e   a r e a   R i g h t   b o t t o m   d e l e t e   l a y e r   S a m p l e   c o l o r   S e l e c t   l a y e r   S m a l l   c i r c l e   D i s p l a y   f o n t   L a r g e   c i r c l e   C a p t i o n   f o n t   I n v a l i d   n a m e   P r i n t   p r e v i e w   d e l e t e   o b j e c t   d e l e t e   s e r i e s   S e l e c t   s e r i e s   S h o w   a l l   d a t a   C e n t e r   b o t t o m   N a m e   i n   l a y e r   D o t D a s h D o t t e d   Q u e s t i o n   m a r k   S h o w   b y   v a l u e s   I n c l u d e   e n t i r e   D i s p l a y   f o r m a t   C h a r a c t e r   f o n t   S p e c i f i e d   a r e a   L a r g e   t r i a n g l e   S m a l l   t r i a n g l e   S h o w   p r o p o r t i o n   L o a d   i n d e x   f i l e   a d d   l e g e n d   i t e m   R e f e r e n c e   p o i n t   I n c l u d e s   e n t i r e   A t   i n t e r s e c t i o n   S i z e d   p i e   c h a r t   S h o w   l i n k e d   d a t a   N o t h i n g   t o   p r i n t   E n a b l e   s e l e c t i o n   T e x t   o r i e n t a t i o n   S t r i n g   n o t   f o u n d   I m p o r t   S H P   f i l e     O b j e c t   v i s i b i l i t y   c h a n g e   t i t l e   a r e a   O b j e c t   p r o p e r t i e s   S e r i e s   t o   d i s p l a y   N o t h i n g   t o   i m p o r t   L a y e r   s e t t i n g s . . .   E x c l a m a t i o n   p o i n t   S e r i e s   g r o u p   n a m e   D i s p l a y   c h a r a c t e r   F i g u r e   c o l o r   t o n e   A n g l e   o f   r o t a t i o n   A t t r i b u t e s   t o   c o p y   O u t p u t   i s   d i s a b l e d   c h a n g e   l e g e n d   a r e a   U n k n o w n   i d e n t i f i e r   c h a n g e   l e g e n d   i t e m   d e l e t e   l e g e n d   i t e m   C h a r t   d r a w i n g   a r e a   E n d   o f   s c a l e   r a n g e   S p e c i f i e d   b y   s c a l e   O b j e c t   b o u n d a r y   b o x   C o l l e c t i o n   s e t t i n g s   G e o g r a p h i c a l   s c h e m a   c h a n g e   l a y e r   s e r i e s   c h a n g e   d r a w i n g   a r e a   s e r i e s   v a l u e   c h a n g e   I n v a l i d   f i l e   f o r m a t   D e l e t e   l o a d e d   c h a r t ?   G e o g r a p h i c a l   s c h e m a s   B a s e   c o l o r   o f   s h a d o w   G r p h c s   l i b r a r y   e r r o r   S t a r t   o f   s c a l e   r a n g e   P r o j e c t i o n   p a r a m e t e r s   T r a n s p a r e n t   b a c k g r o u n d   A l i g n 
 A l i g n   t i t l e   t e x t   T h e   n a m e   i s   n o t   u n i q u e   L e g e n d   i t e m   s e t t i n g s . . .   I n v a l i d   S H P   f i l e   f o r m a t   U n s u p p o r t e d   f i l e   f o r m a t !   D a t a   s o u r c e   o r g a n i z a t i o n   I n c o r r e c t   p a r a m e t e r   t y p e   G e o g r a p h i c a l   s c h e m a   l i n e s   c h a n g e   b o u n d i n g   r e c t a n g l e   R e f e r e n c e   p o i n t   p o s i t i o n     G e o g r a p h i c a l   s c h e m a   l a y e r   S e r i e s   n a m e   n o t   s p e c i f i e d   G e o g r a p h i c a l   s c h e m a   f i e l d   R e f e r e n c e   t o   d e l e t e d   o b j e c t   A z i m u t h a l   H a m m e r   p r o j e c t i o n   L a y e r   i t e m   t y p e   a l r e a d y   s e t   A z i m u t h a l   A i t o f f   p r o j e c t i o n   T i t l e 
 S h o w   c h a r t   t i t l e   a r e a   G e o g r a p h i c   c o o r d i n a t e   e d i t o r   S c a l e   m u s t   b e   g r e a t e r   t h a n   0   S e g m e n t s   a r e   d i f f e r e n t   s i z e s   L e g e n d 
 S h o w   c h a r t   l e g e n d   a r e a   C y l i n d r i c a l   M i l l e r   p r o j e c t i o n   I n d e x   i s   o u t s i d e   o f   t h e   r a n g e   G e o g r a p h i c   c o o r d i n a t e   g a l l e r y   A z i m u t h a l   W a g n e r   7   p r o j e c t i o n   G e o g r a p h i c a l   s c h e m a   t i t l e   a r e a   N o   p r i n t e r   i n s t a l l e d   o n   s y s t e m   T e x t 
 T e x t   s h o w n   i n   c h a r t   t i t l e   G e o g r a p h i c a l   s c h e m a   c o l l e c t i o n   c h a n g e   p o i n t   o b j e c t   p r o p e r t i e s   G e o g r a p h i c a l   s c h e m a   l e g e n d   a r e a   I n c o r r e c t   c o n s t r u c t o r   p a r a m e t e r   c l e a r i n g   o f   g e o g r a p h i c a l   s c h e m a   L a y e r   o b j e c t   t y p e   d i s t i n g u i s h e d   L e g e n d   i t e m s 
 S h o w n   l e g e n d   i t e m s   c h a n g e   p r i n t   s e t t i n g   p a r a m e t e r s   B a s e   g e o g r a p h i c a l   s c h e m a   o b j e c t   B o r d e r 
 L e g e n d   a r e a   b o r d e r   s t y l e   S e l e c t   g e o g r a p h i c a l   s c h e m a   f i l e   G e o g r a p h i c a l   s c h e m a   l e g e n d   i t e m   A z i m u t h a l   e q u i d i s t a n t   p r o j e c t i o n   G e o g r a p h i c a l   s c h e m a   d r a w i n g   a r e a   a p p l i c a t i o n   o f   o b j e c t   a t t r i b u t e s   a p p l i c a t i o n   o f   s e r i e s   a t t r i b u t e s   c h a n g e   p o l y g o n   o b j e c t   p r o p e r t i e s   G e o g r a p h i c a l   s c h e m a   l a y e r   s e r i e s   c h a n g e   p r o p e r t i e s   o f   l a y e r   o b j e c t   B o r d e r 
 B o r d e r   s t y l e   o f   t i t l e   a r e a   l o a d   g e o g r a p h i c a l   s c h e m a   d o c u m e n t   c h a n g e   p o l y l i n e   o b j e c t   p r o p e r t i e s   T r a n s f e r r e d   o b j e c t   i s   n o t   a   n u m b e r   A z i m u t h a l   W i n k e l   T r i p e l   p r o j e c t i o n   B o r d e r 
 B o r d e r   s t y l e   o f   l e g e n d   a r e a   D o   y o u   w a n t   t o   d e l e t e   t h i s   o b j e c t ?   T r a n s f e r r e d   o b j e c t   i s   n o t   i n   a r r a y   C y l i n d r i c a l   e q u i d i s t a n t   p r o j e c t i o n   P o i n t   o b j e c t   o f   g e o g r a p h i c a l   s c h e m a   C o n i c   L a m b e r t   e q u a l   a r e a   p r o j e c t i o n   c h a n g e   f i e l d   o f   g e o g r a p h i c a l   s c h e m a   c h a n g e   m u l t i p o i n t   o b j e c t   p r o p e r t i e s   C l e a r 
 C l e a r   l o a d e d   d a t a   f r o m   c o n t r o l   L i n e a r   o b j e c t   o f   g e o g r a p h i c a l   s c h e m a   T i t l e   f o n t 
 F o n t   t o   u s e   f o r   t i t l e   t e x t   T h e   n u m b e r   o f   p a r a m e t e r s   i s   i n c o r r e c t   B o r d e r   c o l o r 
 L e g e n d   a r e a   b o r d e r   c o l o r   S a v e 
 S a v e   g e o g r a p h i c a l   s c h e m a   t o   d i s k   V i s i b l e   r e g i o n   o f   g e o g r a p h i c a l   s c h e m a   C o u l d   n o t   l o a d   l a y e r   d e s c r i p t i o n   f i l e   P o i n t   c o l l e c t i o n s   a r e   d i f f e r e n t   s i z e s   S e l e c t   a t   l e a s t   o n e   s e r i e s   t o   i m p o r t !   S e r i e s   c o l l e c t i o n s   a r e   d i f f e r e n t   s i z e s   s e t t i n g   o f   " V a l u e "   p r o p e r t y   o f   o b j e c t s   P s e u d o   c y l i n d r i c a l   W i n k e l   1   p r o j e c t i o n   P s e u d o   c y l i n d r i c a l   E c k e r t   1   p r o j e c t i o n   P s e u d o   c y l i n d r i c a l   E c k e r t   2   p r o j e c t i o n   P s e u d o   c y l i n d r i c a l   E c k e r t   3   p r o j e c t i o n   P s e u d o   c y l i n d r i c a l   E c k e r t   4   p r o j e c t i o n   P s e u d o   c y l i n d r i c a l   E c k e r t   5   p r o j e c t i o n   P s e u d o   c y l i n d r i c a l   E c k e r t   6   p r o j e c t i o n   P s e u d o   c y l i n d r i c a l   R o b i n s o n   p r o j e c t i o n   M i s c e l l a n e o u s   O r t e l i u s   o v a l   p r o j e c t i o n   P o l y g o n a l   o b j e c t   o f   g e o g r a p h i c a l   s c h e m a   F i n d   o n   M a p 
 F i n d   s e l e c t e d   o b j e c t   o n   m a p   T h e   i s   a   l a y e r   w i t h   t h i s   n a m e   i n   s y s t e m   P s e u d o   c y l i n d r i c a l   P u t n i n   P 2   p r o j e c t i o n   P s e u d o   c y l i n d r i c a l   P u t n i n   P 5   p r o j e c t i o n   S c a l e 
 G e o g r a p h i c a l   s c h e m a   d i s p l a y   s c a l e   S e g m e n t   c o l l e c t i o n s   a r e   d i f f e r e n t   s i z e s   M i s c e l l a n e o u s   B a c o n   g l o b u l a r   p r o j e c t i o n   T i t l e   c o l o r 
 C o l o r   t o   u s e   f o r   t i t l e   t e x t   P s e u d o   c y l i n d r i c a l   L o x i m u t a l   p r o j e c t i o n   P s e u d o   c y l i n d r i c a l   M o l l w e i d e   p r o j e c t i o n   R e a d 
 R e a d   g e o g r a p h i c a l   s c h e m a   f r o m   d i s k   T o p 
 S e t s   l i m i t a t i o n   f o r   t o p   v i s i b l e   a r e a   O b j e c t   i s   n o t   g e o g r a p h i c a l   s c h e m a   o b j e c t   C o l l e c t i o n   o f   g e o g r a p h i c a l   s c h e m a   l a y e r s   P s e u d o   c y l i n d r i c a l   s i n u s o i d a l   p r o j e c t i o n   B o r d e r   c o l o r 
 B o r d e r   c o l o r   f o r   t i t l e   a r e a   M u l t i p o i n t   o b j e c t   o f   g e o g r a p h i c a l   s c h e m a   c h a n g e   p r o p e r t i e s   o f   g e o g r a p h i c a l   s c h e m a   )�B o r d e r   c o l o r 
 B o r d e r   c o l o r   f o r   l e g e n d   a r e a   )�M i s c e l l a n e o u s   a p i a n   g l o b u l a r   1   p r o j e c t i o n   )�V a l u e   o f   g e o g r a p h i c a l   s c h e m a   l a y e r   s e r i e s   )�S p e c i f y   f i l e   i n   w h i c h   c h a r t   w i l l   b e   s a v e d   )�C y l i n d r i c a l   L a m b e r t   e q u a l   a r e a   p r o j e c t i o n   )�C y l i n d r i c a l   G a l l   s t e r e o g r a p h i c   p r o j e c t i o n   )�B a c k g r o u n d   C o l o r 
 C o n t r o l   b a c k g r o u n d   c o l o r   *�S c a l e   K e e p i n g 
 S p e c i f i e s   c h a r t   s c a l i n g   m o d e   *�M i s c e l l a n e o u s   V a n   D e r   G r i n t e n   1   p r o j e c t i o n   *�M i s c e l l a n e o u s   V a n   D e r   G r i n t e n   2   p r o j e c t i o n   *�M i s c e l l a n e o u s   V a n   D e r   G r i n t e n   3   p r o j e c t i o n   *�M i s c e l l a n e o u s   N i c o l o s i   g l o b u l a r   p r o j e c t i o n   *�N e w   c o l u m n s   c a n n o t   b e   a d d e d   t o   d a t a   s o u r c e   *�L e f t 
 S e t s   l i m i t a t i o n   f o r   l e f t   v i s i b l e   a r e a   +�C o o r d i n a t e   c o n v e r s i o n   e r r o r .   I n v a l i d   v a l u e .   +�A t t e m p t   t o   r e a d   s h a p e   t h a t   i s   n o t   N u l l S h a p e   +�B o r d e r   C o l o r 
 C o l o r   o f   b o r d e r   a r o u n d   c o n t r o l   ,�A t t e m p t   t o   r e a d   s h a p e   t h a t   i s   n o t   P o i n t S h a p e   ,�S e n d   t o   L e g e n d 
 A d d   s e l e c t e d   s e r i e s   t o   l e g e n d   ,�R i g h t 
 S e t s   l i m i t a t i o n   f o r   r i g h t   v i s i b l e   a r e a   ,�L a y e r   o b j e c t   c o l l e c t i o n s   a r e   d i f f e r e n t   s i z e s   ,�B a c k g r o u n d   c o l o r 
 T i t l e   a r e a   b a c k g r o u n d   c o l o r   ,�M i s c e l l a n e o u s   A u g u s t   e p i c y c l o i d a l   p r o j e c t i o n   -�S e l e c t i o n 
 C a l l e d   w h e n   s e l e c t i n g   i t e m   o n   c h a r t   -�P s e u d o   c y l i n d r i c a l   B o g g s   e u m o r p h i c   p r o j e c t i o n   -�B a c k g r o u n d   c o l o r 
 L e g e n d   a r e a   b a c k g r o u n d   c o l o r   .�C o l l e c t i o n   o f   p o l y l i n e s   o f   g e o g r a p h i c a l   s c h e m a   .�C o l l e c t i o n   o f   g e o g r a p h i c a l   s c h e m a   l a y e r   s e r i e s   .�B o t t o m 
 S e t s   l i m i t a t i o n   f o r   b o t t o m   v i s i b l e   a r e a   .�I n n e r   b o u n d a r y   c o l l e c t i o n s   a r e   d i f f e r e n t   s i z e s   .�A t t e m p t   t o   r e a d   s h a p e   t h a t   i s   n o t   P o l y g o n S h a p e   .�C o l l e c t i o n   c a n n o t   c o n t a i n   o b j e c t s   o f   t h i s   t y p e   .�B o r d e r 
 T y p e   o f   b o r d e r   d i s p l a y e d   a r o u n d   c o n t r o l   /�L a t i t u d e   O f f s e t 
 S e t s   p r o j e c t i o n   l a t i t u d e   o f f s e t   /�c h a n g e   p r o p e r t i e s   o f   g e o g r a p h i c a l   s c h e m a   l a y e r s   /�A t t e m p t   t o   r e a d   s h a p e   t h a t   i s   n o t   P o l y L i n e S h a p e   0�S h a p e   o f   p o l y g o n a l   o b j e c t   o f   g e o g r a p h i c a l   s c h e m a   0�M a p   c a n n o t   b e   d i s p l a y e d   i n   s p e c i f i e d   p r o j e c t i o n !   0�O b j e c t s   h a v e   d i f f e r e n t   n u m b e r   o f   d a t a   f o r   s e r i e s   0�R e c o r d   n u m b e r s   i n   S H P   a n d   S H X   f i l e s   d o   n o t   m a t c h   1�S e g m e n t   o f   p o l y l i n e   o b j e c t   o f   g e o g r a p h i c a l   s c h e m a   1�A t t e m p t   t o   r e a d   s h a p e   t h a t   i s   n o t   M u l t i P o i n t S h a p e   1�L o n g i t u d e   O f f s e t 
 S e t s   p r o j e c t i o n   l o n g i t u d e   o f f s e t   1�E r r o r   i n i t i a l i z i n g   c o o r d i n a t e   c o n v e r s i o n   s e r v i c e !   1�A t t e m p t   t o   r e a d   s h a p e   t h a t   i s   n o t   M u l t i P a t c h S h a p e   1�A d d i t i o n a l   D a t a 
 S e l e c t   a d d i t i o n a l   d a t a   t o   d i s p l a y   2�L a y e r   I D   t r a n s f e r r e d   t o   r o l l b a c k   o b j e c t   i s   i n v a l i d   3�S e r i e s   I D   t r a n s f e r r e d   t o   r o l l b a c k   o b j e c t   i s   i n v a l i d   3�A t t e m p t   t o   l o a d   l a y e r   w i t h   n a m e   t h a t   a l r e a d y   e x i s t s   4�I m p o r t   g e o g r a p h i c a l   s c h e m a   l a y e r   ( A r c I n f o   S h a p e F i l e )   4�T r a n s p a r e n t 
 S h o w   c o n t r o l   w i t h   t r a n s p a r e n t   b a c k g r o u n d   4�C o l l e c t i o n   o f   l a y e r s   h a s   d i f f e r e n t   n u m b e r   o f   o b j e c t s   5�P r o j e c t i o n 
 S e t s   t h e   p r o j e c t i o n   o f   g e o g r a p h i c a l   s c h e m a   5�E n a b l e d 
 W h e t h e r   t h i s   c o n t r o l   w i l l   b e   a v a i l a b l e   o r   n o t   6�D B F   f i l e   c a n n o t   b e   l o a d e d   w i t h   a c c o m p a n y i n g   l a y e r   d a t a   6�C a n n o t   l o a d   l a y e r   " % s "   b e c a u s e   e r r o r   " % s "   h a s   o c c u r r e d   6�T h i s   p r o c e d u r e   c a n n o t   b e   u s e d   w h e n   d a t a   s o u r c e   i s   s e t !   6�C o l l e c t i o n   o f   g e o g r a p h i c a l   s c h e m a   l i n e   s e g m e n t   o b j e c t s   7�L a y e r   I D   t r a n s f e r r e d   t o   r o l l b a c k   o b j e c t   i s   i n a c c e s s i b l e   9�T r a n s p a r e n t   b a c k g r o u n d 
 L e g e n d   a r e a   t r a n s p a r e n t   b a c k g r o u n d   9�S h o r t c u t 
 K e y b o a r d   s h o r t c u t   f o r   q u i c k   e x e c u t i o n   o f   c o n t r o l   9�P a r a m e t e r   i s   n o t   v a l i d   f i l e   n a m e   w i t h   g e o g r a p h i c a l   s c h e m a   ;�T r a n s p a r e n t   b a c k g r o u n d 
 T r a n s p a r e n t   b a c k g r o u n d   o f   t i t l e   a r e a   ;�P s e u d o   c y l i n d r i c a l   H a t a n o   a s y m e t r i c a l   e q u a l   a r e a   p r o j e c t i o n   <�T r a n s p a r e n t   b a c k g r o u n d 
 T r a n s p a r e n t   b a c k g r o u n d   o f   l e g e n d   a r e a   >�F i l e   c o n t a i n s   u n s u p p o r t e d   s h a p e   t y p e   o r   f i l e   f o r m a t   i s   i n v a l i d   ?�S a v e   v a l u e   o f   t h i s   s e r i e s   t o   " V a l u e "   p r o p e r t y   o f   l a y e r   o b j e c t s ?   ?�P s e u d o   c y l i n d r i c a l   M c B r y d e - T h o m a s   f l a t   p o l a r   q u a r t i c   p r o j e c t i o n   A�P s e u d o   c y l i n d r i c a l   M c B r y d e - T h o m a s   f l a t   p o l a r   p a r a b o l i c   p r o j e c t i o n   B�P s e u d o   c y l i n d r i c a l   M c B r y d e - T h o m a s   f l a t   p o l a r   s i n u s o i d a l   p r o j e c t i o n   G�O u t p u t 
 A   u s e r   c a n   s a v e   d o c u m e n t   c o n t e n t s ,   p r i n t   i t   o r   c o p y   t o   c l i p b o a r d   I�H i e r a r c h i c a l   d a t a   s o u r c e   b e i n g   u s e d .   S u b o r d i n a t e   r e c o r d s   w i l l   b e   i g n o r e d .   K�A p p l y   A t t r i b u t e s 
 A p p l y   s e l e c t e d   a t t r i b u t e s   o f   c u r r e n t   o b j e c t   t o   a l l   o b j e c t s   L�S h o w   C o o r d i n a t e s 
 S h o w   c o o r d i n a t e s   o f   c h a r t   u n d e r   m o u s e   c u r s o r   i n   s t a t u s   l i n e   M�L a y e r s 
 L e t s   y o u   s e t   p a r a m e t e r s   o f   p l a n   l a y e r s   a n d   o b j e c t s   t h a t   b e l o n g   t o   t h e m   M�T o o l t i p 
 T o o l t i p   t e x t   t h a t   w i l l   b e   s h o w n   t o   c l a r i f y   t h e   p u r p o s e   o f   t h e   c o n t r o l   Q�S e r i e s   w h o s e   v a l u e   m u s t   b e   s a v e d   t o   " V a l u e "   p r o p e r t y   o f   o b j e c t s   m u s t   b e   i m p o r t e d !   W�T h i s   t y p e   o f   d a t a   s o u r c e   m u s t   c o n t a i n   e x a c t l y   t h r e e   c o l u m n s   ( o b j e c t ,   d a t a   s e r i e s ,   d a t a )   \�S a v e   t o   " V a l u e "   p r o p e r t y 
 S a v e   v a l u e s   o f   s e l e c t e d   s e r i e s   t o   " V a l u e "   p r o p e r t y   o f   l a y e r   o b j e c t s   b�D e t a i l   D a t a   p r o c e s s o r 
 C a l l e d   w h e n   d a t a   p r o c e s s o r   d e t a i l s   o f   s e l e c t e d   o b j e c t   o f   g e o g r a p h i c a l   s c h e m a   m�C u r r e n t   g e o g r a p h i c a l   s c h e m a   w i l l   b e   r e p l a c e d   b y   t h e   o n e   y o u   a r e   l o a d i n g . 
 
   A r e   y o u   s u r e   y o u   w a n t   t o   c o n t i n u e ?   �  �PNG

   IHDR   �      ���`  �IDATh���O[U��#���Bds��,j����!)�)İ�:W-w43:/,S��,�/U�a����,�ѱnƷ�N����9#���,&&���i��Ҟ�V��A�����s��9�>��9'@�RW��P˰�\�����W?DTv����-���`0Sn���SCr[ ��f�®>i�uD�]R���Vǯ�LvD�{�;��D-x!R��D�jx�<�y�����9�<}S(fc�D6��Y(�4$��_ʳ�z̢ �!Z� i��	�� ��,D}�N���Y�r���\�Z������5m}��W�{��aVW����x����tOBk�I�1Q��Bdu��g@+J��.h�-@F�!J!@�j+��Gi�O���N��zolX=g�"i�G�xxD��"OgV�O�<� c����� %ꐜ�^k����/!7�����OBh�A�׶5�D���	 �硴ev�I�t�=Q:$�D��z/�R�R��*ֳ���������S3�6�'��~�E��%���7��y� D��������� ���� ��m��ӂG���eO���÷������1�&BWqv�g>y�ǿ�\P L��
@d��:�T�jDf��F�n�D�H
+��l���	��=r���U��?�s|�M���4�G����h(,h���{K QuF%~���WXF��i�5��| ��F�c<V�c�}��1�3<������.� x�7�o�Et������������VGXk�g��xB;�����䘙t-8����*��{�k���>lQ�b׾Rl~�9l|�^�> y�3��^�}�<�<�n1��w���v���Bd�B� �lJ5@F�y+ ��Ad�w�� {i�]�߱E;N�fE����[��8P��J�
3��t�tn��i�td@���� Ht]�'�L�"x�B��_���y}��ڟ�m-�rPޒ��ޕ��_���h
���Wj����N���Q��i�sR�|+���@Fi�,@Z�i �*��0��>7����i���b4w�q�e4<h>%c���(y2��V�o��B�<�=��+�:�D#Ӓ�L�� �d =qo�����E���h�"�#((�Pz"P�(G3��u��'a+C�M����T�(��7�w�)-��!�LU�l����:ݷ������ �5�ɴO<����q�_�O%�`��    IEND�B`��������������:  BM:      :   (   �                                   ������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������ �  �  � ���������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������ff�ff������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������f��f������������ �  �  �  �  � ������������������������������������������������������������������������������������������������������������������������������������������������ff�ff�ff�ff�ff���������������������������������������fff�ff�ff���������������������������������������������������������������������������������������������������������������������f��f��f������ff�ff�ff������� �  �  � ��������������f��f��f�����������������f��f��f��f������ �  �  �  �  �  �  � ���������������������������������������������������������������������������������f��f��f��f��f��f��f��f��f��f��f��f��������������������ff�ff�ff�ff�ff�ff�ff�ff�ff����������������ff�ff�ff���������f��f �  � ��������������������������������������������������������������������������������������������������������������������f��f��f������ff�ff�ff������� �  �  � �����������f��f��f��f��f��������������f��f��f��f������ �  �  �  �  �  �  � ��������������f��f��f�����������������������������������������f��f��f���������������������������������������������f�����������������ff�ff�ff�ff�ff�ff�ff�ff�ff�ff�ff����������ff�ff�ff�ff�ff���������f � ��������������������������������������������������������������������������������f��f��f��f��f��������������������������f��f��f������ff�ff�ff������� �  �  � ��������f��f��f��f��f��f��f��������������f��f��������� �  �  �  �  �  �  � �����������f��f��f��f��f�����������������������������������f��f��f��f��f������������������������ �  �  � ��� � ���f����������������f��fff�ff�ff�ff�ff�ff�ff�ff�ff�ff�ff������f��fff�ff�ff�ff�ff������������������������������������������������������������������������������������������������f��f�����������������������������f��f��f������ff�ff�ff������� �  �  � ��������f��f��f��f��f��f��f������������������������������ �  �  �  �  � �����������f��f��f��f��f��f��f�����������������������������f��f��f��f��f��f��f������������������ �  � ������ � ������f����������������f��f��fff�ff�ff�ff�ff�ff�ff�ff�ff�ff������f��f��f �  �  �  � �����������������������������������������������������������������������������������������������f��f��f��������������������������f��f��f������ff�ff�ff������� �  �  � ��������f��f��f��f��f��f��f��������f��f��f��f��������������� �  �  � ��������������f��f��f��f��f��f��f�����������������������������f��f��f��f��f��f��f��������������������������� � ��� � ���f�������������f��f��f��f��fff�ff�ff�ff�ff�ff�ff�ff�ff�ff���f��f��f �  �  �  � ������ff�ff�ff�ff���������������������������������������������������������������������������������f��f��������������������������f��f��f������ff�ff�ff������� �  �  � �����������f��f��f��f��f��������f��f��f��f��f��f�����������������������������������f��f��f��f��f��f��f�����������������������������f��f��f��f��f��f��f������������������ff�ff�ff���� �  � ���f�������������f��f��f��f��f��fff�ff�ff�ff�ff�ff�ff�ff�ff������f��f �  �  � ������ff�ff�ff�ff�ff�ff������������������������������������������������������������������������������f��f��������������������������f��f��f������ff�ff�ff������������������������������f��f��f��������f��f��f��f��f��f��f��f�����������������������������������f��f��f��f��f�����������������������������������f��f��f��f��f���������������������ff�ff�ff������� � ���f�������������f��f��f��f��f��f��f �  �  �  �  �  �  �  � ��������f �  � ��������f��fff�ff�ff�ff�ff�ff���������������������������������������������������������������������������f��f��f�����������������������f��f��f������ff�ff�ff������������������������������������������f��f��f��f��f��f��f��f��f��f���������������ff�ff�ff������������f��f��f����������������������� �� �� �����������f��f��f���������������������ff�ff�ff�ff�������������f�������������f��f��f��f��f��f��f �  �  �  �  �  �  �  � ��������������������f��f��f��fff�ff�ff�ff�ff�ff���������������������������������������������������������������������������f��f�����������������������f��f��f������ff�ff�ff������������������������������������������f��f��f��f��f��f��f��f��f��f������������ff�ff�ff�ff�ff������������������������������������ �� �� �� �� ���������������������������������������ff�������������������f�������������f��f��f��f��f��f��f �  �  �  �  �  �  �  � ��������������������f��f��f��f��fff�ff�ff�ff�ff������������������������������������������������������������������f��������f��f��������f���������������������������ff�ff�ff������������������������������������������f��f��f��f��f��f��f��f��f��f���������ff�ff�ff�ff�ff�ff�ff������������������������������ �� �� �� �� �� �� ���������������������������������������������������������f����������������f��f��f��f��f��f �  �  �  �  �  �  � �����������������������f��f��f��f��f �  �  �  �  � ��������������������������������������������������������������������f��f��f��f��f��f��f���������������������������ff�ff�ff������������������������������������������f��f��f��f��f��f��f��f��f��f���������ff�ff�ff�ff�ff�ff�ff������������������������������ �� �� �� �� �� �� ���������������������������������������������������������f����������������f��f��f��f��f��f �  �  �  �  �  �  � �����������������������f��f��f��f��f �  �  �  �  � ������������������������������������������������������������������������������������������������������������������ff�ff�ff���������������������������������������������f��f��f��f��f��f��f��f������������ff�ff�ff�ff�ff�ff�ff������������������������������ �� �� �� �� �� �� ��������������������������̙�̙�̙�̙�̙�̙�̙�̙�̙�̙��f�������������������f��f��f��f��f �  �  �  �  �  � �����������������������������f��f��f��f �  �  �  � ���������������������������������������������������������������������������������������������������������������������ff�ff�ff������������������������������������������������f��f��f��f��f��f������������������ff�ff�ff�ff�ff������������������������������������ �� �� �� �� �����������������������������������������������������������������������������������f��f��f��f �  �  �  �  � �����������������������������������f��f��f �  �  � �����������������������������������������������������������������������������������������������������������������������������������������������������������������������������������f��f��f��f������������������������ff�ff�ff������������������������������������������ �� �� ��������������������������������������������������������������������������������������������f��f �  �  � ��������������������������������������������f��f �  � ������������  �PNG

   IHDR   �      ����  [IDATh��Z�!,�������<7p�"3,Z?ii�Z�r ?e "$��Y�!%m�)����/;6�"R�V����kEP �:f����:�x�w�g5� �6~�ڻ������h,"��R�t���J�+��"���'Eá��ŧxl����O���q;?��� �Ov4�<;��RK^
��o��-~;��Y<n0��J�pɛ�{ж�+p%[O8n����+9=O o�.{`	YZ[��u����x��X+n�vP/ĵD�+��6%��fm�ۊ��-��rZ|2Q7��{���wN2�x&�C�ՁR�o g��w���%A���;�SH���ho��lD`���9e����~TdU�/    IEND�B`����������  BM�      >   (   �            �                      ��� ����������������������  ��������������������  ��������������������  �������������������  ���������������  ������?���������  ��������������  ��O������������  ���G�����?��၁  ���?�������?��q��  ���?���?������1��  �������������?�1��  ��������������?�၁  �������������������  ������������������  ����������������������  ��������������.  �PNG

   IHDR   �      ���`  �IDATh���O[e��/�&)TcL��,N�,N�C`�#[1����2D"r��}�a�M�(a�d�t��Q��B�ڞrY�F����x�����C����I��ᔓ|����'�<i	@�Cl^�s��L>*���Hĝ�⮌@�Q�F�q�9�������"9��$�� ��l'i��Ow^�!C�^�N*O�#?Ň���B�?y���d�3�H�_����L��ȯ�� Q���-\oz�&�������:��k�>�Ft8��C�5��uaj�6��L�G��T���HS5AJQ�_y����`�d��x���� �sD'�)�ӡD��f���~l���1#Z�94Y�84>��z��@�2�䖷���z,hǜ��~�??���#���ԟ��9@������DS)ZS�C,n��Kbn��	Է���V�]u��M�46�T�۰�� �P]��+O���,����ԟ��9@���3�a���̡ٱ�3luX��]��us���l� ��2����p�w=�hg�
��J��J�Sw`��U�46Q<�?���v���8i[�6݌�x��B<�o�[���%�9�qz��q.Ѿ�������^���!;6���c� ��=&��i���zԛ4Y�S���1�?��nY\�/���y��ϴ��*!��������h��v�5�Ac���;�r�#w�FnM�a��� �{K�����:O����9����Lk�I�����D�1S�y��ͣ�Y��Y{ۻ�9X�h]p����Ňn�_�����y����/,�+�����O��[������L��1S�:f���,e�Dc����O��o�{�t�^,J�BU�k˳����M��l����<��g�3�#!��������	|�x��?���lD�JފNA�� <�"�C_�@�*ØTz����%�<1��^�����b�/y��<���5�����JU�3^��O��XZ�SZ�%Z�	dG�@�$���3A�'ø��ϗ?>�0�P����ʂ`�~O�=�
 ��X�����u��g���/���w�2���Ԣ�1�����CJY&��|�&��~A�iB �nTt��מu	��=?	7�����y���4nn���O���p������ ��DBQS8��G�u4GbU�*�b��3�߃:�a�L�㇯<��{~n�����y���d��I�_���BK8w�y�N���U��G'�6W���2N$ﱄ���{���B���/TB�\��o
���4�?s�8՗A�    IEND�B`���������������6  BM6      6  (   �             	                      3   f   �   �   �    3  33  f3  �3  �3  �3   f  3f  ff  �f  �f  �f   �  3�  f�  ��  ̙  ��   �  3�  f�  ��  ��  ��   �  3�  f�  ��  ��  ��    3 3 3 f 3 � 3 � 3 � 3  33 333 f33 �33 �33 �33  f3 3f3 ff3 �f3 �f3 �f3  �3 3�3 f�3 ��3 ̙3 ��3  �3 3�3 f�3 ��3 ��3 ��3  �3 3�3 f�3 ��3 ��3 ��3   f 3 f f f � f � f � f  3f 33f f3f �3f �3f �3f  ff 3ff fff �ff �ff �ff  �f 3�f f�f ��f ̙f ��f  �f 3�f f�f ��f ��f ��f  �f 3�f f�f ��f ��f ��f   � 3 � f � � � � � � �  3� 33� f3� �3� �3� �3�  f� 3f� ff� �f� �f� �f�  �� 3�� f�� ��� ̙� ���  ̙ 3̙ f̙ �̙ �̙ �̙  �� 3�� f�� ��� ��� ���   � 3 � f � � � � � � �  3� 33� f3� �3� �3� �3�  f� 3f� ff� �f� �f� �f�  �� 3�� f�� ��� ̙� ���  �� 3�� f�� ��� ��� ���  �� 3�� f�� ��� ��� ���   � 3 � f � � � � � � �  3� 33� f3� �3� �3� �3�  f� 3f� ff� �f� �f� �f�  �� 3�� f�� ��� ̙� ���  �� 3�� f�� ��� ��� ���  �� 3�� f�� ��� ��� ���       ((( 555 CCC PPP ]]] kkk xxx ��� ��� ��� ��� ��� ��� ��� ��� ��� ���                                                                                 ������������������������������������������������������������������������������������������������������������������������������������������������������������������������������涶��������������������������������������������������������������������������������������������������������U�ڣ����OO�����U�ڣ����OO����涶������OO�����U�ڣ����OO�����U�ڣ����OO�����U�ڣζ���O�����U�ڣ����OO�����U�ڣ����OO�����U�ڣ����OO������U��O����O������U��O����O�����涶������O������U��O����O������U��O����O������U��O�����������U��O����O������U��O����O������U��O����Oڶ�����U���y���O������U���y���O�����涶��y���O������U���y���O������U���y���O������U���y����������U���y���O������U���y���O������U���y���Oڶ�����U����OOs�������U����OOs�������U����OOs�������U����OOs�������U����OOs�������U����OOs�������U����OOs�������U����OOs�������U����OOs�������U�� $N��  ����涶��$N��  �����U�� $N��  �����U�� $N��  �����U�� $���� �����U�� $N��  �����U�� $N��  �����U�� $N��  ������U�� $N��  �����U���NsO�  ����涶��NsO�  �����U���NsO�  �����U���NsO�  �����U���N���� �����U���NsO�  �����U���NsO�  �����U���NsO�  ������U���NsO�  �����U��*yyy�  ����涶��yyy�  �����U��*yyy�  �����U��*yyy�  �����U��*y���� �����U��*yyy�  �����U��*yyy�  �����U��*yyy�  ������U��*yyy�  �����U��O��yN ���U�涶����yN ���U��U��O��yN ���U��U��O��yN ���U��U��O��������U��U��O��yN ���U��U��O��yN ���U��U��O��yN ٶ�����U��O��yN ���U��U�zO��sO ��ހ��U�zO��sO ��ހ��U�zO��sO ��ހ��U�zO��sO ��ހ��U�zO��sO ��ހ��U�zO��sO ��ހ��U�zO��sO ��ހ��U�zO��sO ��ހ��U�zO��sO ��ހ�涶���Ν���z����U��z�Ν���z����U��z�Ν���z����U��zȶ����z����U��z�Ν���z����U��z�Ν���z����U��z�Ν��ܶ�����U��z�Ν���z����U��z�Ν���z���涶����y��y�����U�����y��y�����U�����y��y�����U��������y�����U�����y��y�����U�����y��y�����U�����y��y������U�����y��y�����U�����y��y����涶��y���U������U���y���U������U���y���U������U���y����������U���y���U������U���y���U������U���y���U�������U���y���U������U���y���U�����涶�����������U�� ������������U�� ������������U�� ���ܶ�������U�� ������������U�� ������������U�� ��������ܶ����� ������������U�� ������������U������������������������������������������������������������������������������������������������������������������������������������������������檪�����s  <?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           targetNamespace="http://v8.1c.ru/8.2/data/geo"
           xmlns:tns="http://v8.1c.ru/8.2/data/geo"
           xmlns:ui="http://v8.1c.ru/8.1/data/ui"
           xmlns:core="http://v8.1c.ru/8.1/data/core"
           attributeFormDefault="unqualified"
           elementFormDefault="qualified">

    <xs:import namespace="http://v8.1c.ru/8.1/data/core" schemaLocation="../../../xdto/src/res/core.xsd"/>
    <xs:import namespace="http://v8.1c.ru/8.1/data/ui" schemaLocation="../../../xdto/src/res/uiobjects.xsd"/>

    <xs:simpleType name="GeographicalSchemaShowMode">
        <xs:restriction base="xs:string">
            <xs:enumeration value="ScaleDefined"/>
            <xs:enumeration value="AllData"/>
            <xs:enumeration value="SpecifiedArea"/>
        </xs:restriction>
    </xs:simpleType>


    <xs:simpleType name="GeographicalSchemaLineType">
        <xs:restriction base="xs:string">
            <xs:enumeration value="None"/>
            <xs:enumeration value="Solid"/>
            <xs:enumeration value="Dotted"/>
            <xs:enumeration value="Dashed"/>
            <xs:enumeration value="DashDotted"/>
            <xs:enumeration value="DashDottedDotted"/>
        </xs:restriction>
    </xs:simpleType>

    <xs:simpleType name="GeographicalSchemaProjection">
        <xs:restriction base="xs:string">
            <xs:enumeration value="CylindricalMillerProjection"/>
            <xs:enumeration value="CylindricalLambertEqualAreaProjection"/>
            <xs:enumeration value="CylindricalGallStereographicProjection"/>
            <xs:enumeration value="CylindricalEquidistantProjection"/>

            <xs:enumeration value="PseudoCylindricalSinusoidalProjection"/>
            <xs:enumeration value="PseudoCylindricalMollweideProjection"/>
            <xs:enumeration value="PseudoCylindricalRobinsonProjection"/>
            <xs:enumeration value="PseudoCylindricalEckert1Projection"/>
            <xs:enumeration value="PseudoCylindricalEckert2Projection"/>
            <xs:enumeration value="PseudoCylindricalEckert3Projection"/>
            <xs:enumeration value="PseudoCylindricalEckert4Projection"/>
            <xs:enumeration value="PseudoCylindricalEckert5Projection"/>
            <xs:enumeration value="PseudoCylindricalEckert6Projection"/>
            <xs:enumeration value="PseudoCylindricalHatanoAsymetricalEqualAreaProjection"/>
            <xs:enumeration value="PseudoCylindricalLoximutalProjection"/>
            <xs:enumeration value="PseudoCylindricalMcBrydeThomasFlatPolarParabolicProjection"/>
            <xs:enumeration value="PseudoCylindricalMcBrydeThomasFlatPolarQuarticProjection"/>
            <xs:enumeration value="PseudoCylindricalMcBrydeThomasFlatPolarSinusoidalProjection"/>
            <xs:enumeration value="PseudoCylindricalPutninP2Projection"/>
            <xs:enumeration value="PseudoCylindricalPutninP5Projection"/>
            <xs:enumeration value="PseudoCylindricalWinkel1Projection"/>
            <xs:enumeration value="PseudoCylindricalBoggsEumorphicProjection"/>

            <xs:enumeration value="AzimuthalHammerProjection"/>
            <xs:enumeration value="AzimuthalWagner7Projection"/>
            <xs:enumeration value="AzimuthalAitoffProjection"/>
            <xs:enumeration value="AzimuthalWinkelTripelProjection"/>
            <xs:enumeration value="AzimuthalEquidistantProjection"/>
            <xs:enumeration value="AzimuthalLambertEqualAreaProjection"/>

            <xs:enumeration value="MiscellaneousAugustEpicycloidalProjection"/>
            <xs:enumeration value="MiscellaneousBaconGlobularProjection"/>
            <xs:enumeration value="MiscellaneousNicolosiGlobularProjection"/>
            <xs:enumeration value="MiscellaneousApianGlobular1Projection"/>
            <xs:enumeration value="MiscellaneousOrteliusOvalProjection"/>
            <xs:enumeration value="MiscellaneousVanDerGrinten1Projection"/>
            <xs:enumeration value="MiscellaneousVanDerGrinten2Projection"/>
            <xs:enumeration value="MiscellaneousVanDerGrinten3Projection"/>

            <xs:enumeration value="ConicLambertEqualAreaProjection"/>
        </xs:restriction>
    </xs:simpleType>
    
    <xs:simpleType name="GeographicalSchemaDataSourceOrganizationType">
        <xs:restriction base="xs:string">
            <xs:enumeration value="AtRow"/>
            <xs:enumeration value="AtIntersection"/>
        </xs:restriction>
    </xs:simpleType>

    <!-- ShapeCoordinateSystemInfo -->
    <xs:complexType name="scsi">
        <xs:sequence>
          <!-- gcsName -->
          <xs:element name="name" type="xs:string"/>
          <!-- datumName -->
          <xs:element name="dName" type="xs:string"/>
          <!-- spheroidName -->
          <xs:element name="sName" type="xs:string"/>

          <!-- semimajorAxis -->
          <xs:element name="smjAxs" type="xs:decimal"/>
          <!-- semiminorAxis -->
          <xs:element name="smnAxs" type="xs:decimal"/>
          <!-- inverseFlattening -->
          <xs:element name="invfl" type="xs:decimal"/>

          <!-- angularUnitName -->
          <xs:element name="aun" type="xs:string"/>
          <!-- radiansPerUnit -->
          <xs:element name="rpu" type="xs:decimal"/>

          <!-- primeMeridianName -->
          <xs:element name="pmn" type="xs:string"/>
          <!-- primeMeridianLongitudee -->
          <xs:element name="pml" type="xs:decimal"/>
        </xs:sequence>
    </xs:complexType>
    
    <xs:simpleType name="GeographicalSchemaLayerSeriesShowMode">
        <xs:restriction base="xs:string">
            <xs:enumeration value="DontShow"/>
            <xs:enumeration value="Text"/>
            <xs:enumeration value="Column"/>
            <xs:enumeration value="ShapeSize"/>
            <xs:enumeration value="ShapeColor"/>
            <xs:enumeration value="ShapeColorHue"/>
            <xs:enumeration value="Picture"/>
            <xs:enumeration value="Pie"/>
            <xs:enumeration value="SizedPie"/>
        </xs:restriction>
    </xs:simpleType>

    <xs:simpleType name="SeriesValuesDrawingMode">
        <xs:restriction base="xs:string">
            <xs:enumeration value="ShowAsValue"/>
            <xs:enumeration value="ShowAsPart"/>
        </xs:restriction>
    </xs:simpleType>

    <!-- DataSeriesItem -->
    <xs:complexType name="dsi">
        <xs:sequence>
            <!-- figureBaseColor -->
            <xs:element name="bc" type="ui:Color" minOccurs="0"/>
            <!-- textColor -->
            <xs:element name="tc" type="ui:Color" minOccurs="0"/>
            <!-- textFont -->
            <xs:element name="tf" type="ui:Font" minOccurs="0"/>
            <!-- value -->
            <xs:element name="v" type="xs:anyType" maxOccurs="1" minOccurs="0"/>
        </xs:sequence>
        <!-- objectID - идентификатор объекта -->
        <xs:attribute name="oi" type="xs:decimal"/>
        <!-- name -->
        <xs:attribute name="n" type="xs:string"/>
        <!-- text -->
        <xs:attribute name="t" type="xs:string"/>
        <!-- format -->
        <xs:attribute name="f" type="xs:string"/>
        <!-- drawingType-->
        <xs:attribute name="dt" type="tns:GeographicalSchemaLayerSeriesShowMode"/>
        <!-- group -->
        <xs:attribute name="g" type="xs:string"/>
        <!-- textOrientation -->
        <xs:attribute name="to" type="xs:decimal"/>
        <!-- valueDrawingType -->
        <xs:attribute name="vdt" type="tns:SeriesValuesDrawingMode"/>
    </xs:complexType>

    <!-- LayerObject -->
    <xs:complexType name="lobj">
        <xs:sequence>
            <!-- datails -->
            <xs:element name="det" type="xs:anyType" maxOccurs="1" minOccurs="0"/>
            <!-- value -->
            <xs:element name="val" type="xs:anyType" maxOccurs="1" minOccurs="0"/>
        </xs:sequence>

        <!-- objectID - идентификатор объекта -->
        <xs:attribute name="oi" type="xs:decimal"/>
        <!-- toolTip -->
        <xs:attribute name="tt" type="xs:string"/>
        <!-- showLinkedData -->
        <xs:attribute name="sld" type="xs:boolean"/>
        <!-- visibke -->
        <xs:attribute name="vis" type="xs:boolean"/>
        <!-- xMinOriginal -->
        <xs:attribute name="xn" type="xs:decimal"/>
        <!-- xMaxOriginal -->
        <xs:attribute name="xx" type="xs:decimal"/>
        <!-- yMinOriginal -->
        <xs:attribute name="yn" type="xs:decimal"/>
        <!-- yMaxOriginal -->
        <xs:attribute name="yx" type="xs:decimal"/>

        <!-- drawed xMin -->
        <xs:attribute name="dxn" type="xs:decimal"/>
        <!-- drawed xMax -->
        <xs:attribute name="dxx" type="xs:decimal"/>
        <!-- drawed yMin -->
        <xs:attribute name="dyn" type="xs:decimal"/>
        <!-- drawed yMax -->
        <xs:attribute name="dyx" type="xs:decimal"/>

    </xs:complexType>

    <xs:simpleType name="GeographicalSchemaMarkerType">
        <xs:restriction base="xs:string">
            <xs:enumeration value="None"/>
            <xs:enumeration value="Pin"/>
            <xs:enumeration value="Darts"/>
            <xs:enumeration value="LittleCircle"/>
            <xs:enumeration value="BigCircle"/>
            <xs:enumeration value="LittleSquare"/>
            <xs:enumeration value="BigSquare"/>
            <xs:enumeration value="LittleTriangle"/>
            <xs:enumeration value="BigTriangle"/>
            <xs:enumeration value="QuestionMark"/>
            <xs:enumeration value="ExclamationPoint"/>
        </xs:restriction>
    </xs:simpleType>

    <xs:simpleType name="PaintingReferencePointPosition">
        <xs:restriction base="xs:string">
            <xs:enumeration value="LeftTop"/>
            <xs:enumeration value="LeftCenter"/>
            <xs:enumeration value="LeftBottom"/>
            <xs:enumeration value="CenterTop"/>
            <xs:enumeration value="Center"/>
            <xs:enumeration value="CenterBottom"/>
            <xs:enumeration value="RightTop"/>
            <xs:enumeration value="RightCenter"/>
            <xs:enumeration value="RightBottom"/>
        </xs:restriction>
    </xs:simpleType>

    <xs:simpleType name="GeographicalSchemaPointObjectDrawingType">
        <xs:restriction base="xs:string">
            <xs:enumeration value="Char"/>
            <xs:enumeration value="Marker"/>
            <xs:enumeration value="Picture"/>
        </xs:restriction>
    </xs:simpleType>

    <!-- ps - PointShape -->
    <xs:complexType name="ps">
        <xs:complexContent>
            <xs:extension base="tns:lobj">
                <xs:sequence>
                    <!-- color -->
                    <xs:element name="clr" type="ui:Color" minOccurs="0"/>
                    <!-- font -->
                    <xs:element name="fnt" type="ui:Font" minOccurs="0"/>
                    <!-- picture -->
                    <xs:element name="pic" type="ui:Picture" minOccurs="0"/>
                </xs:sequence>
                <!-- xOriginal -->
                <xs:attribute name="x" type="xs:decimal"/>
                <!-- yOriginal -->
                <xs:attribute name="y" type="xs:decimal"/>
                <!-- drawed x -->
                <xs:attribute name="a" type="xs:decimal"/>
                <!-- drawed y -->
                <xs:attribute name="b" type="xs:decimal"/>
                <!-- symbol -->
                <xs:attribute name="smbl" type="xs:string"/>
                <!-- pushPin -->
                <xs:attribute name="pn" type="tns:GeographicalSchemaMarkerType"/>
                <!-- pointPosition-->
                <xs:attribute name="pp" type="tns:PaintingReferencePointPosition"/>
                <!-- drawingType -->
                <xs:attribute name="dt" type="tns:GeographicalSchemaPointObjectDrawingType"/>
            </xs:extension>
        </xs:complexContent>
    </xs:complexType>

    <!-- sps - SimplePointShape -->
    <xs:complexType name="sps">
        <!-- objectID - идентификатор объекта -->
        <xs:attribute name="i" type="xs:decimal"/>
        <!-- xOriginal -->
        <xs:attribute name="x" type="xs:decimal"/>
        <!-- yOriginal -->
        <xs:attribute name="y" type="xs:decimal"/>
        <!-- drawed x -->
        <xs:attribute name="a" type="xs:decimal"/>
        <!-- drawed y -->
        <xs:attribute name="b" type="xs:decimal"/>
    </xs:complexType>

    <!-- dp - drawed point -->
    <xs:complexType name="dp">
        <!-- drawed x -->
        <xs:attribute name="x" type="xs:decimal"/>
        <!-- drawed y -->
        <xs:attribute name="y" type="xs:decimal"/>
    </xs:complexType>

    <!-- dps - drawed points -->
    <xs:complexType name="dps">
      <xs:sequence>
        <!-- drawed point -->
        <xs:element name="p" type="tns:dp" minOccurs="0" maxOccurs="unbounded"/>
      </xs:sequence>
    </xs:complexType>

    <!-- mps - MultiPointShape -->
    <xs:complexType name="mps">
        <xs:complexContent>
            <xs:extension base="tns:lobj">
                <xs:sequence>
                    <!-- point -->
                    <xs:element name="pnt" type="tns:sps" minOccurs="0" maxOccurs="unbounded"/>
                    <!-- drawed point -->
                    <xs:element name="dps" type="tns:dps" minOccurs="0" maxOccurs="unbounded"/>
                    <!-- color -->
                    <xs:element name="clr" type="ui:Color" minOccurs="0"/>
                    <!-- font -->
                    <xs:element name="fnt" type="ui:Font" minOccurs="0"/>
                    <!-- picture -->
                    <xs:element name="pic" type="ui:Picture" minOccurs="0"/>
                </xs:sequence>
                <!-- symbol -->
                <xs:attribute name="smbl" type="xs:string"/>
                <!-- pushPin -->
                <xs:attribute name="pn" type="tns:GeographicalSchemaMarkerType"/>
                <!-- pointPosition-->
                <xs:attribute name="pp" type="tns:PaintingReferencePointPosition"/>
                <!-- drawingType -->
                <xs:attribute name="dt" type="tns:GeographicalSchemaPointObjectDrawingType"/>
              </xs:extension>
        </xs:complexContent>
    </xs:complexType>

    <!-- IdentifiedPointsShapeVector -->
    <xs:complexType name="ipsv">
        <xs:sequence>
            <!-- point -->
            <xs:element name="p" type="tns:sps" minOccurs="0" maxOccurs="unbounded"/>
        </xs:sequence>
        <!-- objectID - идентификатор объекта -->
        <xs:attribute name="i" type="xs:decimal"/>
    </xs:complexType>

    <!-- pls - PolyLineShape -->
    <xs:complexType name="pls">
        <xs:complexContent>
            <xs:extension base="tns:lobj">
                <xs:sequence>
                    <!-- color -->
                    <xs:element name="clr" type="ui:Color" minOccurs="0"/>
                    <!-- line -->
                    <xs:element name="ln" type="ui:Line" minOccurs="0"/>
                    <!-- segment -->
                    <xs:element name="sg" type="tns:ipsv" maxOccurs="unbounded" minOccurs="0"/>
                    <!-- drawed points -->
                    <xs:element name="dps" type="tns:dps" maxOccurs="unbounded" minOccurs="0"/>
                </xs:sequence>
            </xs:extension>
        </xs:complexContent>
    </xs:complexType>

    <!-- ps - PolygonShape -->
    <xs:complexType name="pgs">
        <xs:complexContent>
            <xs:extension base="tns:lobj">
                <xs:sequence>
                    <!-- color -->
                    <xs:element name="clr" type="ui:Color" minOccurs="0"/>
                    <!-- borderColor -->
                    <xs:element name="bClr" type="ui:Color" minOccurs="0"/>
                    <!-- contour -->
                    <xs:element name="cnt" type="tns:ipsv" maxOccurs="unbounded" minOccurs="0"/>
                    <!-- drawed points -->
                    <xs:element name="dps" type="tns:dps" maxOccurs="unbounded" minOccurs="0"/>
                </xs:sequence>
            </xs:extension>
        </xs:complexContent>
    </xs:complexType>

    <!-- mpcs - MultiPatchShape -->
    <!--
    <xs:complexType name="mpcs">
        <xs:complexContent>
            <xs:extension base="tns:lobj">
                <xs:sequence>
                    <xs:element name="point" type="tns:sps" maxOccurs="unbounded" minOccurs="0"/>
                    <xs:element name="part" type="xs:decimal" maxOccurs="unbounded" minOccurs="0"/>
                    <xs:element name="partType" type="xs:decimal" maxOccurs="unbounded" minOccurs="0"/>
                    <xs:element name="zRange" type="xs:decimal" maxOccurs="unbounded" minOccurs="0"/>
                    <xs:element name="zArray" type="xs:decimal" maxOccurs="unbounded" minOccurs="0"/>
                    <xs:element name="mRange" type="xs:decimal" maxOccurs="unbounded" minOccurs="0"/>
                    <xs:element name="mArray" type="xs:decimal" maxOccurs="unbounded" minOccurs="0"/>
                </xs:sequence>
            </xs:extension>
        </xs:complexContent>
    </xs:complexType>
    -->

    <!-- ValueItem -->
    <xs:complexType name="vi">
        <xs:sequence>
            <!-- value -->
            <xs:element name="v" type="xs:anyType" maxOccurs="1" minOccurs="0"/>
            <!-- details -->
            <xs:element name="d" type="xs:anyType" maxOccurs="1" minOccurs="0"/>
            <!-- drawed point -->
            <xs:element name="p" type="tns:dp" minOccurs="0" maxOccurs="unbounded"/>
            <!-- percents -->
            <xs:element name="r" type="xs:decimal" minOccurs="0" maxOccurs="unbounded"/>
        </xs:sequence>
        <!-- objectID - идентификатор объекта -->
        <xs:attribute name="i" type="xs:decimal"/>
        <!-- tooltip -->
        <xs:attribute name="t" type="xs:string"/>
        <!-- value -->
        <xs:attribute name="vs" type="xs:string"/>
        <xs:attribute name="vn" type="xs:decimal"/>
        <xs:attribute name="vb" type="xs:boolean"/>
        <xs:attribute name="vd" type="xs:dateTime"/>
        <!-- details -->
        <xs:attribute name="ds" type="xs:string"/>
        <xs:attribute name="dn" type="xs:decimal"/>
        <xs:attribute name="db" type="xs:boolean"/>
        <xs:attribute name="dd" type="xs:dateTime"/>
    </xs:complexType>

    <!--sad - SeriesAndData -->
    <xs:complexType name="sad">
        <xs:sequence>
            <!--vi - данные -->
            <xs:element name="v" type="tns:vi"/>
        </xs:sequence>
        <!--si - идентификатор серии -->
        <xs:attribute name="i" type="xs:decimal"/>
    </xs:complexType>

    <!--oas - ObjectAndSeries -->
    <xs:complexType name="oas">
        <xs:sequence>
            <!--si - даныне объекта -->
            <xs:element name="d" type="tns:sad" maxOccurs="unbounded" minOccurs="1"/>
        </xs:sequence>
        <!--si - идентификатор объекта-->
        <xs:attribute name="i" type="xs:decimal"/>
    </xs:complexType>

    <!-- LayerItem -->
    <xs:complexType name="layer">
        <xs:sequence>
            <!-- gsc -->
            <xs:element name="gsc" type="tns:scsi"/>
            <!-- series-->
            <xs:element name="series" type="tns:dsi" maxOccurs="unbounded" minOccurs="0"/>
            <!-- point -->
            <xs:element name="pnt" type="tns:ps" maxOccurs="unbounded" minOccurs="0"/>
            <!-- multipoint-->
            <xs:element name="mpnt" type="tns:mps" maxOccurs="unbounded" minOccurs="0"/>
            <!-- polyline-->
            <xs:element name="pln" type="tns:pls" maxOccurs="unbounded" minOccurs="0"/>
            <!-- polyfon-->
            <xs:element name="plgn" type="tns:pgs" maxOccurs="unbounded" minOccurs="0"/>
            <!-- multipatch
            <xs:element name="mPtch" type="tns:mpcs" maxOccurs="unbounded" minOccurs="0"/>
            -->
            <!-- objectData -->
            <xs:element name="data" type="tns:oas" maxOccurs="unbounded" minOccurs="0"/>
        </xs:sequence>

        <!-- objectID - идентификатор объекта -->
        <xs:attribute name="oi" type="xs:decimal"/>
        <!-- name -->
        <xs:attribute name="name" type="xs:string"/>
        <!-- text -->
        <xs:attribute name="text" type="xs:string"/>

        <!-- xMinOriginal -->
        <xs:attribute name="xmn" type="xs:decimal"/>
        <!-- xMaxOriginal -->
        <xs:attribute name="xmx" type="xs:decimal"/>
        <!-- yMinOriginal -->
        <xs:attribute name="ymn" type="xs:decimal"/>
        <!-- yMaxOriginal -->
        <xs:attribute name="ymx" type="xs:decimal"/>

        <!-- visible -->
        <xs:attribute name="vis" type="xs:boolean"/>
        <!-- selectable -->
        <xs:attribute name="sel" type="xs:boolean"/>

        <!-- scaleRangeBegin-->
        <xs:attribute name="srb" type="xs:decimal"/>
        <!-- scaleRangeEnd -->
        <xs:attribute name="sre" type="xs:decimal"/>

        <!-- dataSourceRange -->
        <xs:attribute name="dsr" type="xs:string"/>
        <!-- contentType -->
        <xs:attribute name="ct" type="tns:GeographicalSchemaDataSourceOrganizationType"/>

        <!-- elementsType -->
        <xs:attribute name="et" type="core:UUID"/>
    </xs:complexType>

    <!-- PrintOption -->
    <xs:complexType name="po">
        <xs:sequence>
            <!-- value -->
            <xs:element name="val" type="xs:anyType" minOccurs="0"/>
        </xs:sequence>
        <!-- key -->
        <xs:attribute name="key" type="xs:decimal"/>
    </xs:complexType>

    <xs:simpleType name="GeographicalSchemaLegendItemShowScaleType">
        <xs:restriction base="xs:string">
            <xs:enumeration value="DontShow"/>
            <xs:enumeration value="ShowByValues"/>
        </xs:restriction>
    </xs:simpleType>

    <!-- GeographicalSchemeLegendItem -->
    <xs:complexType name="legItem">
        <xs:sequence>
            <!-- sampleTextColor -->
            <xs:element name="stc" type="ui:Color" minOccurs="0"/>
            <!-- sampleTextFont -->
            <xs:element name="stf" type="ui:Font" minOccurs="0"/>
            <!-- picture -->
            <xs:element name="pic" type="ui:Picture" minOccurs="0"/>
        </xs:sequence>
        <!-- objectID - идентификатор объекта -->
        <xs:attribute name="oi" type="xs:decimal"/>
        <!-- LayerObjectID - идентификатор объекта -->
        <xs:attribute name="loi" type="xs:decimal"/>
        <!-- ItemObjectID - идентификатор серии слоя  -->
        <xs:attribute name="soi" type="xs:decimal"/>
        <!-- sampleText -->
        <xs:attribute name="st" type="xs:string"/>
        <!-- captionText -->
        <xs:attribute name="ct" type="xs:string"/>
        <!-- scaleType -->
        <xs:attribute name="sct" type="tns:GeographicalSchemaLegendItemShowScaleType"/>
    </xs:complexType>

    <!-- GeographicalSchemeLegend -->
    <xs:complexType name="legend">
        <xs:sequence>
            <!-- color -->
            <xs:element name="clr" type="ui:Color" minOccurs="0"/>
            <!-- border -->
            <xs:element name="brdr" type="ui:Border" minOccurs="0"/>
            <!-- borderColor -->
            <xs:element name="brdrClr" type="ui:Color" minOccurs="0"/>
            <!-- borderColor -->
            <xs:element name="fnt" type="ui:Font" minOccurs="0"/>
            <!-- legendBackColor -->
            <xs:element name="bkClr" type="ui:Color" minOccurs="0"/>
            <!-- item -->
            <xs:element name="item" type="tns:legItem" maxOccurs="unbounded" minOccurs="0"/>
        </xs:sequence>
        <!-- legendAreaShow -->
        <xs:attribute name="show" type="xs:boolean"/>
        <!-- legendTransparent -->
        <xs:attribute name="trnsprnt" type="xs:boolean"/>
        <!-- legendShowScaleLine -->
        <xs:attribute name="scale" type="xs:boolean"/>
        <!-- rectLeft -->
        <xs:attribute name="left" type="xs:decimal"/>
        <!-- rectRight -->
        <xs:attribute name="right" type="xs:decimal"/>
        <!-- rectTop -->
        <xs:attribute name="top" type="xs:decimal"/>
        <!-- rectBottom -->
        <xs:attribute name="bottom" type="xs:decimal"/>
    </xs:complexType>

    <!-- GeographicalSchemeDrawingArea -->
    <xs:complexType name="drawing">
        <xs:sequence>
            <!-- border -->
            <xs:element name="brdr" type="ui:Border" minOccurs="0"/>
            <!-- backColor -->
            <xs:element name="bkClr" type="ui:Color" minOccurs="0"/>
            <!-- color -->
            <xs:element name="clr" type="ui:Color" minOccurs="0"/>
        </xs:sequence>
        <!-- transparent -->
        <xs:attribute name="trnsprnt" type="xs:boolean"/>
        <!-- rectLeft -->
        <xs:attribute name="left" type="xs:decimal"/>
        <!-- rectRight -->
        <xs:attribute name="right" type="xs:decimal"/>
        <!-- rectTop -->
        <xs:attribute name="top" type="xs:decimal"/>
        <!-- rectBottom -->
        <xs:attribute name="bottom" type="xs:decimal"/>
    </xs:complexType>

    <!-- GeographicalSchemeTitle -->
    <xs:complexType name="title">
        <xs:sequence>
            <!-- border-->
            <xs:element name="brdr" type="ui:Border" minOccurs="0"/>
            <!-- borderColor-->
            <xs:element name="brdrClr" type="ui:Color" minOccurs="0"/>
            <!-- font-->
            <xs:element name="fnt" type="ui:Font" minOccurs="0"/>
            <!-- font -->
            <xs:element name="txt" type="core:LocalStringType" minOccurs="0"/>
            <!-- textColor -->
            <xs:element name="txtClr" type="ui:Color" minOccurs="0"/>
            <!-- titleBackColor-->
            <xs:element name="bckClr" type="ui:Color" minOccurs="0"/>
        </xs:sequence>
        <!-- titleAreaShow-->
        <xs:attribute name="show" type="xs:boolean"/>
        <!-- align -->
        <xs:attribute name="algn" type="ui:HorizontalAlign"/>
        <!-- titleTransparent -->
        <xs:attribute name="trnsprnt" type="xs:boolean"/>
        <!-- rectLeft -->
        <xs:attribute name="left" type="xs:decimal"/>
        <!-- rectRight -->
        <xs:attribute name="right" type="xs:decimal"/>
        <!-- rectTop -->
        <xs:attribute name="top" type="xs:decimal"/>
        <!-- rectBottom -->
        <xs:attribute name="bottom" type="xs:decimal"/>
    </xs:complexType>

    <xs:complexType name="GeographicalSchema">
        <xs:sequence>
            <xs:element name="pic" type="ui:Picture" minOccurs="0" maxOccurs="1"/>
            <!-- -->
            <xs:element name="layer" type="tns:layer" minOccurs="0" maxOccurs="unbounded"/>
            <!-- -->
            <xs:element name="po" type="tns:po" minOccurs="0" maxOccurs="unbounded"/>
            <!-- -->
            <xs:element name="legend" type="tns:legend"/>
            <!-- -->
            <xs:element name="title" type="tns:title"/>
            <!-- -->
            <xs:element name="drawing" type="tns:drawing"/>
        </xs:sequence>
        <!-- currentID -->
        <xs:attribute name="curId" type="xs:decimal"/>
        <!-- latitudeShift -->
        <xs:attribute name="latShift" type="xs:decimal"/>
        <!-- longitudeShift -->
        <xs:attribute name="longShift" type="xs:decimal"/>
        <!-- schemeProjection -->
        <xs:attribute name="proj" type="tns:GeographicalSchemaProjection"/>
        <!-- showMode -->
        <xs:attribute name="showMode" type="tns:GeographicalSchemaShowMode"/>
        <!-- scale -->
        <xs:attribute name="scale" type="xs:decimal"/>

        <!-- xMinOriginal -->
        <xs:attribute name="xmn" type="xs:decimal"/>
        <!-- xMaxOriginal -->
        <xs:attribute name="xmx" type="xs:decimal"/>
        <!-- yMinOriginal -->
        <xs:attribute name="ymn" type="xs:decimal"/>
        <!-- yMaxOriginal -->
        <xs:attribute name="ymx" type="xs:decimal"/>

        <!-- drawed xMin -->
        <xs:attribute name="dxn" type="xs:decimal"/>
        <!-- drawed xMax -->
        <xs:attribute name="dxx" type="xs:decimal"/>
        <!-- drawed yMin -->
        <xs:attribute name="dyn" type="xs:decimal"/>
        <!-- drawed yMax -->
        <xs:attribute name="dyx" type="xs:decimal"/>

        <!-- viewed xMin -->
        <xs:attribute name="vxn" type="xs:decimal"/>
        <!-- viewed xMax -->
        <xs:attribute name="vxx" type="xs:decimal"/>
        <!-- viewed yMin -->
        <xs:attribute name="vyn" type="xs:decimal"/>
        <!-- viewed yMax -->
        <xs:attribute name="vyx" type="xs:decimal"/>

        <!-- m_useOutput по умолчанию Auto -->
        <xs:attribute name="useOutput" type="ui:UseOutput" />

    </xs:complexType>

</xs:schema>
�W  ﻿<model xmlns="http://v8.1c.ru/8.1/xdto" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<package targetNamespace="http://v8.1c.ru/8.2/data/geo" elementFormQualified="true" attributeFormQualified="false">
		<import namespace="http://v8.1c.ru/8.1/data/core"/>
		<import namespace="http://v8.1c.ru/8.1/data/ui"/>
		<valueType name="GeographicalSchemaDataSourceOrganizationType" base="xs:string">
			<enumeration>AtRow</enumeration>
			<enumeration>AtIntersection</enumeration>
		</valueType>
		<valueType name="GeographicalSchemaLayerSeriesShowMode" base="xs:string">
			<enumeration>DontShow</enumeration>
			<enumeration>Text</enumeration>
			<enumeration>Column</enumeration>
			<enumeration>ShapeSize</enumeration>
			<enumeration>ShapeColor</enumeration>
			<enumeration>ShapeColorHue</enumeration>
			<enumeration>Picture</enumeration>
			<enumeration>Pie</enumeration>
			<enumeration>SizedPie</enumeration>
		</valueType>
		<valueType name="GeographicalSchemaLegendItemShowScaleType" base="xs:string">
			<enumeration>DontShow</enumeration>
			<enumeration>ShowByValues</enumeration>
		</valueType>
		<valueType name="GeographicalSchemaLineType" base="xs:string">
			<enumeration>None</enumeration>
			<enumeration>Solid</enumeration>
			<enumeration>Dotted</enumeration>
			<enumeration>Dashed</enumeration>
			<enumeration>DashDotted</enumeration>
			<enumeration>DashDottedDotted</enumeration>
		</valueType>
		<valueType name="GeographicalSchemaMarkerType" base="xs:string">
			<enumeration>None</enumeration>
			<enumeration>Pin</enumeration>
			<enumeration>Darts</enumeration>
			<enumeration>LittleCircle</enumeration>
			<enumeration>BigCircle</enumeration>
			<enumeration>LittleSquare</enumeration>
			<enumeration>BigSquare</enumeration>
			<enumeration>LittleTriangle</enumeration>
			<enumeration>BigTriangle</enumeration>
			<enumeration>QuestionMark</enumeration>
			<enumeration>ExclamationPoint</enumeration>
		</valueType>
		<valueType name="GeographicalSchemaPointObjectDrawingType" base="xs:string">
			<enumeration>Char</enumeration>
			<enumeration>Marker</enumeration>
			<enumeration>Picture</enumeration>
		</valueType>
		<valueType name="GeographicalSchemaProjection" base="xs:string">
			<enumeration>CylindricalMillerProjection</enumeration>
			<enumeration>CylindricalLambertEqualAreaProjection</enumeration>
			<enumeration>CylindricalGallStereographicProjection</enumeration>
			<enumeration>CylindricalEquidistantProjection</enumeration>
			<enumeration>PseudoCylindricalSinusoidalProjection</enumeration>
			<enumeration>PseudoCylindricalMollweideProjection</enumeration>
			<enumeration>PseudoCylindricalRobinsonProjection</enumeration>
			<enumeration>PseudoCylindricalEckert1Projection</enumeration>
			<enumeration>PseudoCylindricalEckert2Projection</enumeration>
			<enumeration>PseudoCylindricalEckert3Projection</enumeration>
			<enumeration>PseudoCylindricalEckert4Projection</enumeration>
			<enumeration>PseudoCylindricalEckert5Projection</enumeration>
			<enumeration>PseudoCylindricalEckert6Projection</enumeration>
			<enumeration>PseudoCylindricalHatanoAsymetricalEqualAreaProjection</enumeration>
			<enumeration>PseudoCylindricalLoximutalProjection</enumeration>
			<enumeration>PseudoCylindricalMcBrydeThomasFlatPolarParabolicProjection</enumeration>
			<enumeration>PseudoCylindricalMcBrydeThomasFlatPolarQuarticProjection</enumeration>
			<enumeration>PseudoCylindricalMcBrydeThomasFlatPolarSinusoidalProjection</enumeration>
			<enumeration>PseudoCylindricalPutninP2Projection</enumeration>
			<enumeration>PseudoCylindricalPutninP5Projection</enumeration>
			<enumeration>PseudoCylindricalWinkel1Projection</enumeration>
			<enumeration>PseudoCylindricalBoggsEumorphicProjection</enumeration>
			<enumeration>AzimuthalHammerProjection</enumeration>
			<enumeration>AzimuthalWagner7Projection</enumeration>
			<enumeration>AzimuthalAitoffProjection</enumeration>
			<enumeration>AzimuthalWinkelTripelProjection</enumeration>
			<enumeration>AzimuthalEquidistantProjection</enumeration>
			<enumeration>AzimuthalLambertEqualAreaProjection</enumeration>
			<enumeration>MiscellaneousAugustEpicycloidalProjection</enumeration>
			<enumeration>MiscellaneousBaconGlobularProjection</enumeration>
			<enumeration>MiscellaneousNicolosiGlobularProjection</enumeration>
			<enumeration>MiscellaneousApianGlobular1Projection</enumeration>
			<enumeration>MiscellaneousOrteliusOvalProjection</enumeration>
			<enumeration>MiscellaneousVanDerGrinten1Projection</enumeration>
			<enumeration>MiscellaneousVanDerGrinten2Projection</enumeration>
			<enumeration>MiscellaneousVanDerGrinten3Projection</enumeration>
			<enumeration>ConicLambertEqualAreaProjection</enumeration>
		</valueType>
		<valueType name="GeographicalSchemaShowMode" base="xs:string">
			<enumeration>ScaleDefined</enumeration>
			<enumeration>AllData</enumeration>
			<enumeration>SpecifiedArea</enumeration>
		</valueType>
		<valueType name="PaintingReferencePointPosition" base="xs:string">
			<enumeration>LeftTop</enumeration>
			<enumeration>LeftCenter</enumeration>
			<enumeration>LeftBottom</enumeration>
			<enumeration>CenterTop</enumeration>
			<enumeration>Center</enumeration>
			<enumeration>CenterBottom</enumeration>
			<enumeration>RightTop</enumeration>
			<enumeration>RightCenter</enumeration>
			<enumeration>RightBottom</enumeration>
		</valueType>
		<valueType name="SeriesValuesDrawingMode" base="xs:string">
			<enumeration>ShowAsValue</enumeration>
			<enumeration>ShowAsPart</enumeration>
		</valueType>
		<objectType name="GeographicalSchema">
			<property name="curId" type="xs:decimal" lowerBound="0" form="Attribute"/>
			<property name="latShift" type="xs:decimal" lowerBound="0" form="Attribute"/>
			<property name="longShift" type="xs:decimal" lowerBound="0" form="Attribute"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.2/data/geo" name="proj" type="d4p1:GeographicalSchemaProjection" lowerBound="0" form="Attribute"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.2/data/geo" name="showMode" type="d4p1:GeographicalSchemaShowMode" lowerBound="0" form="Attribute"/>
			<property name="scale" type="xs:decimal" lowerBound="0" form="Attribute"/>
			<property name="xmn" type="xs:decimal" lowerBound="0" form="Attribute"/>
			<property name="xmx" type="xs:decimal" lowerBound="0" form="Attribute"/>
			<property name="ymn" type="xs:decimal" lowerBound="0" form="Attribute"/>
			<property name="ymx" type="xs:decimal" lowerBound="0" form="Attribute"/>
			<property name="dxn" type="xs:decimal" lowerBound="0" form="Attribute"/>
			<property name="dxx" type="xs:decimal" lowerBound="0" form="Attribute"/>
			<property name="dyn" type="xs:decimal" lowerBound="0" form="Attribute"/>
			<property name="dyx" type="xs:decimal" lowerBound="0" form="Attribute"/>
			<property name="vxn" type="xs:decimal" lowerBound="0" form="Attribute"/>
			<property name="vxx" type="xs:decimal" lowerBound="0" form="Attribute"/>
			<property name="vyn" type="xs:decimal" lowerBound="0" form="Attribute"/>
			<property name="vyx" type="xs:decimal" lowerBound="0" form="Attribute"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.1/data/ui" name="useOutput" type="d4p1:UseOutput" lowerBound="0" form="Attribute"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.1/data/ui" name="pic" type="d4p1:Picture" lowerBound="0"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.2/data/geo" name="layer" type="d4p1:layer" lowerBound="0" upperBound="-1"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.2/data/geo" name="po" type="d4p1:po" lowerBound="0" upperBound="-1"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.2/data/geo" name="legend" type="d4p1:legend"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.2/data/geo" name="title" type="d4p1:title"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.2/data/geo" name="drawing" type="d4p1:drawing"/>
		</objectType>
		<objectType name="dp">
			<property name="x" type="xs:decimal" lowerBound="0" form="Attribute"/>
			<property name="y" type="xs:decimal" lowerBound="0" form="Attribute"/>
		</objectType>
		<objectType name="dps">
			<property xmlns:d4p1="http://v8.1c.ru/8.2/data/geo" name="p" type="d4p1:dp" lowerBound="0" upperBound="-1"/>
		</objectType>
		<objectType name="drawing">
			<property name="trnsprnt" type="xs:boolean" lowerBound="0" form="Attribute"/>
			<property name="left" type="xs:decimal" lowerBound="0" form="Attribute"/>
			<property name="right" type="xs:decimal" lowerBound="0" form="Attribute"/>
			<property name="top" type="xs:decimal" lowerBound="0" form="Attribute"/>
			<property name="bottom" type="xs:decimal" lowerBound="0" form="Attribute"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.1/data/ui" name="brdr" type="d4p1:Border" lowerBound="0"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.1/data/ui" name="bkClr" type="d4p1:Color" lowerBound="0"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.1/data/ui" name="clr" type="d4p1:Color" lowerBound="0"/>
		</objectType>
		<objectType name="dsi">
			<property name="oi" type="xs:decimal" lowerBound="0" form="Attribute"/>
			<property name="n" type="xs:string" lowerBound="0" form="Attribute"/>
			<property name="t" type="xs:string" lowerBound="0" form="Attribute"/>
			<property name="f" type="xs:string" lowerBound="0" form="Attribute"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.2/data/geo" name="dt" type="d4p1:GeographicalSchemaLayerSeriesShowMode" lowerBound="0" form="Attribute"/>
			<property name="g" type="xs:string" lowerBound="0" form="Attribute"/>
			<property name="to" type="xs:decimal" lowerBound="0" form="Attribute"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.2/data/geo" name="vdt" type="d4p1:SeriesValuesDrawingMode" lowerBound="0" form="Attribute"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.1/data/ui" name="bc" type="d4p1:Color" lowerBound="0"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.1/data/ui" name="tc" type="d4p1:Color" lowerBound="0"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.1/data/ui" name="tf" type="d4p1:Font" lowerBound="0"/>
			<property name="v" lowerBound="0"/>
		</objectType>
		<objectType name="ipsv">
			<property name="i" type="xs:decimal" lowerBound="0" form="Attribute"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.2/data/geo" name="p" type="d4p1:sps" lowerBound="0" upperBound="-1"/>
		</objectType>
		<objectType name="layer">
			<property name="oi" type="xs:decimal" lowerBound="0" form="Attribute"/>
			<property name="name" type="xs:string" lowerBound="0" form="Attribute"/>
			<property name="text" type="xs:string" lowerBound="0" form="Attribute"/>
			<property name="xmn" type="xs:decimal" lowerBound="0" form="Attribute"/>
			<property name="xmx" type="xs:decimal" lowerBound="0" form="Attribute"/>
			<property name="ymn" type="xs:decimal" lowerBound="0" form="Attribute"/>
			<property name="ymx" type="xs:decimal" lowerBound="0" form="Attribute"/>
			<property name="vis" type="xs:boolean" lowerBound="0" form="Attribute"/>
			<property name="sel" type="xs:boolean" lowerBound="0" form="Attribute"/>
			<property name="srb" type="xs:decimal" lowerBound="0" form="Attribute"/>
			<property name="sre" type="xs:decimal" lowerBound="0" form="Attribute"/>
			<property name="dsr" type="xs:string" lowerBound="0" form="Attribute"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.2/data/geo" name="ct" type="d4p1:GeographicalSchemaDataSourceOrganizationType" lowerBound="0" form="Attribute"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.1/data/core" name="et" type="d4p1:UUID" lowerBound="0" form="Attribute"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.2/data/geo" name="gsc" type="d4p1:scsi"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.2/data/geo" name="series" type="d4p1:dsi" lowerBound="0" upperBound="-1"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.2/data/geo" name="pnt" type="d4p1:ps" lowerBound="0" upperBound="-1"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.2/data/geo" name="mpnt" type="d4p1:mps" lowerBound="0" upperBound="-1"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.2/data/geo" name="pln" type="d4p1:pls" lowerBound="0" upperBound="-1"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.2/data/geo" name="plgn" type="d4p1:pgs" lowerBound="0" upperBound="-1"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.2/data/geo" name="data" type="d4p1:oas" lowerBound="0" upperBound="-1"/>
		</objectType>
		<objectType name="legItem">
			<property name="oi" type="xs:decimal" lowerBound="0" form="Attribute"/>
			<property name="loi" type="xs:decimal" lowerBound="0" form="Attribute"/>
			<property name="soi" type="xs:decimal" lowerBound="0" form="Attribute"/>
			<property name="st" type="xs:string" lowerBound="0" form="Attribute"/>
			<property name="ct" type="xs:string" lowerBound="0" form="Attribute"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.2/data/geo" name="sct" type="d4p1:GeographicalSchemaLegendItemShowScaleType" lowerBound="0" form="Attribute"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.1/data/ui" name="stc" type="d4p1:Color" lowerBound="0"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.1/data/ui" name="stf" type="d4p1:Font" lowerBound="0"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.1/data/ui" name="pic" type="d4p1:Picture" lowerBound="0"/>
		</objectType>
		<objectType name="legend">
			<property name="show" type="xs:boolean" lowerBound="0" form="Attribute"/>
			<property name="trnsprnt" type="xs:boolean" lowerBound="0" form="Attribute"/>
			<property name="scale" type="xs:boolean" lowerBound="0" form="Attribute"/>
			<property name="left" type="xs:decimal" lowerBound="0" form="Attribute"/>
			<property name="right" type="xs:decimal" lowerBound="0" form="Attribute"/>
			<property name="top" type="xs:decimal" lowerBound="0" form="Attribute"/>
			<property name="bottom" type="xs:decimal" lowerBound="0" form="Attribute"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.1/data/ui" name="clr" type="d4p1:Color" lowerBound="0"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.1/data/ui" name="brdr" type="d4p1:Border" lowerBound="0"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.1/data/ui" name="brdrClr" type="d4p1:Color" lowerBound="0"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.1/data/ui" name="fnt" type="d4p1:Font" lowerBound="0"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.1/data/ui" name="bkClr" type="d4p1:Color" lowerBound="0"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.2/data/geo" name="item" type="d4p1:legItem" lowerBound="0" upperBound="-1"/>
		</objectType>
		<objectType name="lobj">
			<property name="oi" type="xs:decimal" lowerBound="0" form="Attribute"/>
			<property name="tt" type="xs:string" lowerBound="0" form="Attribute"/>
			<property name="sld" type="xs:boolean" lowerBound="0" form="Attribute"/>
			<property name="vis" type="xs:boolean" lowerBound="0" form="Attribute"/>
			<property name="xn" type="xs:decimal" lowerBound="0" form="Attribute"/>
			<property name="xx" type="xs:decimal" lowerBound="0" form="Attribute"/>
			<property name="yn" type="xs:decimal" lowerBound="0" form="Attribute"/>
			<property name="yx" type="xs:decimal" lowerBound="0" form="Attribute"/>
			<property name="dxn" type="xs:decimal" lowerBound="0" form="Attribute"/>
			<property name="dxx" type="xs:decimal" lowerBound="0" form="Attribute"/>
			<property name="dyn" type="xs:decimal" lowerBound="0" form="Attribute"/>
			<property name="dyx" type="xs:decimal" lowerBound="0" form="Attribute"/>
			<property name="det" lowerBound="0"/>
			<property name="val" lowerBound="0"/>
		</objectType>
		<objectType xmlns:d3p1="http://v8.1c.ru/8.2/data/geo" name="mps" base="d3p1:lobj">
			<property name="smbl" type="xs:string" lowerBound="0" form="Attribute"/>
			<property name="pn" type="d3p1:GeographicalSchemaMarkerType" lowerBound="0" form="Attribute"/>
			<property name="pp" type="d3p1:PaintingReferencePointPosition" lowerBound="0" form="Attribute"/>
			<property name="dt" type="d3p1:GeographicalSchemaPointObjectDrawingType" lowerBound="0" form="Attribute"/>
			<property name="pnt" type="d3p1:sps" lowerBound="0" upperBound="-1"/>
			<property name="dps" type="d3p1:dps" lowerBound="0" upperBound="-1"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.1/data/ui" name="clr" type="d4p1:Color" lowerBound="0"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.1/data/ui" name="fnt" type="d4p1:Font" lowerBound="0"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.1/data/ui" name="pic" type="d4p1:Picture" lowerBound="0"/>
		</objectType>
		<objectType name="oas">
			<property name="i" type="xs:decimal" lowerBound="0" form="Attribute"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.2/data/geo" name="d" type="d4p1:sad" upperBound="-1"/>
		</objectType>
		<objectType xmlns:d3p1="http://v8.1c.ru/8.2/data/geo" name="pgs" base="d3p1:lobj">
			<property xmlns:d4p1="http://v8.1c.ru/8.1/data/ui" name="clr" type="d4p1:Color" lowerBound="0"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.1/data/ui" name="bClr" type="d4p1:Color" lowerBound="0"/>
			<property name="cnt" type="d3p1:ipsv" lowerBound="0" upperBound="-1"/>
			<property name="dps" type="d3p1:dps" lowerBound="0" upperBound="-1"/>
		</objectType>
		<objectType xmlns:d3p1="http://v8.1c.ru/8.2/data/geo" name="pls" base="d3p1:lobj">
			<property xmlns:d4p1="http://v8.1c.ru/8.1/data/ui" name="clr" type="d4p1:Color" lowerBound="0"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.1/data/ui" name="ln" type="d4p1:Line" lowerBound="0"/>
			<property name="sg" type="d3p1:ipsv" lowerBound="0" upperBound="-1"/>
			<property name="dps" type="d3p1:dps" lowerBound="0" upperBound="-1"/>
		</objectType>
		<objectType name="po">
			<property name="key" type="xs:decimal" lowerBound="0" form="Attribute"/>
			<property name="val" lowerBound="0"/>
		</objectType>
		<objectType xmlns:d3p1="http://v8.1c.ru/8.2/data/geo" name="ps" base="d3p1:lobj">
			<property name="x" type="xs:decimal" lowerBound="0" form="Attribute"/>
			<property name="y" type="xs:decimal" lowerBound="0" form="Attribute"/>
			<property name="a" type="xs:decimal" lowerBound="0" form="Attribute"/>
			<property name="b" type="xs:decimal" lowerBound="0" form="Attribute"/>
			<property name="smbl" type="xs:string" lowerBound="0" form="Attribute"/>
			<property name="pn" type="d3p1:GeographicalSchemaMarkerType" lowerBound="0" form="Attribute"/>
			<property name="pp" type="d3p1:PaintingReferencePointPosition" lowerBound="0" form="Attribute"/>
			<property name="dt" type="d3p1:GeographicalSchemaPointObjectDrawingType" lowerBound="0" form="Attribute"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.1/data/ui" name="clr" type="d4p1:Color" lowerBound="0"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.1/data/ui" name="fnt" type="d4p1:Font" lowerBound="0"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.1/data/ui" name="pic" type="d4p1:Picture" lowerBound="0"/>
		</objectType>
		<objectType name="sad">
			<property name="i" type="xs:decimal" lowerBound="0" form="Attribute"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.2/data/geo" name="v" type="d4p1:vi"/>
		</objectType>
		<objectType name="scsi">
			<property name="name" type="xs:string"/>
			<property name="dName" type="xs:string"/>
			<property name="sName" type="xs:string"/>
			<property name="smjAxs" type="xs:decimal"/>
			<property name="smnAxs" type="xs:decimal"/>
			<property name="invfl" type="xs:decimal"/>
			<property name="aun" type="xs:string"/>
			<property name="rpu" type="xs:decimal"/>
			<property name="pmn" type="xs:string"/>
			<property name="pml" type="xs:decimal"/>
		</objectType>
		<objectType name="sps">
			<property name="i" type="xs:decimal" lowerBound="0" form="Attribute"/>
			<property name="x" type="xs:decimal" lowerBound="0" form="Attribute"/>
			<property name="y" type="xs:decimal" lowerBound="0" form="Attribute"/>
			<property name="a" type="xs:decimal" lowerBound="0" form="Attribute"/>
			<property name="b" type="xs:decimal" lowerBound="0" form="Attribute"/>
		</objectType>
		<objectType name="title">
			<property name="show" type="xs:boolean" lowerBound="0" form="Attribute"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.1/data/ui" name="algn" type="d4p1:HorizontalAlign" lowerBound="0" form="Attribute"/>
			<property name="trnsprnt" type="xs:boolean" lowerBound="0" form="Attribute"/>
			<property name="left" type="xs:decimal" lowerBound="0" form="Attribute"/>
			<property name="right" type="xs:decimal" lowerBound="0" form="Attribute"/>
			<property name="top" type="xs:decimal" lowerBound="0" form="Attribute"/>
			<property name="bottom" type="xs:decimal" lowerBound="0" form="Attribute"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.1/data/ui" name="brdr" type="d4p1:Border" lowerBound="0"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.1/data/ui" name="brdrClr" type="d4p1:Color" lowerBound="0"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.1/data/ui" name="fnt" type="d4p1:Font" lowerBound="0"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.1/data/core" name="txt" type="d4p1:LocalStringType" lowerBound="0"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.1/data/ui" name="txtClr" type="d4p1:Color" lowerBound="0"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.1/data/ui" name="bckClr" type="d4p1:Color" lowerBound="0"/>
		</objectType>
		<objectType name="vi">
			<property name="i" type="xs:decimal" lowerBound="0" form="Attribute"/>
			<property name="t" type="xs:string" lowerBound="0" form="Attribute"/>
			<property name="vs" type="xs:string" lowerBound="0" form="Attribute"/>
			<property name="vn" type="xs:decimal" lowerBound="0" form="Attribute"/>
			<property name="vb" type="xs:boolean" lowerBound="0" form="Attribute"/>
			<property name="vd" type="xs:dateTime" lowerBound="0" form="Attribute"/>
			<property name="ds" type="xs:string" lowerBound="0" form="Attribute"/>
			<property name="dn" type="xs:decimal" lowerBound="0" form="Attribute"/>
			<property name="db" type="xs:boolean" lowerBound="0" form="Attribute"/>
			<property name="dd" type="xs:dateTime" lowerBound="0" form="Attribute"/>
			<property name="v" lowerBound="0"/>
			<property name="d" lowerBound="0"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.2/data/geo" name="p" type="d4p1:dp" lowerBound="0" upperBound="-1"/>
			<property name="r" type="xs:decimal" lowerBound="0" upperBound="-1"/>
		</objectType>
	</package>
</model>������   �PNG

   IHDR   `      �z�   UIDATX��Ա� EQ�`Vr�5�-����7&�	�ؒ4}�C  @   �r��4�W���߼�}��v:���-�    � ���i��|nD?    IEND�B`���������������6
  BM6
      6  (   `                                   3   f   �   �   �    3  33  f3  �3  �3  �3   f  3f  ff  �f  �f  �f   �  3�  f�  ��  ̙  ��   �  3�  f�  ��  ��  ��   �  3�  f�  ��  ��  ��    3 3 3 f 3 � 3 � 3 � 3  33 333 f33 �33 �33 �33  f3 3f3 ff3 �f3 �f3 �f3  �3 3�3 f�3 ��3 ̙3 ��3  �3 3�3 f�3 ��3 ��3 ��3  �3 3�3 f�3 ��3 ��3 ��3   f 3 f f f � f � f � f  3f 33f f3f �3f �3f �3f  ff 3ff fff �ff �ff �ff  �f 3�f f�f ��f ̙f ��f  �f 3�f f�f ��f ��f ��f  �f 3�f f�f ��f ��f ��f   � 3 � f � � � � � � �  3� 33� f3� �3� �3� �3�  f� 3f� ff� �f� �f� �f�  �� 3�� f�� ��� ̙� ���  ̙ 3̙ f̙ �̙ �̙ �̙  �� 3�� f�� ��� ��� ���   � 3 � f � � � � � � �  3� 33� f3� �3� �3� �3�  f� 3f� ff� �f� �f� �f�  �� 3�� f�� ��� ̙� ���  �� 3�� f�� ��� ��� ���  �� 3�� f�� ��� ��� ���   � 3 � f � � � � � � �  3� 33� f3� �3� �3� �3�  f� 3f� ff� �f� �f� �f�  �� 3�� f�� ��� ̙� ���  �� 3�� f�� ��� ��� ���  �� 3�� f�� ��� ��� ���       ((( 555 CCC PPP ]]] kkk xxx ��� ��� ��� ��� ��� ��� ��� ��� ��� ���                                                                                 ������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������ת��;4{4]�3��3>(S(�*3%G�'��'* I "�!�!!"�� w	�����ct�M+� � � ;�%*t� �#C"Yp�*�)�)�)� N +-$�#�#b'�'P$�#�$	$�#+�3��h+�i(�'e# ##c""�"�"�"B#h �!Q!m!�!���J,�  �&(�1W�}��r � �>��[��#vh[<x����:��O����&?
s���
?�(o
7B����{)I�j.A�!�+�*� �,g k;3���'��h�$n$�$E��2�2�2�}*

B4��$\&:&%�%~&&�%$)H	��
�
i=$
)]�	f*��3j���*��*�	����� :`��	�=����3fG���0!3�0I3x3�0�0�,�,�,h,�11M1}1�12B2v2K0�-�-.=.m.�.�./5/b/�/N-�/�/~--05 �?+�	��_6t��6K����\5}R*[	�5,�'�'�+W��R��$��'�Fd�J������w��3��"\�$'G*�(D'�(�\l[�&��� � i )�(�+�+��D)%J%D*!� ���%^tM���&���O4�4\4�44%404l4�  �! � `� `  `�# `C  `� `9 `� `� `� `� `H `� `�% ` `u `� `� `�  ` `� `W `  `� `� `� `O  ` `, `3 `  `+  `� `� `� `2 `D `d  `a `r  `� `k  `�  `� `� `  `H `c `� `S `� `�  `f `� `b `� `G `� `=  `  `� `B
 `7  `�! `B `# `h# ` `R `/ `�$ `�  `I$ `� `�$ `) `� ` `�	 ` ` `� `� `g! `� `r `� `� `j `_	 ` `� `X `k `' `� `E	 `�	 `� `� `� `; `d  `&  `� `  `T `� `� `� `� `� `C `&  `�  `z `%
 `� `� `Y `�  `�  `=  `� `M `�  `�  `� `=  `u `W `! `�  `n `1  `�  `� ` `  `; `) ` `y  `�  ` `&  `�% ` `�  `&  `  `�  `�  `k `� `! ` ` `V  `� `� `f ` `!  `&  `� `  `T `1  `z `% `1  `�  `+	 `� `?& `� `V `x `5% `�  `]  `  `I  `v `,! `~
 `E	 `� `� `p `� `� `` `  `�	 `2 `� ` `J `�  `o `& `V `� `	 `� `� ` `q `� `� `� `� `�& `� `!  `� `� `C `&  `�  `� `� `�  `� `1 `� `� `r  `1  `. `=  `� `� `� `� `�  `�  `� `; ` ` `% `� `` `� ` `�
 `� `�	 `� `{	 `� `� `�
 `$ `� `` `5 ``
 `� `W `� `f `c `� ` `: `� `R `y `� `� `� ` `�! `� `�" `�" `$# ` ` `* `< `� `+ `E	 `r  `� `� ` `� `� `� `  `� `� `� `� `  `�  `: `�  ` `� `b `> `  `U ` `C `
 `
 `s `� `| `� `� `_" `�  `� `� `e `� `� `�# `� `/ ` `� ` `4 `� `b `� ` `� `y `� `y `\ `? `T `7 `� `` `� `� `H `s `� `� `� `� ` `" `M `# `� `}  `2 `j `�  `�  `W `H `  `� ` `X `� `� `s `�
 `E	 `5 `� `�
 `B `E	 `	 `� `� `E  `3 `� `7 `� `� ` `E `� `� `� `�( +) �) �* ?. ?K ;a ca 