  �'      ResB             I1     r  �2  �2  �       �  IDS_ACCOUNT_CORRESPONDENCE_KIND_DT IDS_ACCOUNT_CORRESPONDENCE_KIND_CT IDS_ACCOUNT_ACCOUNT_KIND_ACTIVE IDS_ACCOUNT_ACCOUNT_KIND_PASSIVE IDS_ACCOUNT_ACCOUNT_KIND_ACTIVE_PASSIVE IDS_CALC_REG_SD_RP_PRESENTATION IDS_CALC_REG_SD_AP_PRESENTATION IDS_CALC_REG_SD_AAP_PRESENTATION IDS_CALC_REG_SD_BP_PRESENTATION IDS_COMPARISONKIND_EQUEL IDS_COMPARISONKIND_NOTEQUEL IDS_COMPARISONKIND_LESS IDS_COMPARISONKIND_LESSOREQUAL IDS_COMPARISONKIND_MORE IDS_COMPARISONKIND_MOREOREQUEL IDS_COMPARISONKIND_INTERVAL IDS_COMPARISONKIND_INTERVALINCLUDINGBORDERS IDS_COMPARISONKIND_INTERVALINCLUDINGFROMBORDER IDS_COMPARISONKIND_INTERVALINCLUDINGTILLBORDER IDS_COMPARISONKIND_CONTAIN IDS_COMPARISONKIND_NOTCONTAIN IDS_COMPARISONKIND_IN IDS_COMPARISONKIND_INHIERARCHY IDS_COMPARISONKIND_NOTIN IDS_COMPARISONKIND_NOTINHIERARCHY IDS_COMPARISONKIND_INGROUP IDS_COMPARISONKIND_NOTINGROUP IDS_COMPARISONKIND_BEGINSWITH IDS_COMPARISONKIND_NOTBEGINSWITH IDS_COMPARISONKIND_LIKE IDS_COMPARISONKIND_NOTLIKE IDS_MESSAGESTATUS_MARKER_NONE IDS_MESSAGESTATUS_MARKER_ORDINARY IDS_MESSAGESTATUS_MARKER_INFORMATION IDS_MESSAGESTATUS_MARKER_ATTENTION IDS_MESSAGESTATUS_MARKER_IMPORTANT IDS_MESSAGESTATUS_MARKER_VERY_IMPORTANT IDS_DATALOCKMODE_SHARED IDS_DATALOCKMODE_EXCLUSIVE IDS_ABFVNORMAL IDS_ABFVLARGE IDS_AIVOLD IDS_AIV833 IDS_FSVAUTO IDS_FSVNORMAL IDS_FSVCOMPACT IDS_DATA_FILL_CHECK_NONE IDS_DATA_FILL_CHECK_ERROR IDS_DATA_FILL_CHECK_WARNING IDS_ACCUM_REG_CORRESPONDENCE_KIND_DT IDS_ACCUM_REG_CORRESPONDENCE_KIND_CT IDS_ACCUMREG_AGGREGATEUSE IDS_ACCREG_AGGREGATEUSE_AUTO IDS_ACCREG_AGGREGATEUSE_ALWAYS IDS_ACCUMREG_AGGREGATEPERIODICITY IDS_ACCREG_AGGREGATEPERIODICITY_NONE IDS_ACCREG_AGGREGATEPERIODICITY_AUTO IDS_ACCREG_AGGREGATEPERIODICITY_DAY IDS_ACCREG_AGGREGATEPERIODICITY_MONTH IDS_ACCREG_AGGREGATEPERIODICITY_QUARTER IDS_ACCREG_AGGREGATEPERIODICITY_HALFYEAR IDS_ACCREG_AGGREGATEPERIODICITY_YEAR IDS_VALUEWRITINGMODE_PLAIN IDS_VALUEWRITINGMODE_POST IDS_VALUEWRITINGMODE_UNPOST IDS_VALUEPOSTINGMODE_NOTOPERATIONAL IDS_VALUEPOSTINGMODE_OPERATIONAL IDS_VALUEAUTOTIMEMODE_NONE IDS_VALUEAUTOTIMEMODE_LAST IDS_VALUEAUTOTIMEMODE_FIRST IDS_VALUEAUTOTIMEMODE_CURRENTORLAST IDS_VALUEAUTOTIMEMODE_CURRENTORFIRST IDS_VALUESSLICEUSING_NONE IDS_VALUESSLICEUSING_FIRST IDS_VALUESSLICEUSING_LAST IDS_FFC_DO_NOT_CHECK IDS_FFC_CHECK_ON_WRITE IDS_VALUEUSEPOSTINGMODE_NOTOPERATIONAL IDS_VALUEUSEPOSTINGMODE_OPERATIONAL IDS_VALUEUSEPOSTINGMODE_CHOICE IDS_VALUEUSEPOSTINGMODE_AUTO IDS_TASK_LIST_MODE_ALL_TASKS IDS_TASK_LIST_MODE_BY_PERFORMER IDS_DATALOCKMODEENUM_SHARED IDS_DATALOCKMODEENUM_EXCLUSIVE IDS_TRANSACTIONSTARTDATALOCKMODE_AUTOMATIC IDS_TRANSACTIONSTARTDATALOCKMODE_CONTROLLED IDS_CLIENT_CONNECTION_SPEED_NORMAL IDS_CLIENT_CONNECTION_SPEED_LOW IDS_DATAANALYSIS_DATASOURCECOLUMNDATAKIND_CONTIGUOUS IDS_DATAANALYSIS_DATASOURCECOLUMNDATAKIND_DISCRETE IDS_DATAANALYSISCLUSTERIZATIONMETHODTYPE_KMEANS IDS_DATAANALYSISCLUSTERIZATIONMETHODTYPE_NEARESTNEIGHBOR IDS_DATAANALYSISCLUSTERIZATIONMETHODTYPE_FURTHESTNEIGHBOR IDS_DATAANALYSISCLUSTERIZATIONMETHODTYPE_CENTROID IDS_DATAANALYSISCLUSTERIZATIONMETHODTYPE_WARD IDS_DATAANALYSISTIMEINTERVALUNITTYPE_SECOND IDS_DATAANALYSISTIMEINTERVALUNITTYPE_CURRENTMINUTE IDS_DATAANALYSISTIMEINTERVALUNITTYPE_MINUTE IDS_DATAANALYSISTIMEINTERVALUNITTYPE_CURRENTHOUR IDS_DATAANALYSISTIMEINTERVALUNITTYPE_HOUR IDS_DATAANALYSISTIMEINTERVALUNITTYPE_CURRENTDAY IDS_DATAANALYSISTIMEINTERVALUNITTYPE_DAY IDS_DATAANALYSISTIMEINTERVALUNITTYPE_CURRENTWEEK IDS_DATAANALYSISTIMEINTERVALUNITTYPE_WEEK IDS_DATAANALYSISTIMEINTERVALUNITTYPE_CURRENTMONTH IDS_DATAANALYSISTIMEINTERVALUNITTYPE_MONTH IDS_DATAANALYSISTIMEINTERVALUNITTYPE_CURRENTQUARTER IDS_DATAANALYSISTIMEINTERVALUNITTYPE_QUARTER IDS_DATAANALYSISTIMEINTERVALUNITTYPE_CURRENTYEAR IDS_DATAANALYSISTIMEINTERVALUNITTYPE_YEAR IDS_DATAANALYSISTIMEINTERVALUNITTYPE_CURRENTDECADE IDS_DATAANALYSISTIMEINTERVALUNITTYPE_DECADE IDS_DATAANALYSISTIMEINTERVALUNITTYPE_CURRENTHALFYEAR IDS_DATAANALYSISTIMEINTERVALUNITTYPE_HALFYEAR IDS_DATAANALYSISRESULTTABLEFILLTYPE_NOTFILL IDS_DATAANALYSISRESULTTABLEFILLTYPE_KEYFIELDS IDS_DATAANALYSISRESULTTABLEFILLTYPE_USEDFIELDS IDS_DATAANALYSISRESULTTABLEFILLTYPE_ALLFIELDS IDS_DATAANALYSIS_DATAANALYSISNUMERICVALUESUSETYPE_ASNUMERIC IDS_DATAANALYSIS_DATAANALYSISNUMERICVALUESUSETYPE_ASBOOLEAN IDS_DATAANALYSERDATASOURCETYPEASSOCIATION_TRANSACTIONAL IDS_DATAANALYSERDATASOURCETYPEASSOCIATION_OBJECT IDS_DATAANALYSIS_DATASOURCECOLUMNTYPEDECISIONTREE_NOTUSED IDS_DATAANALYSIS_DATASOURCECOLUMNTYPEDECISIONTREE_INPUT IDS_DATAANALYSIS_DATASOURCECOLUMNTYPEDECISIONTREE_PREDICTABLE IDS_DATAANALYSERDATASOURCECOLUMNTYPECLUSTERIZATION_NOTUSED IDS_DATAANALYSERDATASOURCECOLUMNTYPECLUSTERIZATION_INPUTCOLUMN IDS_DATAANALYSERDATASOURCECOLUMNTYPECLUSTERIZATION_KEYCOLUMN IDS_DATAANALYSERDATASOURCECOLUMNTYPECLUSTERIZATION_PREDICTABLECOLUMN IDS_DATAANALYSERDATASOURCECOLUMNTYPECLUSTERIZATION_INPUTANDPREDICTABLECOLUMN IDS_DATAANALYSERDATASOURCECOLUMNTYPESUMMARYSTATISTICS_NOTUSED IDS_DATAANALYSERDATASOURCECOLUMNTYPESUMMARYSTATISTICS_INPUT IDS_DATAANALYSERDATASOURCECOLUMNTYPEASSOCIATION_NOTUSED IDS_DATAANALYSERDATASOURCECOLUMNTYPEASSOCIATION_TRANSACTIONID IDS_DATAANALYSERDATASOURCECOLUMNTYPEASSOCIATION_ELEMENTID IDS_DATAANALYSERDATASOURCECOLUMNTYPESEQUENTIALPATTERNS_NOTUSED IDS_DATAANALYSERDATASOURCECOLUMNTYPESEQUENTIALPATTERNS_ELEMENTID IDS_DATAANALYSERDATASOURCECOLUMNTYPESEQUENTIALPATTERNS_SEQUENCEID IDS_DATAANALYSERDATASOURCECOLUMNTYPESEQUENTIALPATTERNS_TRANSACTIONTIME IDS_DATAANALYSIS_PREDICTIONCOLUMNTYPE_INPUT IDS_DATAANALYSIS_PREDICTIONCOLUMNTYPE_PREDICT IDS_DATAANALYSIS_PREDICTIONCOLUMNTYPE_DATASOURCE IDS_DATAANALYSISDISTANCEMETRICTYPE_SQUAREDEUCLIDEAN IDS_DATAANALYSISDISTANCEMETRICTYPE_EUCLIDEAN IDS_DATAANALYSISDISTANCEMETRICTYPE_MANHATTAN IDS_DATAANALYSISDISTANCEMETRICTYPE_SUPREMUM IDS_DATAANALYSERASSOCIATIONRULESPRUNETYPE_REDUNDANT IDS_DATAANALYSERASSOCIATIONRULESPRUNETYPE_COVERED IDS_DATAANALYSISSTANDARDIZETYPE_NOTSTANDARDIZE IDS_DATAANALYSISSTANDARDIZETYPE_STANDARDIZE IDS_DATAANALYSERDECISIONTREENODESPRUNETYPE_NOTPRUNE IDS_DATAANALYSERDECISIONTREENODESPRUNETYPE_PRUNE IDS_DATAANALYSERASSOCIATIONRULESORDERTYPE_BYSUPPORT IDS_DATAANALYSERASSOCIATIONRULESORDERTYPE_BYCONFIDENCE IDS_DATAANALYSERASSOCIATIONRULESORDERTYPE_BYIMPORTANCE IDS_DATAANALYSERSEQUENTIALPATTERNSORDERTYPE_BYSUPPORT IDS_DATAANALYSERSEQUENTIALPATTERNSORDERTYPE_BYLENGTH IDS_DATAANALYSIS_DATASOURCECOLUMNTYPE_NOTUSED IDS_VALUEEDITUSAGE_FORITEMUSAGE IDS_VALUEEDITUSAGE_FORFOLDERUSAGE IDS_VALUEEDITUSAGE_FORBOTHUSAGE IDS_UPDATEONDATACHANGES_AUTO IDS_UPDATEONDATACHANGES_DONOTUPDATE IDS_DATAANALYSIS_GROUPNAME IDS_LINKEDVALUECHANGEMODE_CLEAR IDS_LINKEDVALUECHANGEMODE_DONTCHANGE IDS_BOUNDKINDENUMVALUES_INCLUDING IDS_BOUNDKINDENUMVALUES_EXCLUDING IDS_RESULTCOMPOSITIONTYPEAUTOVAL IDS_RESULTCOMPOSITIONTYPEBACKGROUNDVAL IDS_RESULTCOMPOSITIONTYPEDIRECTLYVAL IDS_SHOWSTATEMODEAUTOVAL IDS_SHOWSTATEMODEDONTSHOWVAL IDS_SHOWSTATEMODESHOWVAL IDS_SHOWSTATEMODESHOWONCOMPOSITIONVAL IDS_INCLSIGNERCERT_DONTINCLUDE IDS_INCLSIGNERCERT_INCLSIGNERCERT IDS_INCLSIGNERCERT_INCLCHAINWOROOT IDS_INCLSIGNERCERT_INCLWHOLECHAIN IDS_CHECKCERTMODE_IGNORETIMEVALIDITY IDS_CHECKCERTMODE_IGNORESIGNATUREVALIDITY IDS_CHECKCERTMODE_IGNOREREVOCATIONSTATUS IDS_CHECKCERTMODE_ALLOWTESTCERTIFICATES IDS_CERTSTORETYPE_PERSONAL IDS_CERTSTORETYPE_ADDRESSBOOK IDS_CERTSTORETYPE_CERTAUTHORITY IDS_CERTSTORETYPE_ROOT IDS_STORESTORAGETYPE_USER IDS_STORESTORAGETYPE_COMPUTER IDS_STORESTORAGETYPE_APPLICATION IDS_INTERACTIVEMODETYPE_USE IDS_INTERACTIVEMODETYPE_DONTUSE IDS_UPDATEDBCFGINFOELEMTYPE_INFO IDS_UPDATEDBCFGINFOELEMTYPE_WARNING IDS_UPDATEDBCFGINFOELEMTYPE_ERROR IDS_UPDATEDBCFGINFOSTATE_NOTACTIVE IDS_UPDATEDBCFGINFOSTATE_INITIAL IDS_UPDATEDBCFGINFOSTATE_UPDATE IDS_EDBTRANSACTIONSISOLATIONLEVEL_AUTOMATIC IDS_EDBTRANSACTIONSISOLATIONLEVEL_READUNCOMMITTED IDS_EDBTRANSACTIONSISOLATIONLEVEL_READCOMMITTED IDS_EDBTRANSACTIONSISOLATIONLEVEL_REPEATABLEREAD IDS_EDBTRANSACTIONSISOLATIONLEVEL_SERIALIZABLE IDS_UPDATE_PREDEFINED_DATA_MODE_ENUM IDS_UPDATE_PREDEFINED_DATA_MODE_ENUM_AUTO IDS_UPDATE_PREDEFINED_DATA_MODE_ENUM_ON IDS_UPDATE_PREDEFINED_DATA_MODE_ENUM_OFF IDS_PLURALSTYPE_CARDINAL IDS_PLURALSTYPE_ORDINAL IDS_DATAHISTORY_DATAROWCHANGETYPE_CREATE IDS_DATAHISTORY_DATAROWCHANGETYPE_UPDATE IDS_DATAHISTORY_DATAROWCHANGETYPE_DELETE IDS_DATAHISTORY_DATAROWCHANGETYPE_MOVE IDS_LOGFORMDATACHANGETYPE_ADD IDS_LOGFORMDATACHANGETYPE_CHANGE IDS_LOGFORMDATACHANGETYPE_DELETE IDS_UNS_INFORMATION IDS_UNS_IMPORTANT IDS_CAAS_CONNECTED IDS_CAAS_DISCONNECTED IDS_CAAS_NOTSTARTED IDS_REPORTRESULTVIEWMODEAUTOVAL IDS_REPORTRESULTVIEWMODEDEFAULTVAL IDS_REPORTRESULTVIEWMODECOMPACTVAL model.xdto enums.xsd DataAnalysis.xsd   K e y   D a y   L o w   U s e   A d d   H o u r   Y e a r   A u t o   I t e m   W e e k   T a x i   M o v e   N o n e   T i m e   L i k e   L a s t   S h o w   F i r s t   P r u n e   I n p u t   I t e m s   M o n t h   E v e n t   E r r o r   Q u e r y   D e b i t   L a r g e   C l e a r   C h a n g e   C r e a t e   D e l e t e   M i n u t e   A c t i v e   C r e d i t   N o r m a l   O b j e c t   A l w a y s   S e c o n d   S h a r e d   R e c o r d   W a r n i n g   O r d i n a l   E x p e n s e   M a x i m u m   R e g u l a r   Q u a r t e r   I n   l i s t   R e c e i p t   D e f a u l t   C o m p a c t   C o v e r e d   F o l d e r s   D i s c r e t e   I n a c t i v e   D i r e c t l y   N o   c h e c k   C o n t a i n s   N o t   u s e d   C a r d i n a l   E q u a l   t o   I n   g r o u p   N o t   l i k e   S e q u e n c e   S t a n d a r d   I n c l u d i n g   E x c l u d i n g   A u t o m a t i c   H a l f   y e a r   A t t e n t i o n   P r e d i c t e d   R e d u n d a n t   I m p o r t a n t   A s   n u m b e r   R e a l - t i m e   C o n n e c t e d   L e s s   t h a n   N o   s t a t u s   A l l   t a s k s   D o   n o t   u s e   B a c k g r o u n d   C i t y   b l o c k   A s   B o o l e a n   P r o c e s s i n g   C o n t i n u o u s   A l l   f i e l d s   C o n t r o l l e d   K e y   f i e l d s   E x c e p t i o n a l   D o   n o t   f i l l   N o t   i n   l i s t   U s e d   f i e l d s   E n a b l e   t e s t   I n f o r m a t i o n   D o   n o t   s h o w   A c t u a l i z i n g   N o t   r u n n i n g   S t a n d a r d i z e   V e r s i o n   8 . 2   B e g i n s   w i t h   C u r r e n t   d a y   W a r d   m e t h o d   S h a r e d   m o d e   B a s e   p e r i o d   D o N o t U p d a t e   C u r r e n t   y e a r   N o t   i n   g r o u p   S e r i a l i z a b l e   B y   p e r f o r m e r   N o t   e q u a l   t o   C u r r e n t   w e e k   D i s c o n n e c t e d   D o   n o t   p r u n e   C u r r e n t   h o u r   R a n g e   ( > ,   < )   G r e a t e r   t h a n   D i s p l a y   e r r o r   D a t a   A n a l y s i s   B y   c o n f i d e n c e   D o   n o t   c h a n g e   R a n g e   ( > = ,   < )   C l e a r   P o s t i n g   A c t i o n   p e r i o d   C u r r e n t   m o n t h   C o m p u t e r   d a t a   I n   l i s t   g r o u p   R a n g e   ( > ,   < = )   N o n - p e r i o d i c a l   C h e c k   o n   w r i t e   K - M e a n s   m e t h o d   C u r r e n t   m i n u t e   A c t i v e / P a s s i v e   E x c l u s i v e   m o d e   R a n g e   ( > = ,   < = )   R e a d   c o m m i t t e d   V e r y   i m p o r t a n t   D o   n o t   i n c l u d e   C u r r e n t   1 0   d a y s   B y   s i g n i f i c a n c e   D i s p l a y   w a r n i n g   C u r r e n t   q u a r t e r   C e n t r o i d   m e t h o d   C u r r e n t   o r   l a s t   C u r r e n t   o r   f i r s t   R e a d   u n c o m m i t t e d   R e p e a t a b l e   r e a d s   D o e s   n o t   c o n t a i n   B y   s e r i e s   l e n g t h   A p p l i c a t i o n   d a t a   F o l d e r s   a n d   i t e m s   R o o t   c e r t i f i c a t e s   S q u a r e d   E u c l i d e a n   N o t   i n   l i s t   g r o u p   C u r r e n t   h a l f   y e a r   D o   n o t   s t a n d a r d i z e   I n c l u d e   f u l l   c h a i n   D a t a   s o u r c e   c o l u m n   B y   n u m b e r   o f   c a s e s   I g n o r e   a c t i o n   t i m e   I n p u t   a n d   p r e d i c t e d   R e g i s t r a t i o n   p e r i o d   D o e s   n o t   b e g i n   w i t h   U p d a t e   a u t o m a t i c a l l y   A c t u a l   a c t i o n   p e r i o d   S h o w   d u r i n g   e x e c u t i o n   L e s s   t h a n   o r   e q u a l   t o   P e r s o n a l   c e r t i f i c a t e s   U p d a t e   p r e d e f i n e d   d a t a   R e c i p i e n t   c e r t i f i c a t e s   N e a r e s t   n e i g h b o r   m e t h o d   G r e a t e r   t h a n   o r   e q u a l   t o   F u r t h e s t   n e i g h b o r   m e t h o d   I g n o r e   s i g n a t u r e   v a l i d i t y   O p e r a t i n g   s y s t e m   u s e r   d a t a   I n c l u d e   c h a i n   w i t h o u t   r o o t   D o   n o t   u p d a t e   a u t o m a t i c a l l y   I n c l u d e   s u b j e c t   c e r t i f i c a t e   V e r i f i c a t i o n   c e n t e r   c e r t i f i c a t e s   U s e   o f   a c c u m u l a t i o n   r e g i s t e r   a g g r e g a t e   )�I g n o r e   c h e c k   i n   r e v o k e d   c e r t i f i c a t e s   l i s t   .�P e r i o d i c i t y   o f   a c c u m u l a t i o n   r e g i s t e r   a g g r e g a t e   ������0  ﻿<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tns="http://v8.1c.ru/8.2/data/data-analysis" targetNamespace="http://v8.1c.ru/8.2/data/data-analysis" elementFormDefault="qualified" attributeFormDefault="unqualified">

  <xs:simpleType name="AnalysisDataType">
    <xs:restriction base="xs:string">
      <xs:enumeration value="Contiguous"/>
      <xs:enumeration value="Discrete"/>
    </xs:restriction>
  </xs:simpleType>

  <xs:simpleType name="ClusterizationMethod">
    <xs:restriction base="xs:string">
      <xs:enumeration value="KMeans"/>
      <xs:enumeration value="NearestNeighbor"/>
      <xs:enumeration value="FurthestNeighbor"/>
      <xs:enumeration value="Centroid"/>
    </xs:restriction>
  </xs:simpleType>

  <xs:simpleType name="DataAnalysisTimeIntervalUnitType">
    <xs:restriction base="xs:string">
      <xs:enumeration value="Second"/>
      <xs:enumeration value="CurrentMinute"/>
      <xs:enumeration value="Minute"/>
      <xs:enumeration value="CurrentHour"/>
      <xs:enumeration value="Hour"/>
      <xs:enumeration value="CurrentDay"/>
      <xs:enumeration value="Day"/>
      <xs:enumeration value="CurrentWeek"/>
      <xs:enumeration value="Week"/>
      <xs:enumeration value="CurrentMonth"/>
      <xs:enumeration value="Month"/>
      <xs:enumeration value="CurrentQuarter"/>
      <xs:enumeration value="Quarter"/>
      <xs:enumeration value="CurrentYear"/>
      <xs:enumeration value="Year"/>
      <xs:enumeration value="CurrentTenDays"/>
      <xs:enumeration value="TenDays"/>
      <xs:enumeration value="CurrentHalfYear"/>
      <xs:enumeration value="HalfYear"/>
    </xs:restriction>
  </xs:simpleType>

  <xs:simpleType name="DataAnalysisResultTableFillType">
    <xs:restriction base="xs:string">
      <xs:enumeration value="DontFill"/>
      <xs:enumeration value="KeyFields"/>
      <xs:enumeration value="UsedFields"/>
      <xs:enumeration value="AllFields"/>
    </xs:restriction>
  </xs:simpleType>

  <xs:simpleType name="DataAnalysisNumericValueUseType">
    <xs:restriction base="xs:string">
      <xs:enumeration value="AsNumeric"/>
      <xs:enumeration value="AsBoolean"/>
    </xs:restriction>
  </xs:simpleType>

  <xs:simpleType name="AssociationRulesDataSourceType">
    <xs:restriction base="xs:string">
      <xs:enumeration value="Event"/>
      <xs:enumeration value="Object"/>
    </xs:restriction>
  </xs:simpleType>

  <xs:simpleType name="DataAnalysisColumnTypeDecisionTree">
    <xs:restriction base="xs:string">
      <xs:enumeration value="NotUsed"/>
      <xs:enumeration value="Input"/>
      <xs:enumeration value="Predictable"/>
    </xs:restriction>
  </xs:simpleType>

  <xs:simpleType name="DataAnalysisColumnTypeClusterization">
    <xs:restriction base="xs:string">
      <xs:enumeration value="NotUsed"/>
      <xs:enumeration value="Input"/>
      <xs:enumeration value="Predictable"/>
      <xs:enumeration value="InputAndPredictable"/>
      <xs:enumeration value="Key"/>
    </xs:restriction>
  </xs:simpleType>

  <xs:simpleType name="DataAnalysisColumnTypeSummaryStatistics">
    <xs:restriction base="xs:string">
      <xs:enumeration value="NotUsed"/>
      <xs:enumeration value="Input"/>
    </xs:restriction>
  </xs:simpleType>

  <xs:simpleType name="DataAnalysisColumnTypeAssociationRules">
    <xs:restriction base="xs:string">
      <xs:enumeration value="NotUsed"/>
      <xs:enumeration value="Object"/>
      <xs:enumeration value="Item"/>
    </xs:restriction>
  </xs:simpleType>

  <xs:simpleType name="DataAnalysisColumnTypeSequentialPatterns">
    <xs:restriction base="xs:string">
      <xs:enumeration value="NotUsed"/>
      <xs:enumeration value="Item"/>
      <xs:enumeration value="Sequence"/>
      <xs:enumeration value="Time"/>
    </xs:restriction>
  </xs:simpleType>

  <xs:simpleType name="PredictionModelColumnType">
    <xs:restriction base="xs:string">
      <xs:enumeration value="Input"/>
      <xs:enumeration value="Predictable"/>
      <xs:enumeration value="DataSourceColumn"/>
    </xs:restriction>
  </xs:simpleType>

  <xs:simpleType name="DataAnalysisDistanceMetricType">
    <xs:restriction base="xs:string">
      <xs:enumeration value="SquaredEuclidean"/>
      <xs:enumeration value="Euclidean"/>
      <xs:enumeration value="CityBlock"/>
      <xs:enumeration value="Maximum"/>
    </xs:restriction>
  </xs:simpleType>

  <xs:simpleType name="AssociationRulesPruneType">
    <xs:restriction base="xs:string">
      <xs:enumeration value="Redundant"/>
      <xs:enumeration value="Covered"/>
    </xs:restriction>
  </xs:simpleType>

  <xs:simpleType name="DataAnalysisStandardizationType">
    <xs:restriction base="xs:string">
      <xs:enumeration value="DontStandardize"/>
      <xs:enumeration value="Standardize"/>
    </xs:restriction>
  </xs:simpleType>

  <xs:simpleType name="DecisionTreeSimplificationType">
    <xs:restriction base="xs:string">
      <xs:enumeration value="DontSimplify"/>
      <xs:enumeration value="Simplify"/>
    </xs:restriction>
  </xs:simpleType>

  <xs:simpleType name="DataAnalysisAssociationRulesOrderType">
    <xs:restriction base="xs:string">
      <xs:enumeration value="BySupport"/>
      <xs:enumeration value="ByConfidence"/>
      <xs:enumeration value="ByImportance"/>
    </xs:restriction>
  </xs:simpleType>

  <xs:simpleType name="DataAnalysisSequentialPatternsOrderType">
    <xs:restriction base="xs:string">
      <xs:enumeration value="BySupport"/>
      <xs:enumeration value="ByLength"/>
    </xs:restriction>
  </xs:simpleType>

  <xs:simpleType name="DataAnalysisColumnType">
    <xs:restriction base="xs:string">
      <xs:enumeration value="NotUsed"/>
    </xs:restriction>
  </xs:simpleType>

</xs:schema>
������������:  <?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
    targetNamespace="http://v8.1c.ru/8.1/data/enterprise"
    xmlns:tns="http://v8.1c.ru/8.1/data/enterprise"
    xmlns:core="http://v8.1c.ru/8.1/data/core"
    attributeFormDefault="unqualified"
    elementFormDefault="qualified"
    version="1.0">
  <xs:import namespace="http://v8.1c.ru/8.1/data/core" schemaLocation="../../../xdto/src/res/core.xsd"/>

  <xs:simpleType name="AnyRef">
    <xs:restriction base="xs:string">
      <xs:pattern value="[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}"/>
    </xs:restriction>
  </xs:simpleType>

  <xs:complexType name="ObjectDeletion">
    <xs:sequence>
      <xs:element name="Ref" type="tns:AnyRef"/>
    </xs:sequence>
  </xs:complexType>

  <xs:complexType name="Bound">
    <xs:sequence>
      <xs:element name="value" nillable="true" />
      <xs:element name="kind" type="xs:decimal" />
    </xs:sequence>
  </xs:complexType>

  <xs:complexType name="PointOfTime">
    <xs:sequence>
      <xs:element name="date" type="xs:dateTime" />
      <xs:element name="reference" nillable="true" type="tns:AnyRef" />
    </xs:sequence>
  </xs:complexType>
  
  <xs:complexType name="PointOfTimeWithPeriodAdjustment">
    <xs:sequence>
      <xs:element name="date" type="xs:dateTime" />
      <xs:element name="dateAdjustment" type="xs:decimal" />
      <xs:element name="reference" nillable="true" type="tns:AnyRef" />
    </xs:sequence>
  </xs:complexType>

  <xs:complexType name="FilterItem">
    <xs:sequence>
      <xs:element name="Name" type="xs:NCName"/>
      <xs:element name="Usage" type="xs:boolean" default="true" minOccurs="0"/>
      <xs:element name="ComparisonType" type="tns:ComparisonType" default="Equal" minOccurs="0" />
      <xs:element name="Value" nillable="true" type="xs:anySimpleType" minOccurs="0"/>
      <xs:element name="ValueFrom" nillable="true" type="xs:anySimpleType" minOccurs="0"/>
      <xs:element name="ValueTo" nillable="true" type="xs:anySimpleType" minOccurs="0"/>
    </xs:sequence>
  </xs:complexType>

  <xs:complexType name="Filter">
    <xs:sequence>
      <xs:element name="FilterItem" type="tns:FilterItem" minOccurs="0" maxOccurs="unbounded"/>
    </xs:sequence>
  </xs:complexType>

  <xs:simpleType name="ComparisonType">
    <xs:restriction base="xs:string">
        <xs:enumeration value="Equal"/>
        <xs:enumeration value="NotEqual"/>
        <xs:enumeration value="Less"/>
        <xs:enumeration value="LessOrEqual"/>
        <xs:enumeration value="Greater"/>
        <xs:enumeration value="GreaterOrEqual"/>
        <xs:enumeration value="Interval"/>
        <xs:enumeration value="IntervalIncludingBounds"/>
        <xs:enumeration value="IntervalIncludingLowerBound"/>
        <xs:enumeration value="IntervalIncludingUpperBound"/>
        <xs:enumeration value="Contains"/>
        <xs:enumeration value="InList"/>
        <xs:enumeration value="InListByHierarchy"/>
        <xs:enumeration value="NotInList"/>
        <xs:enumeration value="NotInListByHierarchy"/>
        <xs:enumeration value="InHierarchy"/>
        <xs:enumeration value="NotInHierarchy"/>
        <xs:enumeration value="NotContains"/>
        <xs:enumeration value="BeginsWith"/>
        <xs:enumeration value="NotBeginsWith"/>
        <xs:enumeration value="Like"/>
        <xs:enumeration value="NotLike"/>
    </xs:restriction>
  </xs:simpleType>

  <xs:simpleType name="AccumulationRecordType">
    <xs:restriction base="xs:string">
        <xs:enumeration value="Receipt"/>
        <xs:enumeration value="Expense"/>
    </xs:restriction>
  </xs:simpleType>
  
  <xs:simpleType name="AccountingRecordType">
    <xs:restriction base="xs:string">
        <xs:enumeration value="Debit"/>
        <xs:enumeration value="Credit"/>
    </xs:restriction>
  </xs:simpleType>
  
  <xs:simpleType name="AccountType">
    <xs:restriction base="xs:string">
        <xs:enumeration value="Active"/>
        <xs:enumeration value="Passive"/>
        <xs:enumeration value="ActivePassive"/>
    </xs:restriction>
  </xs:simpleType>
  
  <xs:simpleType name="FoldersAndItemsUse">
    <xs:restriction base="xs:string">
      <xs:enumeration value="Items"/>
      <xs:enumeration value="Folders"/>
      <xs:enumeration value="FoldersAndItems"/>
    </xs:restriction>
  </xs:simpleType>

  <xs:simpleType name="UpdateOnDataChange">
    <xs:restriction base="xs:string">
      <xs:enumeration value="Auto"/>
      <xs:enumeration value="DontUpdate"/>
    </xs:restriction>
  </xs:simpleType>

  <xs:simpleType name="AutoTimeMode">
    <xs:restriction base="xs:string">
      <xs:enumeration value="DontUse"/>
      <xs:enumeration value="Last"/>
      <xs:enumeration value="First"/>
      <xs:enumeration value="CurrentOrLast"/>
      <xs:enumeration value="CurrentOrFirst"/>
    </xs:restriction>
  </xs:simpleType>
  
  <xs:simpleType name="PostingModeUse">
    <xs:restriction base="xs:string">
      <xs:enumeration value="Regular"/>
      <xs:enumeration value="RealTime"/>
      <xs:enumeration value="Ask"/>
      <xs:enumeration value="Auto"/>
    </xs:restriction>
  </xs:simpleType>

    <xs:simpleType name="DocumentWriteMode">
    <xs:restriction base="xs:string">
      <xs:enumeration value="Write"/>
      <xs:enumeration value="Posting"/>
      <xs:enumeration value="UndoPosting"/>
    </xs:restriction>
  </xs:simpleType>

    <xs:simpleType name="DocumentPostingMode">
        <xs:restriction base="xs:string">
            <xs:enumeration value="Regular"/>
            <xs:enumeration value="RealTime"/>
        </xs:restriction>
    </xs:simpleType>

    <xs:simpleType name="ClientConnectionSpeed">
        <xs:restriction base="xs:string">
            <xs:enumeration value="Normal" />
            <xs:enumeration value="Low" />
        </xs:restriction>
    </xs:simpleType>

    <xs:complexType name="UserMessage">
        <xs:sequence>
            <xs:element name="Text" type="xs:string" minOccurs="0"/>
            <xs:element name="Ref" type="tns:AnyRef"  nillable="true"/>
            <xs:element name="pic" type="xs:anyType" maxOccurs="1" minOccurs="0"/>
            <xs:element name="details" type="xs:string" minOccurs="0"/>
        </xs:sequence>
        <xs:attribute name="MessageMarker" type="xs:decimal"/>
        <xs:attribute name="DataPath" type="xs:string" />
        <xs:attribute name="FormAttributePath" type="xs:string" />
        <xs:attribute name="FormID" type="core:UUID" />
        <xs:attribute name="notification" type="xs:boolean" />
        <xs:attribute name="caption" type="xs:string" />
        <xs:attribute name="url" type="xs:string" />
        <xs:attribute name="tag" type="xs:string" />
    </xs:complexType>

    <xs:complexType name="UserMessages">
        <xs:sequence>
            <xs:element name="message" type="tns:UserMessage" minOccurs="0" maxOccurs="unbounded"/>
        </xs:sequence>
    </xs:complexType>

    <xs:simpleType name="JobScheduleWeekDays">
        <xs:list itemType="xs:decimal"/>
    </xs:simpleType>

    <xs:simpleType name="JobScheduleMonths">
        <xs:list itemType="xs:decimal"/>
    </xs:simpleType>

    <xs:simpleType name="ClientApplicationBaseFontVariant">
      <xs:restriction base="xs:string">
        <xs:enumeration value="Normal"/>
        <xs:enumeration value="Large"/>
      </xs:restriction>
    </xs:simpleType>

    <xs:simpleType name="ClientApplicationInterfaceVariant">
        <xs:restriction base="xs:string">
            <xs:enumeration value="Version8_2"/>
            <xs:enumeration value="Taxi"/>
        </xs:restriction>
    </xs:simpleType>

    <xs:simpleType name="ClientApplicationFormScaleVariant">
        <xs:restriction base="xs:string">
            <xs:enumeration value="Auto"/>
            <xs:enumeration value="Normal"/>
            <xs:enumeration value="Compact"/>
        </xs:restriction>
    </xs:simpleType>

    <xs:complexType name="JobSchedule">
        <xs:sequence>
            <xs:element name="WeekDays" type="tns:JobScheduleWeekDays" />
            <xs:element name="Months" type="tns:JobScheduleMonths" />
            <xs:element name="DetailedDailySchedules" type="tns:JobSchedule"  minOccurs="0" maxOccurs="unbounded" />
        </xs:sequence>
        <xs:attribute name="BeginDate" type="xs:date"/>
        <xs:attribute name="EndDate" type="xs:date"/>
        <xs:attribute name="BeginTime" type="xs:time"/>
        <xs:attribute name="EndTime" type="xs:time"/>
        <xs:attribute name="CompletionTime" type="xs:time"/>
        <xs:attribute name="CompletionInterval" type="xs:decimal"/>
        <xs:attribute name="RepeatPeriodInDay" type="xs:decimal"/>
        <xs:attribute name="RepeatPause" type="xs:decimal"/>
        <xs:attribute name="WeekDayInMonth" type="xs:decimal"/>
        <xs:attribute name="DayInMonth" type="xs:decimal"/>
        <xs:attribute name="WeeksPeriod" type="xs:decimal"/>
        <xs:attribute name="DaysRepeatPeriod" type="xs:decimal"/>
    </xs:complexType>

    <xs:complexType name="Aggregate">
        <xs:sequence>
            <xs:element name="use" type="xs:boolean" minOccurs="0" />
            <xs:element name="periodicity" type="xs:decimal" minOccurs="0" />
            <xs:element name="beginOfPeriod" type="xs:dateTime" minOccurs="0" />
            <xs:element name="endOfPeriod" type="xs:dateTime" minOccurs="0" />
            <xs:element name="size" type="xs:decimal" minOccurs="0" />
            <xs:element name="dimension" type="xs:string" minOccurs="0" maxOccurs="unbounded" />
        </xs:sequence>
    </xs:complexType>
    <xs:complexType name="AggregateList">
        <xs:sequence>
            <xs:element name="buildDate" type="xs:dateTime" minOccurs="0" />
            <xs:element name="sizeLimit" type="xs:decimal" minOccurs="0" />
            <xs:element name="size" type="xs:decimal" minOccurs="0" />
            <xs:element name="effect" type="xs:decimal" minOccurs="0" />
            <xs:element name="aggregate" type="tns:Aggregate" minOccurs="0" maxOccurs="unbounded" />
        </xs:sequence>
    </xs:complexType>

    <xs:simpleType name="AccumulationRegisterAggregateUse">
        <xs:restriction base="xs:string">
            <xs:enumeration value="Auto" />
            <xs:enumeration value="Always"/>
        </xs:restriction>
    </xs:simpleType>
    <xs:simpleType name="AccumulationRegisterAggregatePeriodicity">
        <xs:restriction base="xs:string">
            <xs:enumeration value="Nonperiodical"/>
            <xs:enumeration value="Auto"/>
            <xs:enumeration value="Day"/>
            <xs:enumeration value="Month"/>
            <xs:enumeration value="Quarter"/>
            <xs:enumeration value="HalfYear"/>
            <xs:enumeration value="Year"/>
        </xs:restriction>
    </xs:simpleType>
    
    <xs:simpleType name="LinkedValueChangeMode">
        <xs:restriction base="xs:string">
            <xs:enumeration value="Clear"/>
            <xs:enumeration value="DontChange"/>
        </xs:restriction>
    </xs:simpleType>

    <xs:simpleType name="ResultCompositionMode">
        <xs:restriction base="xs:string">
            <xs:enumeration value="Auto"/>
            <xs:enumeration value="Background"/>
            <xs:enumeration value="Directly"/>
        </xs:restriction>
    </xs:simpleType>

    <xs:simpleType name="AutoShowStateMode">
        <xs:restriction base="xs:string">
            <xs:enumeration value="Auto"/>
            <xs:enumeration value="DontShow"/>
            <xs:enumeration value="Show"/>
            <xs:enumeration value="ShowOnComposition"/>
        </xs:restriction>
    </xs:simpleType>

    <xs:simpleType name="MessageStatus">
        <xs:restriction base="xs:string">
            <xs:enumeration value="WithoutStatus"/>
            <xs:enumeration value="Ordinary"/>
            <xs:enumeration value="Information"/>
            <xs:enumeration value="Attention"/>
            <xs:enumeration value="Important"/>
            <xs:enumeration value="VeryImportant"/>
        </xs:restriction>
    </xs:simpleType>
    
    <xs:simpleType name="ModalityUseMode">
        <xs:restriction base="xs:string">
            <xs:enumeration value="Use"/>
            <xs:enumeration value="UseWithWarnings"/>
            <xs:enumeration value="DontUse"/>
        </xs:restriction>
    </xs:simpleType>

    <xs:simpleType name="TransactionsIsolationLevel">
        <xs:restriction base="xs:string">
            <xs:enumeration value="Auto"/>
            <xs:enumeration value="ReadUncommitted"/>
            <xs:enumeration value="ReadCommitted"/>
            <xs:enumeration value="RepeatableRead"/>
            <xs:enumeration value="Serializable"/>
        </xs:restriction>
    </xs:simpleType>
    
    <xs:simpleType name="PredefinedDataUpdate">
        <xs:annotation>
            <xs:documentation>
                Обновление предопределенных данных
            </xs:documentation>
        </xs:annotation>
        <xs:restriction base="xs:string">
            <xs:enumeration value="Auto"/>
            <xs:enumeration value="AutoUpdate"/>
            <xs:enumeration value="DontAutoUpdate"/>
        </xs:restriction>
    </xs:simpleType>
    <xs:simpleType name="DataChangeType">
        <xs:annotation>
            <xs:documentation>
                ВидИзмененияДанных
            </xs:documentation>
        </xs:annotation>
        <xs:restriction base="xs:string">
            <xs:enumeration value="Create"/>
            <xs:enumeration value="Update"/>
            <xs:enumeration value="Delete"/>
        </xs:restriction>
    </xs:simpleType>

    <xs:simpleType name="DataLineChangeType">
        <xs:annotation>
            <xs:documentation>
                ВидИзмененияСтрокиДанных
            </xs:documentation>
        </xs:annotation>
        <xs:restriction base="xs:string">
            <xs:enumeration value="Add"/>
            <xs:enumeration value="Update"/>
            <xs:enumeration value="Delete"/>
            <xs:enumeration value="Move"/>
        </xs:restriction>
    </xs:simpleType>

    <xs:simpleType name="ReportResultViewMode">
        <xs:restriction base="xs:string">
            <xs:enumeration value="Auto"/>
            <xs:enumeration value="Default"/>
            <xs:enumeration value="Compact"/>
        </xs:restriction>
    </xs:simpleType>

</xs:schema>�������@  ﻿<model xmlns="http://v8.1c.ru/8.1/xdto" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<package targetNamespace="http://v8.1c.ru/8.1/data/enterprise" elementFormQualified="true" attributeFormQualified="false">
		<import namespace="http://v8.1c.ru/8.1/data/core"/>
		<valueType name="AccountType" base="xs:string">
			<enumeration>Active</enumeration>
			<enumeration>Passive</enumeration>
			<enumeration>ActivePassive</enumeration>
		</valueType>
		<valueType name="AccountingRecordType" base="xs:string">
			<enumeration>Debit</enumeration>
			<enumeration>Credit</enumeration>
		</valueType>
		<valueType name="AccumulationRecordType" base="xs:string">
			<enumeration>Receipt</enumeration>
			<enumeration>Expense</enumeration>
		</valueType>
		<valueType name="AccumulationRegisterAggregatePeriodicity" base="xs:string">
			<enumeration>Nonperiodical</enumeration>
			<enumeration>Auto</enumeration>
			<enumeration>Day</enumeration>
			<enumeration>Month</enumeration>
			<enumeration>Quarter</enumeration>
			<enumeration>HalfYear</enumeration>
			<enumeration>Year</enumeration>
		</valueType>
		<valueType name="AccumulationRegisterAggregateUse" base="xs:string">
			<enumeration>Auto</enumeration>
			<enumeration>Always</enumeration>
		</valueType>
		<valueType name="AnyRef" base="xs:string">
			<pattern>[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}</pattern>
		</valueType>
		<valueType name="AutoShowStateMode" base="xs:string">
			<enumeration>Auto</enumeration>
			<enumeration>DontShow</enumeration>
			<enumeration>Show</enumeration>
			<enumeration>ShowOnComposition</enumeration>
		</valueType>
		<valueType name="AutoTimeMode" base="xs:string">
			<enumeration>DontUse</enumeration>
			<enumeration>Last</enumeration>
			<enumeration>First</enumeration>
			<enumeration>CurrentOrLast</enumeration>
			<enumeration>CurrentOrFirst</enumeration>
		</valueType>
		<valueType name="ClientApplicationBaseFontVariant" base="xs:string">
			<enumeration>Normal</enumeration>
			<enumeration>Large</enumeration>
		</valueType>
		<valueType name="ClientApplicationFormScaleVariant" base="xs:string">
			<enumeration>Auto</enumeration>
			<enumeration>Normal</enumeration>
			<enumeration>Compact</enumeration>
		</valueType>
		<valueType name="ClientApplicationInterfaceVariant" base="xs:string">
			<enumeration>Version8_2</enumeration>
			<enumeration>Taxi</enumeration>
		</valueType>
		<valueType name="ClientConnectionSpeed" base="xs:string">
			<enumeration>Normal</enumeration>
			<enumeration>Low</enumeration>
		</valueType>
		<valueType name="ComparisonType" base="xs:string">
			<enumeration>Equal</enumeration>
			<enumeration>NotEqual</enumeration>
			<enumeration>Less</enumeration>
			<enumeration>LessOrEqual</enumeration>
			<enumeration>Greater</enumeration>
			<enumeration>GreaterOrEqual</enumeration>
			<enumeration>Interval</enumeration>
			<enumeration>IntervalIncludingBounds</enumeration>
			<enumeration>IntervalIncludingLowerBound</enumeration>
			<enumeration>IntervalIncludingUpperBound</enumeration>
			<enumeration>Contains</enumeration>
			<enumeration>InList</enumeration>
			<enumeration>InListByHierarchy</enumeration>
			<enumeration>NotInList</enumeration>
			<enumeration>NotInListByHierarchy</enumeration>
			<enumeration>InHierarchy</enumeration>
			<enumeration>NotInHierarchy</enumeration>
			<enumeration>NotContains</enumeration>
			<enumeration>BeginsWith</enumeration>
			<enumeration>NotBeginsWith</enumeration>
			<enumeration>Like</enumeration>
			<enumeration>NotLike</enumeration>
		</valueType>
		<valueType name="DataChangeType" base="xs:string">
			<enumeration>Create</enumeration>
			<enumeration>Update</enumeration>
			<enumeration>Delete</enumeration>
		</valueType>
		<valueType name="DataLineChangeType" base="xs:string">
			<enumeration>Add</enumeration>
			<enumeration>Update</enumeration>
			<enumeration>Delete</enumeration>
			<enumeration>Move</enumeration>
		</valueType>
		<valueType name="DocumentPostingMode" base="xs:string">
			<enumeration>Regular</enumeration>
			<enumeration>RealTime</enumeration>
		</valueType>
		<valueType name="DocumentWriteMode" base="xs:string">
			<enumeration>Write</enumeration>
			<enumeration>Posting</enumeration>
			<enumeration>UndoPosting</enumeration>
		</valueType>
		<valueType name="FoldersAndItemsUse" base="xs:string">
			<enumeration>Items</enumeration>
			<enumeration>Folders</enumeration>
			<enumeration>FoldersAndItems</enumeration>
		</valueType>
		<valueType name="JobScheduleMonths" itemType="xs:decimal"/>
		<valueType name="JobScheduleWeekDays" itemType="xs:decimal"/>
		<valueType name="LinkedValueChangeMode" base="xs:string">
			<enumeration>Clear</enumeration>
			<enumeration>DontChange</enumeration>
		</valueType>
		<valueType name="MessageStatus" base="xs:string">
			<enumeration>WithoutStatus</enumeration>
			<enumeration>Ordinary</enumeration>
			<enumeration>Information</enumeration>
			<enumeration>Attention</enumeration>
			<enumeration>Important</enumeration>
			<enumeration>VeryImportant</enumeration>
		</valueType>
		<valueType name="ModalityUseMode" base="xs:string">
			<enumeration>Use</enumeration>
			<enumeration>UseWithWarnings</enumeration>
			<enumeration>DontUse</enumeration>
		</valueType>
		<valueType name="PostingModeUse" base="xs:string">
			<enumeration>Regular</enumeration>
			<enumeration>RealTime</enumeration>
			<enumeration>Ask</enumeration>
			<enumeration>Auto</enumeration>
		</valueType>
		<valueType name="PredefinedDataUpdate" base="xs:string">
			<enumeration>Auto</enumeration>
			<enumeration>AutoUpdate</enumeration>
			<enumeration>DontAutoUpdate</enumeration>
		</valueType>
		<valueType name="ReportResultViewMode" base="xs:string">
			<enumeration>Auto</enumeration>
			<enumeration>Default</enumeration>
			<enumeration>Compact</enumeration>
		</valueType>
		<valueType name="ResultCompositionMode" base="xs:string">
			<enumeration>Auto</enumeration>
			<enumeration>Background</enumeration>
			<enumeration>Directly</enumeration>
		</valueType>
		<valueType name="TransactionsIsolationLevel" base="xs:string">
			<enumeration>Auto</enumeration>
			<enumeration>ReadUncommitted</enumeration>
			<enumeration>ReadCommitted</enumeration>
			<enumeration>RepeatableRead</enumeration>
			<enumeration>Serializable</enumeration>
		</valueType>
		<valueType name="UpdateOnDataChange" base="xs:string">
			<enumeration>Auto</enumeration>
			<enumeration>DontUpdate</enumeration>
		</valueType>
		<objectType name="Aggregate">
			<property name="use" type="xs:boolean" lowerBound="0"/>
			<property name="periodicity" type="xs:decimal" lowerBound="0"/>
			<property name="beginOfPeriod" type="xs:dateTime" lowerBound="0"/>
			<property name="endOfPeriod" type="xs:dateTime" lowerBound="0"/>
			<property name="size" type="xs:decimal" lowerBound="0"/>
			<property name="dimension" type="xs:string" lowerBound="0" upperBound="-1"/>
		</objectType>
		<objectType name="AggregateList">
			<property name="buildDate" type="xs:dateTime" lowerBound="0"/>
			<property name="sizeLimit" type="xs:decimal" lowerBound="0"/>
			<property name="size" type="xs:decimal" lowerBound="0"/>
			<property name="effect" type="xs:decimal" lowerBound="0"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.1/data/enterprise" name="aggregate" type="d4p1:Aggregate" lowerBound="0" upperBound="-1"/>
		</objectType>
		<objectType name="Bound">
			<property name="value" nillable="true"/>
			<property name="kind" type="xs:decimal"/>
		</objectType>
		<objectType name="Filter">
			<property xmlns:d4p1="http://v8.1c.ru/8.1/data/enterprise" name="FilterItem" type="d4p1:FilterItem" lowerBound="0" upperBound="-1"/>
		</objectType>
		<objectType name="FilterItem">
			<property name="Name" type="xs:NCName"/>
			<property name="Usage" type="xs:boolean" lowerBound="0" default="true"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.1/data/enterprise" name="ComparisonType" type="d4p1:ComparisonType" lowerBound="0" default="Equal"/>
			<property name="Value" lowerBound="0" nillable="true"/>
			<property name="ValueFrom" lowerBound="0" nillable="true"/>
			<property name="ValueTo" lowerBound="0" nillable="true"/>
		</objectType>
		<objectType name="JobSchedule">
			<property name="BeginDate" type="xs:date" lowerBound="0" form="Attribute"/>
			<property name="EndDate" type="xs:date" lowerBound="0" form="Attribute"/>
			<property name="BeginTime" type="xs:time" lowerBound="0" form="Attribute"/>
			<property name="EndTime" type="xs:time" lowerBound="0" form="Attribute"/>
			<property name="CompletionTime" type="xs:time" lowerBound="0" form="Attribute"/>
			<property name="CompletionInterval" type="xs:decimal" lowerBound="0" form="Attribute"/>
			<property name="RepeatPeriodInDay" type="xs:decimal" lowerBound="0" form="Attribute"/>
			<property name="RepeatPause" type="xs:decimal" lowerBound="0" form="Attribute"/>
			<property name="WeekDayInMonth" type="xs:decimal" lowerBound="0" form="Attribute"/>
			<property name="DayInMonth" type="xs:decimal" lowerBound="0" form="Attribute"/>
			<property name="WeeksPeriod" type="xs:decimal" lowerBound="0" form="Attribute"/>
			<property name="DaysRepeatPeriod" type="xs:decimal" lowerBound="0" form="Attribute"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.1/data/enterprise" name="WeekDays" type="d4p1:JobScheduleWeekDays"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.1/data/enterprise" name="Months" type="d4p1:JobScheduleMonths"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.1/data/enterprise" name="DetailedDailySchedules" type="d4p1:JobSchedule" lowerBound="0" upperBound="-1"/>
		</objectType>
		<objectType name="ObjectDeletion">
			<property xmlns:d4p1="http://v8.1c.ru/8.1/data/enterprise" name="Ref" type="d4p1:AnyRef"/>
		</objectType>
		<objectType name="PointOfTime">
			<property name="date" type="xs:dateTime"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.1/data/enterprise" name="reference" type="d4p1:AnyRef" nillable="true"/>
		</objectType>
		<objectType name="PointOfTimeWithPeriodAdjustment">
			<property name="date" type="xs:dateTime"/>
			<property name="dateAdjustment" type="xs:decimal"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.1/data/enterprise" name="reference" type="d4p1:AnyRef" nillable="true"/>
		</objectType>
		<objectType name="UserMessage">
			<property name="MessageMarker" type="xs:decimal" lowerBound="0" form="Attribute"/>
			<property name="DataPath" type="xs:string" lowerBound="0" form="Attribute"/>
			<property name="FormAttributePath" type="xs:string" lowerBound="0" form="Attribute"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.1/data/core" name="FormID" type="d4p1:UUID" lowerBound="0" form="Attribute"/>
			<property name="notification" type="xs:boolean" lowerBound="0" form="Attribute"/>
			<property name="caption" type="xs:string" lowerBound="0" form="Attribute"/>
			<property name="url" type="xs:string" lowerBound="0" form="Attribute"/>
			<property name="tag" type="xs:string" lowerBound="0" form="Attribute"/>
			<property name="Text" type="xs:string" lowerBound="0"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.1/data/enterprise" name="Ref" type="d4p1:AnyRef" nillable="true"/>
			<property name="pic" lowerBound="0"/>
			<property name="details" type="xs:string" lowerBound="0"/>
		</objectType>
		<objectType name="UserMessages">
			<property xmlns:d4p1="http://v8.1c.ru/8.1/data/enterprise" name="message" type="d4p1:UserMessage" lowerBound="0" upperBound="-1"/>
		</objectType>
	</package>
	<package targetNamespace="http://v8.1c.ru/8.2/data/data-analysis" elementFormQualified="true" attributeFormQualified="false">
		<valueType name="AnalysisDataType" base="xs:string">
			<enumeration>Contiguous</enumeration>
			<enumeration>Discrete</enumeration>
		</valueType>
		<valueType name="AssociationRulesDataSourceType" base="xs:string">
			<enumeration>Event</enumeration>
			<enumeration>Object</enumeration>
		</valueType>
		<valueType name="AssociationRulesPruneType" base="xs:string">
			<enumeration>Redundant</enumeration>
			<enumeration>Covered</enumeration>
		</valueType>
		<valueType name="ClusterizationMethod" base="xs:string">
			<enumeration>KMeans</enumeration>
			<enumeration>NearestNeighbor</enumeration>
			<enumeration>FurthestNeighbor</enumeration>
			<enumeration>Centroid</enumeration>
		</valueType>
		<valueType name="DataAnalysisAssociationRulesOrderType" base="xs:string">
			<enumeration>BySupport</enumeration>
			<enumeration>ByConfidence</enumeration>
			<enumeration>ByImportance</enumeration>
		</valueType>
		<valueType name="DataAnalysisColumnType" base="xs:string">
			<enumeration>NotUsed</enumeration>
		</valueType>
		<valueType name="DataAnalysisColumnTypeAssociationRules" base="xs:string">
			<enumeration>NotUsed</enumeration>
			<enumeration>Object</enumeration>
			<enumeration>Item</enumeration>
		</valueType>
		<valueType name="DataAnalysisColumnTypeClusterization" base="xs:string">
			<enumeration>NotUsed</enumeration>
			<enumeration>Input</enumeration>
			<enumeration>Predictable</enumeration>
			<enumeration>InputAndPredictable</enumeration>
			<enumeration>Key</enumeration>
		</valueType>
		<valueType name="DataAnalysisColumnTypeDecisionTree" base="xs:string">
			<enumeration>NotUsed</enumeration>
			<enumeration>Input</enumeration>
			<enumeration>Predictable</enumeration>
		</valueType>
		<valueType name="DataAnalysisColumnTypeSequentialPatterns" base="xs:string">
			<enumeration>NotUsed</enumeration>
			<enumeration>Item</enumeration>
			<enumeration>Sequence</enumeration>
			<enumeration>Time</enumeration>
		</valueType>
		<valueType name="DataAnalysisColumnTypeSummaryStatistics" base="xs:string">
			<enumeration>NotUsed</enumeration>
			<enumeration>Input</enumeration>
		</valueType>
		<valueType name="DataAnalysisDistanceMetricType" base="xs:string">
			<enumeration>SquaredEuclidean</enumeration>
			<enumeration>Euclidean</enumeration>
			<enumeration>CityBlock</enumeration>
			<enumeration>Maximum</enumeration>
		</valueType>
		<valueType name="DataAnalysisNumericValueUseType" base="xs:string">
			<enumeration>AsNumeric</enumeration>
			<enumeration>AsBoolean</enumeration>
		</valueType>
		<valueType name="DataAnalysisResultTableFillType" base="xs:string">
			<enumeration>DontFill</enumeration>
			<enumeration>KeyFields</enumeration>
			<enumeration>UsedFields</enumeration>
			<enumeration>AllFields</enumeration>
		</valueType>
		<valueType name="DataAnalysisSequentialPatternsOrderType" base="xs:string">
			<enumeration>BySupport</enumeration>
			<enumeration>ByLength</enumeration>
		</valueType>
		<valueType name="DataAnalysisStandardizationType" base="xs:string">
			<enumeration>DontStandardize</enumeration>
			<enumeration>Standardize</enumeration>
		</valueType>
		<valueType name="DataAnalysisTimeIntervalUnitType" base="xs:string">
			<enumeration>Second</enumeration>
			<enumeration>CurrentMinute</enumeration>
			<enumeration>Minute</enumeration>
			<enumeration>CurrentHour</enumeration>
			<enumeration>Hour</enumeration>
			<enumeration>CurrentDay</enumeration>
			<enumeration>Day</enumeration>
			<enumeration>CurrentWeek</enumeration>
			<enumeration>Week</enumeration>
			<enumeration>CurrentMonth</enumeration>
			<enumeration>Month</enumeration>
			<enumeration>CurrentQuarter</enumeration>
			<enumeration>Quarter</enumeration>
			<enumeration>CurrentYear</enumeration>
			<enumeration>Year</enumeration>
			<enumeration>CurrentTenDays</enumeration>
			<enumeration>TenDays</enumeration>
			<enumeration>CurrentHalfYear</enumeration>
			<enumeration>HalfYear</enumeration>
		</valueType>
		<valueType name="DecisionTreeSimplificationType" base="xs:string">
			<enumeration>DontSimplify</enumeration>
			<enumeration>Simplify</enumeration>
		</valueType>
		<valueType name="PredictionModelColumnType" base="xs:string">
			<enumeration>Input</enumeration>
			<enumeration>Predictable</enumeration>
			<enumeration>DataSourceColumn</enumeration>
		</valueType>
	</package>
</model>� �!�f � � C   ���`�E>����� !(!� 0� c�H� ����
l
{�P�B��;j�������i] ��)������Q���J�^ ��Y��`��G�?�l����- �,�F�Yr0 a�y�E���F�
�
a'����S�) R   �	�	��hO�vD����&@2"cA�-��y � � Ru-����<!!\!@a���������	�	
@
� � �Mn���[x+}UvR6 ;�������	a		=	j���!�!��� �  `�  `�  `� `� `�  `�  `  `  `� `i  `� ` `  `�  `  `� `u `�  ` `.  ` `� `� ` `� ` `� `Q `V `� `k `T `> ` `� `� `� `� `	  `�  `& `k `� ` `� `{ `� `� `5 `� `! `( `B  `� `� `� `� `� `� `{ `) `� ` `= `� `7 `� `$  `t `�  `� `]  `  `t `� `$  `t `� `=  `]  `t `�  `o  `� `W  `� `� `m `� `� `� `> ` `U ` `�  `� `� `� `� `M ` `2 `- `; `� `� `_ `] `� `n `  `5 `� `  `�  `i  ` `�  `)  `  `` ` `v `G `]  `t `� `t ` `s `]  `� `�  `�  `3  `�  `� `�  `� `J `� `b `M `  `  `� `� `� `� `b `  `/ `� ` ` `8 `` `? `  `�  `' `  `�  `�  `� `� `� `+ `�  ` `} `�  `  `/ `' `  `J `Y `  `� ` `L  `� `m `� `5 `� `� `� `� `� `u  `� `�  `k `P `� `  `b `T `  ` `� `� `} `Q  `G  `8  `� `? `c  ` ` `Q  `G  `8  `  `{  ` ` `�  `I `C `� C! 