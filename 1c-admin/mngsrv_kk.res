  �'      ResB              -     C  �-  �-  �       �  IDS_SETPROP_SESSIONPARAMSETTINGS_ONLY IDS_EDITBTNTOOLTIP_CLEAR IDS_EDITBTNTOOLTIP_LISTSEL IDS_EDITBTNTOOLTIP_SPECSEL IDS_EDITBTNTOOLTIP_OPEN IDS_EDITBTNTOOLTIP_UP IDS_EDITBTNTOOLTIP_DOWN IDS_EDITBTNTOOLTIP_DROPLIST IDS_ADDIN_TITLE IDS_FSE_TITLE IDS_CPE_TITLE IDS_AGENT_TITLE IDS_CRYPTOEXTENSION IDS_HTML_COPYRIGHT IDS_HTML_RUN_MODE_DESCR IDS_HTML_EXTERN_MODULE_INSTALL_IN_PROGRESS IDS_HTML_EXTERN_MODULE_INSTALLED IDS_HTML_EXTERN_MODULE_ALREADY_INSTALLED IDS_HTML_EXTERN_MODULE_INSTALL_FAILED IDS_CONFIRM_INSTALL_IE IDS_VERIFY_PUBLISHER IDS_INSTALL_NOW_IE IDS_PRESS_CONTINUE IDS_FILE_EXT_INSTALL IDS_CRYPTO_EXT_INSTALL IDS_AGENT_EXT_INSTALL IDS_EXT_MODULE_INSTALL IDS_CHROME_EXT_INSTALL_AGREE_MSG IDS_CHROME_EXT_INSTALL_REJECT_MSG IDS_SAFARI_EXT_INSTALL_TIP_MACOS IDS_SAFARI_EXT_INSTALL_DOWNLOAD IDS_CHROME_37_EXT_INSTALL_BEGIN IDS_CHROME_37_EXT_INSTALL_FILES_EXT IDS_CHROME_37_EXT_INSTALL_CRYPTO_EXT IDS_CHROME_37_EXT_INSTALL_AGENT_EXT IDS_CHROME_37_EXT_INSTALL_ADDIN_EXT IDS_CHROME_37_EXT_INSTALL_WEB_EXT IDS_CHROME_37_EXT_INSTALL_WEB_EXT_LIN IDS_CHROME_37_EXT_INSTALL_HOST_EXT IDS_CHROME_37_EXT_INSTALL_HOST_EXT_LIN IDS_CHROME_37_EXT_INSTALL_HOST_EXT_LEFT IDS_CHROME_37_EXT_INSTALL_HOST_EXT_LEFT_LIN IDS_CHROME_37_EXT_INSTALL_NEED_RESTART IDS_CHROME_37_EXT_INSTALL_WAIT IDS_CHROME_37_EXT_INSTALL_WAIT_LIN IDS_CHROME_37_EXT_INSTALL_RESTART IDS_EDGE_START_IN_IE11 IDS_CHROME_CLIPBOARD_EXT_NOT_INSTALLED IDS_CHROME_CLIPBOARD_EXT_NEED_RESTART IDS_CHROME_EXT_ATTEMPT_TO_USE IDS_CHROME_EXT_INSTALL_MAYBE_LATER IDS_FF_EXT_INSTALL_WEB_EXT IDS_FF_EXT_INSTALL_HOST_EXT_LEFT IDS_FF_EXT_INSTALL_HOST_EXT_LEFT_LIN IDS_FF_CLIPBOARD_EXT_NOT_INSTALLED IDS_HTML_BROWSER_SETTINGS_INFO_CONFIRMATION IDS_HTML_COLOR_CHOOSE IDS_HTML_RED IDS_HTML_GREEN IDS_HTML_BLUE IDS_HTML_BACKGOUND_COLOR_PREVIEW IDS_HTML_TEXT_COLOR_PREVIEW IDS_HTML_OPEN_CALCULATOR IDS_HTML_OPEN_CALENDAR IDS_HTML_COPY_TO_CLIPBOARD_AS_NUMBER IDS_HTML_ADD_NUMBER_TO_CLIPBOARD IDS_HTML_SUBTRACT_NUMBER_FROM_CLIPBOARD IDS_HTML_SERVICE IDS_HTML_FORM_CUSTOMIZATION IDS_HTML_TOOLBAR_CUSTOMIZE IDS_HTML_ADD_REMOVE_BUTTONS IDS_HTML_ADD_GROUP IDS_HTML_ADD_FIELDS IDS_HTML_DELETE_CURRENT_ITEM IDS_HTML_MOVE_CURRENT_ITEM_UP IDS_HTML_MOVE_CURRENT_ITEM_DOWN IDS_HTML_MARK_ALL IDS_HTML_UNMARK_ALL IDS_HTML_DELETE IDS_HTML_MOVE_UP IDS_HTML_MOVE_DOWN IDS_HTML_RESTORE_DEFAULT_SETTINGS IDS_HTML_FORM_ITEM_PROPERTIES IDS_HTML_CHOOSE_FIELDS_TO_PLACE_ON_FORM IDS_HTML_LOADING IDS_HTML_THEMES_PANEL IDS_HTML_NAVIGATION_PANEL IDS_HTML_NAVIGATION2_PANEL IDS_HTML_OPEN_HELP IDS_HTML_SHOW_ABOUT IDS_HTML_FAVORITE_LINKS IDS_HTML_FAVORITES IDS_HTML_HISTORY IDS_HTML_ENTER_NAME_AND_PASSWORD IDS_HTML_EXIT IDS_HTML_REPEAT_ENTER IDS_HTML_FILE_UPLOADING IDS_HTML_FILE IDS_HTML_UPLOADING IDS_HTML_APPLY IDS_HTML_SHOW_BUTTON IDS_HTML_HISTORY_TITLE IDS_HTML_TOPICSELECTOR_TITLE IDS_HTML_FILE_DOWNLOADING IDS_RTE_ACTIVEDOCUMENTGET_ERROR IDS_RTE_CONNECTION_UNEXPECTEDLY_CLOSED IDS_RESET_TOOLBAR IDS_HTML_CLIPSTORE IDS_HTML_CLIPPLUS IDS_HTML_CLIPMINUS IDS_HTML_WINDOW_ACTIVATED_MSG IDS_HTML_UNABLE_TO_SWITCH_TO_MAINWINDOW IDS_HTML_CLIPBOARD_INACCESSIBLE IDS_HTML_USE_BROWSER_CLIPBOARD_COMMANDS IDS_HTML_CLIPBOARD_OPERATION_DISABLED IDS_TE_TITLE IDS_TE_MESSAGE IDS_TE_INTERRUPT IDS_CMD_HELPTRAINING IDS_WEB_COMPAT_PLATFORM_UNSUPPORTED IDS_WEB_COMPAT_BROWSER_UNSUPPORTED IDS_WEB_COMPAT_BROWSER_VERSION_UNSUPPORTED IDS_WEB_COMPAT_WORK_IMPOSSIBLE IDS_WEB_BROWSERSLIST_HEADER IDS_WEB_BROWSERSLIST_DOWNLOAD IDS_WEB_BROWSERSLIST_VERSION_IE IDS_WEB_BROWSERSLIST_VERSION_FF IDS_WEB_BROWSERSLIST_VERSION_CHROME IDS_WEB_BROWSERSLIST_VERSION_SAFARI IDS_WEB_BROWSERSLIST_VERSION_SAFARI_IPAD IDS_WEB_BROWSER_UPDATE_LINK_IE IDS_WEB_BROWSER_UPDATE_LINK_SAFARI IDS_WEB_BROWSER_UPDATE_LINK_SAFARI_IPAD IDS_HTML_SET_WARN_EXT_COMP IDS_HTML_SET_GET_FILE IDS_HTML_SET_WARN_LINK_GET_FILE IDS_EXECUTE_NOT_SUPPORTED IDS_SEARCH_PROCESSING IDS_LOGOUT_WORK_FINISH IDS_WORK_IN_FULLSCREEN IDS_WORK_ONLY_IN_FULLSCREEN IDS_START_WORKING IDS_CONTINUE_WORK IDS_ECS_BROWSER_NOT_SUPPORTED IDS_ECS_MEDIA_REQUIRED_SECURE_ORIGIN IDS_ECS_MEDIA_NOT_AVAILABLE IDC_OIDC_STANDARD_LOGIN_NAME IDC_OIDC_ANOTHER_SERVICES IDC_OIDC_ERROR browsersettingsinfoieru.html browsersettingsinfoieen.html browsersettingsinfochru.html browsersettingsinfochen.html browsersettingsinfosfru.html browsersettingsinfosfen.html browsersettingsinfoff.html browsersettingsinfomain.html   >N  �7C  �:  �0B5  �>AC  V@C  $09;  �K7K;  0AK;  "0@8E  (K�0@C  �:B5C  !5@28A  �@A5BC  #;:59BC  745C. . .   8F5=78O  �01K;40C  VHV@59BC  �:B5C. . .   "0@BK?  0;C  $>=  �;3VAV  ">?BK  �>AC  $09;4K  0;C  �C  =�A�0AK  �@VABV  �>AC  "0�40C  ( F 4 )   �BV=  �;3VAV  "�ABV  B0�40C  "0�40;�0=40@  $09;�0  6�:B5C  "�<5=35  0?0@C  �09B040=  :V@C  �=BV715=V  0HC  �;V<4V  B0�40C  =K�B0<0=K  0HC  �<KABK  10AB0C  �<KAB0  0O�B0C  >�0@K�0  0?0@C  "V7V<=5=  B0�40C  �;V<45@  ?0=5;V  �>=4K@C4K  10AB0C  $>@<0=K�  10?B0CK  �0BKA0BK=  15BB5@  0A�0  10BK@<0;0@  0@;K�K=  15;3V;5C  0;L:C;OB>@4K  0HC  "0�B0;0@4K  6V15@C  !0=  @5BV=45  :�HV@C  �<KABK  60;�0ABK@C  "070;0C  ( S h i f t + F 4 )   ><?>=5=BBV  6�:B5C  "0?AK@<0=K  >@K=40C  �07V@  �09B0  6�:B5C  AK@B�K  :><?>=5=BBV  HC  ( C t r l + S h i f t + F 4 )   "   /   I n s t a l l   >@=0BC"   5;5AV  :5745A:5=HV!   A- �@5:5BB5@  ?0=5;V  02830F8O;0@  ?0=5;V  "0�40;�0=  AV;B5<5;5@  ?5@0F8O  �>;  65BV<AV7.   !�  ! !"    �++  �K<40�K  M;5<5=BBV  6>N  @=0BC4K  :59V=35  �0;4K@C  845>:0<5@0  �>;  65BV<AV7  @0C75@4V  �0;09  10?B094K?   0BK@<0=K  6>N  =5<5A5  �>AC  CKABK@C  1CD5@V=5  A0=  �>AC  �>;409BK=  1@0C75@;5@  BV7V<V  ">;K�  M:@0=4K  @568<45  6�<KA  $>@<0=K�  M;5<5=BV=V�  �0A85BV  h t t p : / / w w w . a p p l e . c o m / r u / i o s /   "V7V<=5=  B0�40C  ( C t r l   +   D o w n )   !K@B�K  :><?>=5=BB5@4V  >@=0BC.   ;<0AC  1CD5@V=5=  A0=4K  5A5?B5C  !K@B�K  :><?>=5=B  >@=0BK;C40  . . .   D09;40@K  10@  6�<KAB0@4K  :5�59BC  @8?B>3@0D8O<5=  6�<KA  :5�59BV;CV  !B0=40@BBK  10?B0C;0@4K  >@=0BC. . .   �09B0  6V15@C4V  >@K=40C  :5@5:  ?5?   �K<40�K  M;5<5=BBV  B�<5=35  0?0@C  �K<40�K  M;5<5=BBV  6>�0@K�0  0?0@C  0@;K�  M;5<5=BB5@45=  15;3V=V  H5HC  !K@B�K  :><?>=5=B  A�BBV  >@=0BK;4K.   ;<0AC  1CD5@V=5  A0=  A5:V;4V  :�HV@C  0�40@;0<0  6�=V=45  0�?0@0BBK  :�@A5BC  $09;40@<5=  6�<KABK  :5�59BC4V  >@=0BC.   $>@<040  >@=0;0AC  �HV=  �@VABV  B0�40�K7  !K@B�K  :><?>=5BB5@4V  >@=0BC  >@K=40;C40  :@8?B>3@0D8OAK  10@    6�<KAB0@4K  :5�59BC  h t t p : / / w w w . a p p l e . c o m / r u / s a f a r i / d o w n l o a d /   )�@8?B>3@0D8O<5=  6�<KA  :5�59BV;CV=  >@=0BC.   +��<KA  B5:  �0=0  B>;K�  M:@0=4K  @568<45  <�<:V=  +�53V73V  B5@57535  �BC4V  >@K=40C  <�<:V=  5<5A.   ,�@K=40C  ?5@0B>@K  251- :;85=BB5  �>;40=K;<094K  ,ܫ 1 !: �AV?>@=K     E010@40@  5BC  6�=5  VA:5  �>AC�   ,ܒ0;0<B>@- 109;0=KAK=40  C0�KBH0  60�K;KA  1>;4K.   -�@=0BC4K  10AB0C  �HV=  " 0;�0ABK@C4K"   10ABK�K7.   /�35@  D09;4K  0;C  >@K=40;<0A0,   10?B0C4K  >@K=40�K7  0�$09;40@<5=  :5�59BV;35=  6�<KABK  >@=0BC  >@K=40;C40  2ܩ     " 1 !- !>DB" ,   1 9 9 6 - 2 0 1 8 .   0@;K�  ���K�  �>@�0;�0=  3�!V74V�  1@0C75@45  0;<0AC  1CD5@V=5  @��A0B  15@V;<535=.   3�"5@57535  �BC  >@K=40;4K.   0;�0ABK@C  �HV=    10AK�K7.   3�0940;0=K;0BK=  1@0C75@1 !: 80;>3?5=  6�<KA  �>;40<094K  5�@8?B>3@0D8O<5=  :5�59BV;35=  6�<KABK  >@=0BC  >@K=40;C40  ;�1 !: �AV?>@K=4K  �>;40=CHK=K�  0BK=  6�=5  ��?8O  A�7V=  5=3V7V�V7  =�C O M - >1J5:BV;5@V  B5:  W i n d o w s   >?5@0F8O;K�  6�95AV=45  �>;40=K;04K  >�845>  :0<5@0�0  �>;  65B:V7C  �HV=  �>@�0;�0=  �>AK;C  �065B  ( h t t p s )   >�!K@B�K  :><?>=5=B0  A�BBV  >@=0BK;4K  =5<5A5  5@B5@5:B5  >@=0BK;�0=.   @�!50=A�;H5<45@V=@=0BC  H0�K@CK=0=  :59V=  �0A85BV  �735@BV;5  0;<094K  Bܫ 1 !: �AV?>@=K     E010@40@  5BC  6�=5  VA:5  �>AC�   10�40@;0<0AK=  >@=0BC.   Bܚ>;409BK=  =�A�0;0@:   4 . 0   6�=5  6>�0@K.   < b r / > �AK=K;0BK=  =�A�0:   A>��K.   Dܚ>;40=0BK=  =�A�0;0@K:   5 2   6�=5  6>�0@K. < b r / > �AK=K;0BK=  =�A�0AK:   A>��K.   E�h t t p : / / w i n d o w s . m i c r o s o f t . c o m / r u - R U / i n t e r n e t - e x p l o r e r / p r o d u c t s / i e / h o m e   E�$>=4K�  B0?AK@<0;0@4K�  ��45;CV  6�@3V7V;C45.   �BV=5<V7,   :�B5  B�@K�K7  . . .   Eܚ>;409BK=  =�A�0;0@:   1 0   6�=5  >40=  6>�0@K. < b r / > �AK=K;0BK=  =�A�0:   A>��K.   Gܚ>;409BK=  =�A�0;0@K:   4 . 0 . 5   =5<5A5  6>�0@K.   < b r / > �AK=K;0BK=  =�A�0:   A>��K.   J�!K@B�K  :><?>=5=BB5@4V  >@=0BC  >@K=40;<04K! 
 @=0BC  :57V=45  �0B5  ?0940  1>;4K!   N�< p > % s   >@=0BC  �HV=  0;K=�0=  D09;4K  10AK�K7  6�=5  >@=0BC4K�  0O�B0;CK=  :�BV�V7. < / p >   Oܚ>;409BK=  =�A�0;0@K:   4 . 0 . 4   ( i O S   3 . 2 )   6�=5  6>�0@K.   < b r / > �AK=K;0BK=  =�A�0:   A>��K.   Qܫ 1 !: �AV?>@=K     E010@40@  5BC  6�=5  VA:5  �>AC�   10�40@;0<0AK=  >@=0BC  >@K=40;K?  60BK@  R�@=0BC40=  10A  B0@BC  �HV=  " 5@C4V  6>N  /   D i s c a r d "   10AK�K7  6�=5  >AK  B5@575=V  601K�K7.   S�!V7  :�HV@C  �HV=  C t r l + C ,   :5AC  �HV=  C t r l + X   6�=5  �>N  �HV=  C t r l + V   �>;40=CK�K7�0  1>;04K.   W�@=0BC�0  @��A0B  15@35  A�@0=KA  ?0940  1>;�0=  :5745  " @=0BC  /   I n s t a l l "   10BK@<0AK=  10AK�K7.   Y�< p >   % s   >@=0BC  �HV=  0;K=�0=  D09;4K  >@K=40C;K  5BV?  60A0�K7  6�=5  >=K  >@K=40C�0  6�:B5�V7. < / p >   o�5�59BC;5@  A�BBV  >@=0BK;�0==0=  :59V=  15BBV  �09B0  6�:B5C  �065B.   
   �09B0  6�:B5C  �HV=  " �07V@  �09B0  6�:B5C  10AK�K7.   r�@=0BC4K  10AB0C  �HV=  " �@V  �0@09/   C o n t i n u e " .   !>AK=  " @=0BC  /   I n s t a l l "   10BK@<0AK=  10AK?  >@=0BC�0  :5;VAV<4V  @0AB0�K7.   |�0940;0=K?  >BK@�0=  =�A�0  1 !: �AV?>@K=  �>;4094K=  BV7V<35  :V@<594V  <. 
   �>;409BK=  1@0C75@;5@4V�  BV7V<V=5  0CKAC  �HV=    10AK�K7.   ��!09B  % h o s t n a m e %   ?KB05BAO  >1@0B8BLAO  :  1CD5@C  >1<5=0.   06<8B5  ' O K '   GB>1K  @07@5H8BL  4>ABC?  MB><C  A09BC.   06<8B5  ' B<5=0'   4;O  70?@5B0  4>ABC?0.   ��< p > @0C75@  :5�59BC  �HV=  >@=0BK;�0=.   @=0BC  �0=0  �0;4K  % s . < / p > < p > ;  �HV=  �01K;40=�0=  D09;4K  >@=0BK;�0=  @5BV=45  6�=5  6V15@V�V7  >@=0BC�K. < / p > < p >   ��< p > @0C75@35  0@=0;�0=  :5�59BC;5@4V  6�=5  :><?>=5=BB0AK=  >940�K409  >@=0B�0=  A>�  ?0@0�BK  �09B0  6�:B5C  �065B.   �09B0  6�:B5C  �HV=  < b > " �07V@  �09B0  �>AK�K7" < / b > . < / p >   ��< p > @0C75@;5@  �HV=  :5�59BC;5@  >@=0BK;4K.   % s   >@=0BKCK  �0;4K. < / p > < p > ;  �HV=  15@V;35=  D09;�0  10AK�K7  ( 1@0C75@  B5@575AV=V�  B�<5=3V  60�K=40)   6�=5  >@K=40C4K�  0O�B0;CK=  :�BV�V7. < / p > < p >   ��AK  DC=:F8>=0;�0  �>;  65B:V7C  �HV=  I n t e r n e t   E x p l o r e r   1@0C75@V=45  �>AK<H0=K  �>AC  �AK=K;04K. 
   ;  �HV=  B�9<5HV<5=  1@0C75@4V�  <5=N4V  0HK�K7  " . . . "   6�=5  "   I n t e r n e t   E x p l o r e r   0HC"   B0@<0�K=  B0�40�K7.   ��< p > @=0BK;0BK=  �@5:5BBV  >@K=40C  �HV=  % s . < / p > < p > @=0BC4K  10AB0C  �HV=,   < b > " 0;�0ABK@C" < / b >   10BK@<0AK=  10AK�K7. < / p > < p > 0@;K�  :><?>=5=BB5@V  6�:B5C  >@K=40;04K.   ;K=�0=  D09;4K  10AK�K7  6�=5  >@=0BCK=  :�BV�V7. < / p >   ��!V7  �>;40=0BK=  1@0C75@  1 !: �AV?>@K=  �>;409BK=  BV7V<35  :V@<594V. 
 �95  6�<KAK  4�@KA  5<5A  1>;CK  <�<:V=. 
 �<KABK  60;�0ABK@C  �HV=    10AK�K7. 
 @0C75@4V  �>;40CHK;0@  BV7V<V=5  �BC  �HV=  >;4K@<0C  ( C a n c e l )   10AK�K7.   ��@=0BC�0  A�@0=KA  ?0940  1>;�0=40  :�7  65B:V7V�V7,   O�=8  02B>@  A0?0AK=40  AV7  A5=5BV=  :><?>=5=BB5@4V  65B:V7CHV  :�@A5BV;35=V=5!   @=0BC40=  10A  B0@BC  �HV=  " >;4K@<0C  /   C a n c e l "   10AK�K7.   @=0BC4K  @0AB0C  �HV=  10AK�K7  ��!V7  �>;40=0BK=  1@0C75@  =�A�0AK  1 !: �AV?>@K=K  �>;409BK=  BV7V<35  :V@<594V. 
 �95  6�<KAK  4�@KA  5<5A  1>;CK  <�<:V=. 
 �<KABK  60;�0ABK@C  �HV=    10AK�K7. 
 @0C75@4V  �>;40CHK;0@  BV7V<V=5  �BC  �HV=  >;4K@<0C  ( C a n c e l )   10AK�K7.   ���@5:5BBV  >@K=40C  �HV=  % s   >@=0BC  �065B.   @=0BC4K  10AB0C  �HV=  < b > " 0;�0ABK@C" < / b >   10AK�K7.   < / p > < p >   :><?>=5=BB5@  6�:B5C  >@K=40;0BK=  1>;04K.   40=  :59V=  0;K=�0=  D09;4K  >@K=40C;K  5BV?  60A0�K7  6�=5  >=K  >@K=40C�0  6�:B5�V7. < / p >   ��@0C75@  :5�59BV<V  >@=0BK;4K.   % s   >@=0BC  �HV=  �0;04K. < / p > < p > @=0BC4K  10AB0C  �HV=  < b > " 0;�0ABK@C" < / b >   10AK�K7. < / p > < p > ><?>=5=BB5@  6�:B5C  >@K=40;0BK=  1>;04K.   40=  :59V=  0;K=�0=  D09;4K  >@K=40C;K  5BV?  60A0�K7  6�=5  >=K  >@K=40C�0  6�:B5�V7. < p >   ��!V7  �>;40=0BK=  >?5@0F8O;K�  6�95  1 !: �AV?>@K=  �>;409BK=  BV7V<35  :V@<594V. 
 �95  6�<KAK  4�@KA  5<5A  1>;CK  <�<:V=. 
 �<KABK  60;�0ABK@C  �HV=    10AK�K7. 
 �>;409BK=  1@0C75@  6�=5  >?5@0F8O;K�  6�95=V�  BV7V<V=5  �BC  �HV=  >;4K@<0C  ( C a n c e l )   10AK�K7.   ��< p >  0AH8@5=85  4;O  1@0C75@0  1K;>  CAB0=>2;5=>.   AB0;>AL  CAB0=>28BL  % s . < / p > < p > ;O  =0G0;0  CAB0=>2:8  =06<8B5  :=>?:C  < b > " @>4>;68BL" < / b > . < / p > < p > C45B  2K?>;=5=0  703@C7:0  :><?>=5=BK.   >A;5  MB>3>  =06<8B5  =0  ?>;CG5==K9  D09;  8  4>648B5AL  7025@H5=8O  CAB0=>2:8. < p >   :�F i r e f o x   1@0C75@V=45  0;<0AC  1CD5@V<5=  B>;K�  6�<KA  VAB5C  �HV=  1@0C75@35  0@=0;�0=    :5�59BC4V  >@=0BC  �065B. 
 5�59BC4V  >@=0BC  �HV=  " @=0BC4K  10AB0C  B�9<5H5AV=  10AK�K7.   F i r e f o x   >@=0BC�0  15@V;35=  A�@0C�0  B>A�0CK;  �>9�0=K  BC@0;K  E010@;0<0  ?0940  1>;�0=  :5745  "  ��A0B  5BC"   B�9<5H5AV=,   >40=  :59V=  " @=0BC  "   B�9<5H5AV=  10AK�K7  ��"�<5=453V  AV;B5<535  10AK�K7. 
 @0C75@    :><?>=5=BV  10@  >@=0BC  D09;K=    6�:B594V. 
 35@  D09;  02B><0BBK  B�@45  0HK;<0A0,   >=K  6�:B5C  B5@575AV=5  5:V  @5B  H5@BV?  �74V3V=5=  0HK�K7. 
 0940  1>;�0=  =�A�0C;0@�0  �0@0?,   >@=0BC4K  >@K=40�K7. 
 @=0BC  :57V=45  !V7  >@=0BC  B5@575AV=V�  6>�0@K40  >�  60�K=40�K  15;3V=V  H5@BV?,   :><?>=5=B0=K�  65B:V7CHV=  B5:A5@5  0;0AK7.   !V7  A5=V<  1V;4V@5BV=  65B:V7CHV  :�@A5BV;35=V=5  :�7  65B:V7V�V7!   ��< p > �@5:5BBV  >@K=40C  �HV=  1@0C75@  <5=  % s   �HV=  :5�59BC  >@=0BC  �065B. < / p > < p > @=0BC4K  10AB0C  �HV=  < b > " 0;�0ABK@C"   B�9<5H5AV=  10AK�K7. < / b > . < / p > < p > F i r e f o x   >@=0BC�0  A�@0C  A0;CK=0  B>A�0CK;  �>9�0=K  BC@0;K  E010@;0<0  ?0940  1>;�0=  :5745  "  ��A0B  5BC"   B�9<5H5AV=,   >40=  A>�  " @=0BC"   B�9<5H5AV=  10AK�K7.   @=0BC�0  A�@0C  ?0940  1>;�0=  :5745  02B>@  @5BV=45  AV7  A5=V<  1V;4V@5BV=  :>=B5=BBV�  65B:V7CHVAV  :�@A5BV;35=V=5  :�7  65B:V7V�V7! < / p >   ��C h r o m e   1@0C75@V=45  0;<0AC  1CD5@V<5=  B>;K�  6�<KA  VAB5C  �HV=  1@0C75@35  0@=0;�0=  :5�59BC4V  >@=0BC  �065B.     
 5�59BC4V  >@=0BC  �HV=  " @=0BC4K  10AB0C  B�9<5HB5AV=  10AK�K7,   >40=  :59V=  0HK;�0=  ?0@0�B0  " + "53V="   B�9<5H5AV=  10AK�K7  ( �>AK<H0=K  �09B0  �>AC  B0;0?  5BV;54V) .   
 �>AK<H0=K  :5;5AV45  �>A�0=  :5745  :5�59BC4V  02B><0BBK  B�@45  >@=0BC  �HV=  " @=0BC4K  :59V=35  �0;4K@C"   B�9<5H5AV=  10AK�K7.   
 5�59BC4V  �07V@  >@=0B?0C  �HV=    " >N"   B�9<5H5AV=  10AK�K7.   ��< p > �@5:5BBV  >@K=40C  �HV=  1@0C75@V  �HV=  6�=5  % s   :5�59BC4V  >@=0BC4K  B0;0?  5B54V. < / p > < p > @=0BC4K  10AB0C  �HV=  < b > " 0;�0ABK@C" < / b >   B�9<5H5AV=  10AK�K7. < / p > < p > 0�40@;0<0  G o o g l e   C h r o m e   <03078=V=45  :5�59BC  ?0@0�K=  0H04K.   5�59BC4V  >@=0BC  �HV=  < b > " + & n b s p ; "" < / b >   B�9<5H5=V  10AK�K7. < / p > < p > 5�59BC4V  >@=0B�0==0=  :59V=  :5�59BC  ?0@0�K=  60CK?  6�=5  �7V=V�  AK@B�K  :><?>=5=BV=  >@=0BC  �065B. < / p > < p > ;  �HV=  0;K=�0=  D09;4K  >@K=40C;K  5BV?  60A0�K7  6�=5  >=K  >@K=40C�0  6�:B5�V7. < / p >   ���@5:5BBV  >@K=40C    �HV=  1@0C75@35  6�=5  0@=0;�0=    % s     0@=0;�0=  :5�59BC4V  >@=0BC  �065B.   < / p > < p > 5�59BC4V  >@=0BC  �HV=  < b > " @=0BC4K  10AB0C" < / b >   B�9<5H5AV=  10AK�K7.   < / p > < p > 0�40@;0<0  G o o g l e   C h r o m e   <03078=V=453V  :5�59BC  ?0@0�K=  0H04K.   5�59BC4V  >@=0BC  �HV=    < b > " + & n b s p ;   "" < / b >   B�9<5H5AV=  10AK�K7.   5�59BC4V  >@=0�0==0=  :59V=  ?0@0�BK  601C  �065B  6�=5  AK@B�K  :><?>=5=BBV�  �7V=  >@=0BC  �065B.   < / p > < p >   ;  �HV=  0;K=�0=  D09;�0  10AK�K7  ( 1@0C75@4V�  B�<5=3V  B5@575AV=45  >@=0;0A�0=)   6�=5  >@=0BC4K�  0O�B0;�0=K=  :�BV�V7.   < / p >   ���  ﻿<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" >
        <meta http-equiv="imagetoolbar" content="no">
        <title>Браузерді қалай орнату керек</title>
        <link rel="shortcut icon" href="e1csys/mngsrv/favicon.ico" />
<style type="text/css">
body
{
    width:100%;
    height:100%;
    overflow:hidden;
    padding:0px;
    margin:0px;
}

p
{
    font-size:13pt;
    font-family:Times New Roman;
}

img
{
    margin-bottom:48px;
}

.title
{
    font-size:21pt;
    font-family:Times New Roman;
}

.step
{
    background-color:#99CCFF;
    padding-left:12px;
}
</style>

</head>

<body>
<div style="width:100%;height:100%;overflow:auto;">
<div style="padding:8px;">

<p class="title" style='font-weight:bold'>1С:Кәсіпорын үшін веб-браузерді теңшеу</p>
<p class="title step">1-қадам</p>

<p>Түртіңіз <b><i>Құралдар</i></b>, терезенің оң жақ жоғарғы бұрышында орналасқан.</p>
<p>Мәзірден таңдаңыз <b><i>Settings</i></b>.</p>

<div><img border=0 src="e1csys/mngsrv/img_ch_1_en.png?sysver=<%V8VER%>" /></div>

<p>Бетбелгі ашылады <b><i>Settings-Basics</i></b>, онда барлық қосымша параметрлер жасалады.</p> 
<p>Нұсқаулықтарға оралу үшін, бетбелгіні таңдаңыз <b><i>Браузерді қалай орнату керек</i></b>.</p>

<div><img border=0 src="e1csys/mngsrv/img_ch_0_en.png?sysver=<%V8VER%>" /></div>

<p class="title step">2-қадам</p>

<p>Сілтемеге өтіңіз <b><i>Show advanced settings...</i></b> экранның төменгі жағында басыңыз және оны басыңыз.</p>

<div><img border=0 src="e1csys/mngsrv/img_ch_2_en.png?sysver=<%V8VER%>" /></div>

<p>Блокқа өтіңіз <b><i>Downloads</i></b> құсбелгісін қойыңыз <b><i>Ask where to save each file before downloading</i></b>.</p>

<div><img border=0 src="e1csys/mngsrv/img_ch_3_en.png?sysver=<%V8VER%>" /></div>

<p class="title step">3-қадам</p>

<p>Қорапты тексергеннен кейін түймесін басыңыз. <b><i>Content Settings</i></b>, ол блокта жоғары орналасқан <b><i>Privacy</i></b>.</p>

<div><img border=0 src="e1csys/mngsrv/img_ch_4_en.png?sysver=<%V8VER%>" /></div>

<p>Пайда болған терезеде <b><i>Content Settings</i></b> блокқа барыңыз <b><i>Pop-Ups</i></b> және қорапты белгілеңіз <b><i>Allow all sites to show pop-ups</i></b>.</p>

<div><img border=0 src="e1csys/mngsrv/img_ch_5_en.png?sysver=<%V8VER%>" /></div>

<p style="background-color:#E4E4E4">Сіздің шолғышыңызда өзгерістерді сақтаудың қажеті жоқ екенін ескеріңіз. 
Параметрді аяқтау үшін бетбелгіні жабыңыз <i>Settings</i>.</p>

</div>
</div>

</body>
</html>
����������  ﻿<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" >
        <meta http-equiv="imagetoolbar" content="no">
        <title>Браузерді қалай орнату керек</title>
        <link rel="shortcut icon" href="e1csys/mngsrv/favicon.ico" />
<style type="text/css">
body
{
    width:100%;
    height:100%;
    overflow:hidden;
    padding:0px;
    margin:0px;
}

p
{
    font-size:13pt;
    font-family:Times New Roman;
}

img
{
    margin-bottom:48px;
}

.title
{
    font-size:21pt;
    font-family:Times New Roman;
}

.step
{
    background-color:#99CCFF;
    padding-left:12px;
}
</style>

</head>

<body>
<div style="width:100%;height:100%;overflow:auto;">
<div style="padding:8px;">

<p class="title" style='font-weight:bold'>1С:Кәсіпорын үшін веб-браузерді теңшеу</p>
<p class="title step">1-қадам</p>

<p>Түртіңіз <b><i>Құралдар</i></b>, терезенің оң жақ жоғарғы бұрышында орналасқан.</p>
<p>Мәзірден таңдаңыз <b><i>Параметрлер</i></b>.</p>

<div><img border=0 src="e1csys/mngsrv/img_ch_1_ru.png?sysver=<%V8VER%>" /></div>

<p>Бетбелгі ашылады <b><i>Параметрлер-Негізгі</i></b>, онда барлық қосымша параметрлер жасалады.</p> 
<p>Нұсқаулықтарға оралу үшін, бетбелгіні таңдаңыз <b><i>Браузерді қалай орнату керек</i></b>.</p>

<div><img border=0 src="e1csys/mngsrv/img_ch_0_ru.png?sysver=<%V8VER%>" /></div>

<p class="title step">2-қадам</p>

<p>Сілтемеге өтіңіз <b><i>Қосымша параметрлерді көрсету</i></b> экранның төменгі жағында басыңыз және оны басыңыз.</p>

<div><img border=0 src="e1csys/mngsrv/img_ch_2_ru.png?sysver=<%V8VER%>" /></div>

<p>Блокқа өтіңіз <b><i>Жүктеулер</i></b> құсбелгісін қойыңыз <b><i>Жүктеу алдында әр файлды сақтау үшін орын сұраңыз</i></b>.</p>

<div><img border=0 src="e1csys/mngsrv/img_ch_3_ru.png?sysver=<%V8VER%>" /></div>

<p class="title step">3-қадам</p>

<p>Қорапты тексергеннен кейін түймесін басыңыз <b><i>Мазмұн параметрлері</i></b>, ол блокта жоғары орналасқан <b><i>Жеке ақпарат</i></b>.</p>

<div><img border=0 src="e1csys/mngsrv/img_ch_4_ru.png?sysver=<%V8VER%>" /></div>

<p>Пайда болған терезеде <b><i>Мазмұн параметрлері</i></b> блокқа барыңыз <b><i>Қалқымалы терезелер</i></b> және қорапты белгілеңіз <b><i>Барлық тораптардағы қалқымалы терезелерге рұқсат беру</i></b>.</p>

<div><img border=0 src="e1csys/mngsrv/img_ch_5_ru.png?sysver=<%V8VER%>" /></div>

<p style="background-color:#E4E4E4">Сіздің шолғышыңызда өзгерістерді сақтаудың қажеті жоқ екенін ескеріңіз. 
Параметрді аяқтау үшін бетбелгіні жабыңыз <i>Параметрлер</i>.</p>

</div>
</div>

</body>
</html>
�������  ﻿<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" >
        <meta http-equiv="imagetoolbar" content="no">
        <title>Браузерді қалай орнату керек</title>

<style type="text/css">
body
{
    width:100%;
    height:100%;
    overflow:hidden;
    padding:0px;
    margin:0px;
}

p
{
    font-size:13pt;
    font-family:Times New Roman;
}

img
{
    margin-bottom:48px;
}

.title
{
    font-size:21pt;
    font-family:Times New Roman;
}

.step
{
    background-color:#99CCFF;
    padding-left:12px;
}
</style>

</head>

<body>
<div style="width:100%;height:100%;overflow:auto;">
<div style="padding:8px;">

<p class="title" style='font-weight:bold'>1С:Кәсіпорын үшін веб-браузерді теңшеу</p>

<ul>
    <li>Қолданбаның қалқымалы терезелерін мәзірде рұқсат ету үшін <b>Құралдар (Tools)</b> шолғышты таңдаңыз <b>Параметрлер (Options)</b>;<br>
        Ашылған терезеде бөлімге өтіңіз <b>Мазмұны (Content)</b>;<br>
        Белгіні алып тастаңыз <b>Қалқымалы терезелерді блоктау (Block pop-up windows)</b>.</li>
    <li>Қолданбалы терезелер арасында ауысуды қолмен іске қосу үшін браузердің мекенжай жолағының түрі <b>about:config</b>;<br>
        Содан кейін сүзгі жолында теріңіз <code>dom.disable_window_flip</code>;<br>
        Параметрді өзгертіңіз <code>false</code>.</li>
    <li>Бастапқы параметрлерде латыни емес таңбаларды қолмен қосу үшін браузердің мекенжай жолағына теріңіз <b>about:config</b>;<br>
        Содан кейін сүзгі жолында теріңіз <code>network.standard-url.encode-query-utf8</code>. Егер бұл параметр табылмаса, теріңіз <code>browser.fixup.use-utf8</code>;<br>
        Параметрді өзгертіңіз <code>true</code>.</li>
    <li>Қолданбалар терезелерінің арасында ауысу үшін пернетақтаны пайдалануды қолмен іске қосу үшін, браузер түрінің мекенжай жолында <b>about:config</b>;<br>
        Содан кейін сүзгі жолында теріңіз <code>dom.popup_allowed_events</code>;<br>
        Осы параметрдің мәніне оқиға қосыңыз <b>keydown</b>.</li>
    <li>Операциялық жүйенің аутентификациясын қолмен конфигурациялау үшін браузердің мекенжай жолағына теріңіз <b>about:config</b>;<br>
        Содан кейін сүзгі жолындағы параметрлер бетінде параметрдің атауын енгізіңіз;<br>
        Бұл параметр үш параметр үшін: <code>network.automatic-ntlm-auth.trusted-uris</code>, <code>network.negotiate-auth.delegation-uris</code>, <code>network.negotiate-auth.trusted-uris</code>;<br>
        Бұдан әрі «1С:Кәсіпорындар» дерекқорымен жұмыс істейтін веб-серверлердің тізімін көрсетіңіз.</li>
</ul>

<p>Орнату аяқталды.</p>

</div>
</div>

</body>
</html>
��������������  ﻿<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" >
        <meta http-equiv="imagetoolbar" content="no">
        <title>Браузерді қалай орнату керек</title>
        <link rel="shortcut icon" href="e1csys/mngsrv/favicon.ico" />
<style type="text/css">
body
{
    width:100%;
    height:100%;
    overflow:hidden;
    padding:0px;
    margin:0px;
}

p
{
    font-size:13pt;
    font-family:Times New Roman;
}

img
{
    margin-bottom:48px;
}

.title
{
    font-size:21pt;
    font-family:Times New Roman;
}

.step
{
    background-color:#99CCFF;
    padding-left:12px;
}
</style>

</head>

<body>
<div style="width:100%;height:100%;overflow:auto;">
<div style="padding:8px;">

<p class="title" style='font-weight:bold'>1С:Кәсіпорын үшін веб-браузерді теңшеу</p>
<p class="title step">1-қадам</p>

<p>Мекенжай жолағы астындағы еркін аймаққа тінтуірдің оң жақ түймешігімен нұқыңыз (суретте ашық қызыл түспен белгіленген) 
пайда болатын мәзірде таңдаңыз <b><i>Menu Bar</i></b>.</p>

<div><img border=0 src="e1csys/mngsrv/img_ie_0_en.png?sysver=<%V8VER%>" /></div>

<div><img border=0 src="e1csys/mngsrv/img_ie_01_en.png?sysver=<%V8VER%>" /></div>

<p>Мекенжай жолағында мәзір пайда болады. Элементті тауып алыңыз <b><i>Tools</i></b>.</p>

<div><img border=0 src="e1csys/mngsrv/img_ie_1_en.png?sysver=<%V8VER%>" /></div>

<p>Оны басыңыз, мәзір ашылады. Элементті таңдаңыз <b><i>Internet Options</i></b>.</p>

<div><img border=0 src="e1csys/mngsrv/img_ie_2_en.png?sysver=<%V8VER%>" /></div>

<p class="title step">2-қадам</p>

<p>Пайда болған терезеде бетбелгіге өтіңіз <b><i>Privacy</i></b>.</p>

<div><img border=0 src="e1csys/mngsrv/img_ie_3_en.png?sysver=<%V8VER%>" /></div>

<p>Терезенің ортасында табыңыз және <b><i>белгісін алып тастаңыз</i></b> жалауша <b><i>Turn on Pop-up Blocker</i></b>.</p>

<div><img border=0 src="e1csys/mngsrv/img_ie_4_en.png?sysver=<%V8VER%>" /></div>

<p>Құсбелгісі қойылғаннан кейін, түймені басыңыз <b><i>ОК</i></b>, параметрлерді сақтау үшін.</p>

<div><img border=0 src="e1csys/mngsrv/img_ie_5_en.png?sysver=<%V8VER%>" /></div>

<p>Орнату аяқталды.</p>

<p style='font-size:9pt'>Мекенжай жолағындағы мәзірді жасыру үшін, 1-қадамның бірінші бөлігін қайталаңыз.</p>

</div>
</div>

</body>
</html>
����������D  ﻿<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" >
        <meta http-equiv="imagetoolbar" content="no">
        <title>Браузерді қалай орнату керек</title>
        <link rel="shortcut icon" href="e1csys/mngsrv/favicon.ico" />
<style type="text/css">
body
{
    width:100%;
    height:100%;
    overflow:hidden;
    padding:0px;
    margin:0px;
}

p
{
    font-size:13pt;
    font-family:Times New Roman;
}

img
{
    margin-bottom:48px;
}

.title
{
    font-size:21pt;
    font-family:Times New Roman;
}

.step
{
    background-color:#99CCFF;
    padding-left:12px;
}
</style>

</head>

<body>
<div style="width:100%;height:100%;overflow:auto;">
<div style="padding:8px;">

<p class="title" style='font-weight:bold'>1С:Кәсіпорын үшін веб-браузерді теңшеу</p>
<p class="title step">1-қадам</p>

<p>Мекенжай жолағы астындағы еркін аймаққа тінтуірдің оң жақ түймешігімен нұқыңыз (суретте ашық қызыл түспен белгіленген) 
пайда болатын мәзірде таңдаңыз <b><i>Мәзір жолағы</i></b>.</p>

<div><img border=0 src="e1csys/mngsrv/img_ie_0_ru.png?sysver=<%V8VER%>" /></div>

<div><img border=0 src="e1csys/mngsrv/img_ie_01_ru.png?sysver=<%V8VER%>" /></div>

<p>Мекенжай жолағында мәзір пайда болады. Элементті тауып алыңыз <b><i>Қызмет</i></b>.</p>

<div><img border=0 src="e1csys/mngsrv/img_ie_1_ru.png?sysver=<%V8VER%>" /></div>

<p>Оны басыңыз, мәзір ашылады. Элементті таңдаңыз <b><i>Интернет опциялары</i></b>.</p>

<div><img border=0 src="e1csys/mngsrv/img_ie_2_ru.png?sysver=<%V8VER%>" /></div>

<p class="title step">2-қадам</p>

<p>Пайда болған терезеде бетбелгіге өтіңіз <b><i>Құпиялылық</i></b>.</p>

<div><img border=0 src="e1csys/mngsrv/img_ie_3_ru.png?sysver=<%V8VER%>" /></div>

<p>Терезенің ортасында табыңыз және <b><i>белгісін алып тастаңыз</i></b> жалауша <b><i>Қалқымалы блоктаушыны қосу</i></b>.</p>

<div><img border=0 src="e1csys/mngsrv/img_ie_4_ru.png?sysver=<%V8VER%>" /></div>

<p>Құсбелгісі қойылғаннан кейін, түймені басыңыз <b><i>ОК</i></b>, параметрлерді сақтау үшін.</p>

<div><img border=0 src="e1csys/mngsrv/img_ie_5_ru.png?sysver=<%V8VER%>" /></div>

<p>Орнату аяқталды.</p>

<p style='font-size:9pt'>Мекенжай жолағындағы мәзірді жасыру үшін, 1-қадамның бірінші бөлігін қайталаңыз.</p>

</div>
</div>

</body>
</html>
��������  ﻿<!DOCTYPE html>
<html class="IWeb">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>Браузерді теңшеу</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta http-equiv="imagetoolbar" content="no">
        <link href="<%CSS_FILE%>?sysver=<%V8VER%>" rel="stylesheet" type="text/css" />
        <link rel="shortcut icon" href="e1csys/mngsrv/favicon.ico" />
        <style type="text/css">
        .main
        {
            width:720px;
            margin:0 auto;
        }
        .head
        {
            font-size:32pt;
            font-family:Times New Roman;
        }
        .title
        {
            font-size:20pt;
            font-family:Times New Roman;
        }
        .txt
        {
            font-size:14pt;
            font-family:Times New Roman;
        }
        </style>
</head>
<body style="width:100%;height:100%;overflow:hidden;padding:0;margin:0;">
<div style="width:100%;height:100%;overflow-y:auto;overflow-x:hidden;">
<div style="padding:10px;padding-left:30px;padding-right:30px;">
<p><span class="txt"><b>Жаңа терезенің ашылуы бұғатталды, бәлкім, қалқымалы блокатор.</b></span></p>
<p><span class="txt">Жалғастыру үшін веб-браузерді баптаңыз.<br>
Орнату нұсқауларын алу үшін осы батырманы басыңыз. <b><i>Нұсқаулықты ашыңыз</i></b>.<br>
Конфигурация аяқталған кезде түймені басыңыз. <b><i>OK</i></b> және қайта іске қосыңыз.</span></p>
<div align="center" style="width:100%;height:50px;padding-top:20px;">
    <div style="width:450px;height:30px;position:relative;">
    <button id="okButton" class="webButton" style="left:10px;">Нұсқаулықты ашыңыз</button>
    <button id="cancelButton" class="webButton" style="left:230px;">OK</button>
    </div>
    </div>
</div>
</div>
<!--@remove+@-->
<script type="text/javascript">
    var CLOSURE_NO_DEPS = true;
    var CLOSURE_BASE_PATH = "scripts/";
</script>
<script type="text/javascript" src="scripts/webtools/libs/closure-library/closure/goog/base.js?sysver=<%V8VER%>"></script>
<script type="text/javascript" src="scripts/mngsrv/src/res/js/deps.js?sysver=<%V8VER%>"></script>
<script type="text/javascript" src="scripts/mngsrv/src/res/js/base.js?sysver=<%V8VER%>"></script>
<script type="text/javascript" src="scripts/mngsrv/src/res/js/dbgstart_browsersettingsinfomain.js?sysver=<%V8VER%>"></script>
<!--@remove-@-->
<script type="text/javascript" src="scripts/mod_browsersettingsinfomain_browsersettingsinfomain.js?sysver=<%V8VER%>"></script>
</body>
</html>
����������������  ﻿<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" >
        <meta http-equiv="imagetoolbar" content="no">
        <title>Браузерді қалай орнату керек</title>
        <link rel="shortcut icon" href="e1csys/mngsrv/favicon.ico" />
<style type="text/css">
body
{
    width:100%;
    height:100%;
    overflow:hidden;
    padding:0px;
    margin:0px;
}

p
{
    font-size:13pt;
    font-family:Times New Roman;
}

img
{
    margin-bottom:48px;
}

.title
{
    font-size:21pt;
    font-family:Times New Roman;
}

.step
{
    background-color:#99CCFF;
    padding-left:12px;
}
</style>

</head>

<body>

<body>
<div style="width:100%;height:100%;overflow:auto;">
<div style="padding:8px;">

<p class="title" style='font-weight:bold'>1С:Кәсіпорын үшін веб-браузерді теңшеу</p>

<p>Жоғарғы сол жақ бұрышта мәзір элементін табыңыз <b><i>Safari</i></b>, оны басыңыз. Ашылған мәзірде құсбелгіні алып тастаңыз <b><i>Block Pop-Up Windows</i></b>.</p>

<div><img border=0 src="e1csys/mngsrv/img_sf_en.png?sysver=<%V8VER%>" /></div>

<p>Орнату аяқталды.</p>

</div>
</div>

</body>
</html>
������   ﻿<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" >
        <meta http-equiv="imagetoolbar" content="no">
        <title>Браузерді қалай орнату керек</title>
        <link rel="shortcut icon" href="e1csys/mngsrv/favicon.ico" />
<style type="text/css">
body
{
    width:100%;
    height:100%;
    overflow:hidden;
    padding:0px;
    margin:0px;
}

p
{
    font-size:13pt;
    font-family:Times New Roman;
}

img
{
    margin-bottom:48px;
}

.title
{
    font-size:21pt;
    font-family:Times New Roman;
}

.step
{
    background-color:#99CCFF;
    padding-left:12px;
}
</style>

</head>

<body>
<div style="width:100%;height:100%;overflow:auto;">
<div style="padding:8px;">

<p class="title" style='font-weight:bold'>1С:Кәсіпорын үшін веб-браузерді теңшеу</p>

<p>Жоғарғы сол жақ бұрышта мәзір элементін табыңыз <b><i>Safari</i></b>, оны басыңыз. Ашылған мәзірде құсбелгіні алып тастаңыз <b><i>Қалқымалы терезелерді блоктау</i></b>.</p>

<div><img border=0 src="e1csys/mngsrv/img_sf_ru.png?sysver=<%V8VER%>" /></div>

<p>Орнату аяқталды.</p>

</div>
</div>

</body>
</html>
� ��� �#��<�\5�X�<���u�����o3t���^F � � _ � z � ��d?_E2��
���N	�D��~�G`�Y5
V
���r
�	�
z
�0	�$
�
v	��v���	�	0I�	�d
	Z�����	�
��	��
���
�9Ll%E��  ]�wj$Y=��w��(Gj���*A}`�C&���1 `  `  `p `� `�
 `� `� `� `� `� `� `q `C `% `� `{ ` `� `�" `�  `S `B `� `� `_ `� `�  `� `/ `� `� `U	 ` 	 `x `
 `$ `B `s  `� `R `� `�  `P  `f `j `� `� ` `� `P ` `�  `�  `� `� `j  `�  `	  `/ `D `� ` `3  `  ` `�  `� `� `  `H `�	 `� `H
 `� `� `� `�  ` `  `�  `�  `� ` `'  `-  `� `}  `� `P `q ` `s `� `� `� `( `F `!  ` ` `a  `A  `�  `� ` `� `H  `� `�  `� `� `7 `9 `� `:  `J `� `� `d `� `
 `�	 `� `U ` `X  `�
 `U `  `� `h ` `�  `� ` `R `& `m `R `� `� `5 `� `N `� `8 `� ` `� � k '! '$ ;' * + 